# Packaging

## Configuration - `Atomstack A5`

|         | Speed | Power | Interval | Passes |
| ------- | ----- | ----- | -------- | ------ |
| Engrave | 1200  | 20%   | 0.13     | N/A    |
| Cut     | 1400  | 100%  | N/A      | 5      |

### F52 - boxes

<table>
  <tr>
    <td><img src="https://packhelp.cz/build/shared-images/product-dimensions/desktop/1280x800_F52.png" alt="F52" width="400px"/></td>
    <td>

- _26.5 x 19.5 x 6 cm_ Outer
- _24.9 x 19 x 5.5 cm_ Inner

    </td>
  </tr>
</table>

<table>
  <tr>
    <td><img src="./F52-front.svg" alt="F52-front" width="400px"/></td>
    <td><img src="./F52-inner.svg" alt="F52-inner" width="400px"/></td>
    <td><img src="./F52-inset.svg" alt="F52-inset" width="400px"/></td>
  </tr>
</table>

### F62 - boxes

<table>
  <tr>
    <td><img src="https://packhelp.cz/build/shared-images/product-dimensions/desktop/1280x800_F62.png" alt="F62" width="400px"/></td>
    <td>

- _30.2 x 23.3 x 8.8 cm_ Outer
- _28.9 x 22.8 x 8.3 cm_ Inner

    </td>
  </tr>
</table>

<table>
  <tr>
    <td><img src="./F62-front.svg" alt="F62-front" width="400px"/></td>
    <td><img src="./F62-inner.svg" alt="F62-inner" width="400px"/></td>
    <td><img src="./F62-inset.svg" alt="F62-inset" width="400px"/></td>
  </tr>
</table>

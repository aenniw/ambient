const fs = require("fs");
const path = require("path");
const express = require("express");

const BASE_PATH = "/";
const ASSETS_DIR = "../expo/dist";

const app = express();
if (fs.existsSync(ASSETS_DIR)) {
  app.use(BASE_PATH, express.static(ASSETS_DIR));
  app.get("*", function (req, resp) {
    resp.sendFile(path.resolve(__dirname, `${ASSETS_DIR}/index.html`));
  });
} else {
  throw Error(`No assets found at "${ASSETS_DIR}"`);
}

app.listen(8081);

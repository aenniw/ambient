import { expect } from "@playwright/test";
import { test } from "./support";

test.beforeEach(async ({ page }) => {
  await page.goto("/");
  await expect(page.getByText(/No device available/).first()).toBeVisible();
  await page.evaluate(() => document.fonts.ready);
});

test.describe("App", () => {
  test(
    "should show unsupported browser view",
    { tag: "@visual" },
    async ({ page, ble }) => {
      await ble.setPermission("not-supported");
      await expect(page).toHaveScreenshot();
    },
  );

  test(
    "should show missing permission view",
    { tag: "@visual" },
    async ({ page, ble }) => {
      await ble.setPermission("prompt");
      await expect(page).toHaveScreenshot();
    },
  );

  test(
    "should show location disabled view",
    { tag: "@visual" },
    async ({ page, ble }) => {
      await ble.disableLocation();
      await expect(page).toHaveScreenshot();
    },
  );

  test(
    "should show bluetooth disabled view",
    { tag: "@visual" },
    async ({ page, ble }) => {
      await ble.disable();
      await expect(page).toHaveScreenshot();
    },
  );

  test("should show home view", { tag: "@visual" }, async ({ page, ble }) => {
    await expect(page).toHaveScreenshot();
  });

  test.describe("Interactions", { tag: "@smoke" }, () => {
    test("should enable bluetooth", async ({ page, ble }) => {
      await ble.disable();
      await expect(page.getByText(/Bluetooth disabled/).first()).toBeVisible();

      await expect(page.getByTestId("icon-bluetooth.disabled")).toBeVisible();
      await page.getByTestId("button-button.enable").click();
      await ble.enable(); // Enabled vie activity thus needs to be manually toggled

      await expect(page.getByTestId("button-add")).toBeVisible();
      await expect(page.getByTestId("icon-pixel.none")).toBeVisible();
    });

    test("should enable location", async ({ page, ble }) => {
      await ble.disableLocation();
      await expect(page.getByText(/Location disabled/).first()).toBeVisible();

      await expect(page.getByTestId("icon-location.disabled")).toBeVisible();
      await page.getByTestId("button-button.enable").click();

      await expect(page.getByTestId("button-add")).toBeVisible();
      await expect(page.getByTestId("icon-pixel.none")).toBeVisible();
    });

    test("should grant permissions", async ({ page, ble }) => {
      await ble.setPermission("denied");
      await expect(
        page
          .getByText(/Permissions permanently denied for application/)
          .first(),
      ).toBeVisible();

      await expect(page.getByTestId("icon-permissions.denied")).toBeVisible();
      await expect(page.getByTestId("button-button.enable")).toHaveAttribute(
        "aria-disabled",
        "true",
      );

      await ble.setPermission("prompt");
      await expect(
        page.getByText(/Permissions not granted/).first(),
      ).toBeVisible();

      await expect(page.getByTestId("icon-permissions.none")).toBeVisible();
      await page.getByTestId("button-button.enable").click();
      await expect(page.getByTestId("button-add")).toBeVisible();
    });

    test("should open documentation", async ({ page, context }) => {
      await expect(page.getByTestId("icon-pixel.none")).toBeVisible();

      const pagePromise = context.waitForEvent("page");
      await page.getByTestId("button-book").click();
      const newPage = await pagePromise;

      await newPage.waitForLoadState();
      expect(newPage.url()).toEqual(
        "https://aenniw.gitlab.io/ambient/docs/intro/wiring/",
      );
    });
  });
});

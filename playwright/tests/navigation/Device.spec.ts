import {
  expect,
  PlaywrightTestArgs,
  PlaywrightTestOptions,
  PlaywrightWorkerArgs,
  PlaywrightWorkerOptions,
} from "@playwright/test";
import { CommonFixtures, config, pixel, storageState, test } from "../support";

test.use(
  storageState([
    { id: "Light", config: deviceConfig(true) },
    { id: "Light-Off", config: deviceConfig(false) },
    { id: "Light-Disabled", config: deviceConfig(false, false) },

    { id: "Light-Option-0", config: deviceConfig(true, true, 0) },
    { id: "Light-Option-1", config: deviceConfig(true, true, 1) },
    { id: "Light-Option-2", config: deviceConfig(true, true, 2) },
    { id: "Light-Option-3", config: deviceConfig(true, true, 3) },

    { id: "Light-0.1.0", config: deviceConfig(true, true, 1, "0.1.0") },
  ]),
);

function deviceConfig(
  state = false,
  enabled = true,
  mode = 0,
  version?: string,
) {
  return config([pixel(state, enabled, mode, 100)], undefined, version);
}

async function navigate(
  { page }: Pick<PlaywrightTestArgs, "page">,
  deviceId: string,
  path: "color" | "colors" = "color",
) {
  await page.goto(`/device/${deviceId}/0/${path}`, { waitUntil: "commit" });

  await expect(page.getByText(/Controls/).first()).toBeVisible();
  await page.evaluate(() => document.fonts.ready);
}

test.describe("Device", () => {
  test("should be off", { tag: "@visual" }, async ({ page, ble }) => {
    await navigate({ page }, "Light-Off");

    await expect(page.getByTestId("icon-pixel.off")).toBeVisible();
    await expect(page).toHaveScreenshot();
  });

  test("should be disabled", { tag: "@visual" }, async ({ page, ble }) => {
    await navigate({ page }, "Light-Disabled");

    await expect(page.getByTestId("icon-pixel.disabled")).toBeVisible();
    await expect(page).toHaveScreenshot();
  });

  test("should be disconnected", { tag: "@visual" }, async ({ page, ble }) => {
    await navigate({ page }, "Light");
    ble.device("Light", "disconnect", true);

    await expect(page.getByTestId("icon-pixel.disconnected")).toBeVisible();
    await expect(page).toHaveScreenshot();
  });

  ["color", "dimm", "rainbow", "colors"].forEach((type, i) => {
    test(
      `should show ${type} options`,
      { tag: "@visual" },
      async ({ page }) => {
        await navigate({ page }, `Light-Option-${i}`);
        await expect(
          page.getByText(/Device not reachable/).first(),
        ).toBeHidden();

        await expect(page).toHaveScreenshot();
      },
    );
  });

  test(
    "should show colors palete options",
    { tag: "@visual" },
    async ({ page }) => {
      await navigate({ page }, "Light-Option-3", "colors");
      await expect(page.getByText(/Device not reachable/).first()).toBeHidden();

      await expect(page).toHaveScreenshot();
    },
  );

  test.describe("Interactions", { tag: "@smoke" }, () => {
    const partitions = [
      "partition-1",
      "partition-2",
      "partition-3",
      "partition-4",
    ];

    test("should be able to switch modes", async ({ page }) => {
      await navigate({ page }, "Light");

      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(221, 255, 204)");
      await expect(page.getByTestId("slider-brightness-medium")).toBeVisible();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeHidden();
      }
      await page.getByTestId("button-mode-1").click();

      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(221, 255, 204)");
      await expect(page.getByTestId("slider-speed")).toBeVisible();
      await expect(page.getByTestId("slider-brightness-medium")).toBeVisible();
      await expect(page.getByTestId("switch-chain")).toBeVisible();
      await expect(page.getByTestId("switch-random")).toBeVisible();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeVisible();
      }
      await page.getByTestId("switch-chain").click();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeHidden();
      }
      await page.getByTestId("button-mode-2").click();

      await expect(page.getByTestId("slider-speed")).toBeVisible();
      await expect(page.getByTestId("slider-brightness-medium")).toBeVisible();
      await expect(page.getByTestId("switch-chain")).toBeVisible();
      await expect(page.getByTestId("switch-random")).toBeVisible();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeHidden();
      }
      await page.getByTestId("switch-random").click();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeVisible();
      }
      await page.getByTestId("button-mode-3").click();

      await expect(page.getByTestId("slider-speed")).toBeVisible();
      await expect(page.getByTestId("slider-brightness-medium")).toBeVisible();
      await expect(page.getByTestId("switch-chain")).toBeVisible();
      await expect(page.getByTestId("switch-random")).toBeVisible();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeVisible();
      }
      await page.getByTestId("switch-random").click();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeHidden();
      }
    });

    test("should not display partitions on pre 0.1.0", async ({ page }) => {
      await navigate({ page }, "Light-0.1.0");

      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(221, 255, 204)");
      await expect(page.getByTestId("slider-brightness-medium")).toBeVisible();
      for (const partition of partitions) {
        await expect(page.getByTestId(`button-${partition}`)).toBeHidden();
      }
    });

    test("should be able to switch partition", async ({ page }) => {
      await navigate({ page }, "Light-Option-1");

      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(221, 255, 204)");
      await expect(page.getByTestId("slider-brightness-medium")).toBeVisible();
      await expect(page.getByTestId("button-partition-1")).toHaveCSS(
        "background-color",
        "rgb(0, 122, 255)",
      );
      await expect(page.getByTestId("button-partition-2")).toHaveCSS(
        "background-color",
        "rgb(128, 128, 128)",
      );
      await expect(page.getByTestId("button-partition-3")).toHaveCSS(
        "background-color",
        "rgb(128, 128, 128)",
      );
      await expect(page.getByTestId("button-partition-4")).toHaveCSS(
        "background-color",
        "rgb(128, 128, 128)",
      );

      await page.getByTestId("button-partition-3").click();
      await expect(page.getByTestId("button-partition-1")).toHaveCSS(
        "background-color",
        "rgb(128, 128, 128)",
      );
      await expect(page.getByTestId("button-partition-2")).toHaveCSS(
        "background-color",
        "rgb(128, 128, 128)",
      );
      await expect(page.getByTestId("button-partition-3")).toHaveCSS(
        "background-color",
        "rgb(0, 122, 255)",
      );
      await expect(page.getByTestId("button-partition-4")).toHaveCSS(
        "background-color",
        "rgb(128, 128, 128)",
      );
    });

    test("should be able to add color", async ({ page }) => {
      await navigate({ page }, "Light-Option-3", "colors");

      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();

      await page.getByTestId("button-add").click();
      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(255, 255, 255)");
      await page.getByTestId("button-close").click();

      await expect(page.getByTestId("button-color-0,0,#ffffff")).toBeHidden();
      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();

      await page.getByTestId("button-add").click();
      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(255, 255, 255)");
      await page.getByTestId("button-check").click();

      await expect(page.getByTestId("button-color-0,0,#ffffff")).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();
    });

    test("should be able to remove color", async ({ page }) => {
      await navigate({ page }, "Light-Option-3", "colors");

      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();

      await page.getByTestId("button-color-100,20,#ddffcc-close").click();

      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeHidden();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();
    });

    test("should be able to edit color", async ({ page }) => {
      await navigate({ page }, "Light-Option-3", "colors");

      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();

      await page.getByTestId("button-color-100,40,#bbff99").click();
      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(187, 255, 153)");
      const picker = page.getByTestId("color-picker").getByTestId("wheel-wrap");
      const pickerBox = await picker.boundingBox();
      await picker.click({
        position: {
          x: (pickerBox?.width || 0) / 2,
          y: (pickerBox?.height || 0) - 1,
        },
      });
      await page.getByTestId("button-check").click();

      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-270,99,#8103ff"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeHidden();
    });

    test("should be able to reset color", async ({ page }) => {
      await navigate({ page }, "Light-Option-3", "colors");

      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeVisible();

      await page.getByTestId("button-color-100,80,#77ff33").click();
      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-thumb"),
      ).toHaveCSS("background-color", "rgb(119, 255, 51)");
      await page.getByTestId("button-wheel-reset").click();
      await page.getByTestId("button-check").click();

      await expect(
        page.getByTestId("button-color-100,20,#ddffcc"),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,40,#bbff99"),
      ).toBeVisible();
      await expect(page.getByTestId("button-color-0,0,#ffffff")).toBeVisible();
      await expect(
        page.getByTestId("button-color-100,80,#77ff33"),
      ).toBeHidden();
    });
  });
});

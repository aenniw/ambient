import { expect } from "@playwright/test";
import { config, storageState, pixel, test } from "../support";

test.use(
  storageState([
    {
      id: "Indoor-light",
      config: config([
        pixel(true, true, 0, 100),
        pixel(true, false, 1, 0),
        pixel(true, undefined, 2, 100),
      ]),
    },
    { id: "Outdoor-light", config: config() },
  ]),
);

test.beforeEach(async ({ page }) => {
  await page.goto("/home");

  await expect(page.getByText(/Indoor/).first()).toBeVisible();
  await expect(page.getByText(/Strand #1/).first()).toBeVisible();
  await expect(page.getByText(/Strand #3/).first()).toBeVisible();
  await expect(page.getByText(/Outdoor/).first()).toBeVisible();
  await page.evaluate(() => document.fonts.ready);
});

test.describe("Home", () => {
  test("should show devices", { tag: "@visual" }, async ({ page }) => {
    await expect(page).toHaveScreenshot();
  });

  test.describe("Interactions", { tag: "@smoke" }, () => {
    test("should show settings", async ({ page }) => {
      await page.getByTestId("icon-cog").click();

      await expect(page.getByText(/Settings/).first()).toBeVisible();
    });

    test("should show discovery", async ({ page }) => {
      await page.getByTestId("button-add").click();

      await expect(page.getByText(/Discovery/).first()).toBeVisible();
    });

    test("should show enabled strand", async ({ page }) => {
      await page.getByTestId("button-strand-0").click();

      await expect(
        page.getByTestId("color-picker").getByTestId("wheel-wrap"),
      ).toBeVisible();
    });

    test("should show disabled strand", async ({ page }) => {
      await page.getByTestId("switch-strand-2").click();
      await page.getByTestId("button-strand-2").click();

      await expect(page.getByTestId("icon-pixel.off")).toBeVisible();
      await expect(page.getByTestId("button-button.enable")).toBeVisible();
      await expect(page.getByText(/Pixels off/).first()).toBeVisible();
    });
  });
});

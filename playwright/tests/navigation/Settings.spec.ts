import { expect } from "@playwright/test";
import { config, storageState, pixel, test } from "../support";

test.use(
  storageState([
    { id: "Indoor-light", config: config([pixel(true, true, 0, 100)]) },
  ]),
);

test.beforeEach(async ({ page }) => {
  await page.goto("/settings/Indoor-light");
  await expect(page.getByText(/Settings/).first()).toBeVisible();
  await page.evaluate(() => document.fonts.ready);
});

test.describe("Settings", () => {
  test("should show configuration", { tag: "@visual" }, async ({ page }) => {
    await expect(page).toHaveScreenshot();
  });

  test.describe("Interactions", { tag: "@smoke" }, () => {
    test("should be able to check updates", async ({ page }) => {
      await expect(page.getByTestId("button-button.check")).toHaveCSS(
        "background-color",
        "rgb(0, 122, 255)",
      );
      page.getByTestId("button-button.check").click();
      await expect(page.getByTestId("button-button.check")).toHaveCSS(
        "background-color",
        "rgb(128, 128, 128)",
      );
    });

    test("should be able to update length", async ({ page }) => {
      await expect(
        page.getByTestId("strand-0-length-value").getByText(/40/).first(),
      ).toBeVisible();
      for (let i = 0; i < 10; i++) {
        await page.getByTestId("button-strand-0-length-decrease").click();
      }

      await expect(
        page.getByTestId("strand-0-length-value").getByText(/0/).first(),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-strand-0-length-increase"),
      ).toHaveCSS("background-color", "rgb(198, 213, 126)");
      await expect(
        page.getByTestId("button-strand-0-length-decrease"),
      ).toHaveCSS("background-color", "rgb(198, 213, 126)");

      for (let i = 0; i < 15; i++) {
        await page.getByTestId("button-strand-0-length-increase").click();
      }
      await expect(
        page.getByTestId("strand-0-length-value").getByText(/60/).first(),
      ).toBeVisible();
      await expect(
        page.getByTestId("button-strand-0-length-increase"),
      ).toHaveCSS("background-color", "rgb(213, 126, 126)");
      await expect(
        page.getByTestId("button-strand-0-length-decrease"),
      ).toHaveCSS("background-color", "rgb(213, 126, 126)");
    });

    test("should be able to update power", async ({ page }) => {
      await expect(
        page.getByTestId("power-value").getByText(/1500/).first(),
      ).toBeVisible();
      for (let i = 0; i < 5; i++) {
        await page.getByTestId("button-power-decrease").click();
      }

      await expect(
        page.getByTestId("power-value").getByText(/1000/).first(),
      ).toBeVisible();
      await expect(page.getByTestId("button-power-increase")).toHaveCSS(
        "background-color",
        "rgb(198, 213, 126)",
      );
      await expect(page.getByTestId("button-power-decrease")).toHaveCSS(
        "background-color",
        "rgb(198, 213, 126)",
      );

      for (let i = 0; i < 15; i++) {
        await page.getByTestId("button-power-increase").click();
      }
      await expect(
        page.getByTestId("power-value").getByText(/2500/).first(),
      ).toBeVisible();
      await expect(page.getByTestId("button-power-increase")).toHaveCSS(
        "background-color",
        "rgb(213, 126, 126)",
      );
      await expect(page.getByTestId("button-power-decrease")).toHaveCSS(
        "background-color",
        "rgb(213, 126, 126)",
      );
    });
  });
});

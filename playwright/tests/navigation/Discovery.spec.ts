import { expect } from "@playwright/test";
import { config, storageState, test } from "../support";

test.use(
  storageState(
    [
      { id: "Indoor-light", config: config([]) },
      { id: "Outdoor-light", config: config([], false) },
    ],
    [],
  ),
);

test.beforeEach(async ({ page }) => {
  await page.goto("/discovery");

  await expect(
    page.getByTestId("button-device-Outdoor-light").first(),
  ).toBeVisible();
  await expect(
    page.getByTestId("button-device-Indoor-light").first(),
  ).toBeVisible();
  await page.evaluate(() => document.fonts.ready);
});

test.describe("Discovery", () => {
  test("should show devices", { tag: "@visual" }, async ({ page }) => {
    await expect(page).toHaveScreenshot();
  });

  test.describe("Interactions", { tag: "@smoke" }, () => {
    test("should pair with device", async ({ page }) => {
      await page.getByTestId("button-device-Indoor-light").click();

      await expect(
        page.getByText(/Attempting to pair device with phone/).first(),
      ).toBeVisible();
      await expect(page.getByTestId("icon-check-circle-outline")).toBeVisible();
      await expect(page.getByTestId("button-button.back")).toBeVisible();
    });

    test("should not pair with device", async ({ page }) => {
      await page.getByTestId("button-device-Outdoor-light").click();

      await expect(
        page.getByText(/Attempting to pair device with phone/).first(),
      ).toBeVisible();
      await expect(page.getByTestId("icon-error-outline")).toBeVisible();
      await expect(page.getByTestId("button-button.retry")).toBeVisible();
    });
  });
});

import { test as base, expect } from "@playwright/test";
import type { Page, Locator } from "@playwright/test";
import { DeviceConfig, PixelConfig } from "../../expo/common/ble-plx/types";
import playwrightConfig from "../playwright.config";

type AppPermissions = PermissionState | "not-supported";

export type CommonFixtures = {
  ble: {
    enable: () => Promise<void>;
    disable: () => Promise<void>;
    setPermission: (v: AppPermissions) => Promise<void>;
    disableLocation: () => Promise<void>;
    device: (id: string, m: string, ...args: any) => Promise<void>;
    uuids: (v: string[]) => Promise<void>;
    devices: (v: { id: string; config: DeviceConfig }[]) => Promise<void>;
  };
};

export const test = base.extend<CommonFixtures>({
  ble: async ({ page }, use) =>
    use({
      enable: async () => {
        await page.evaluateHandle(() => (window as any).ble.enable());
      },
      disable: async () => {
        await page.evaluateHandle(() => (window as any).ble.disable());
      },
      disableLocation: async () => {
        await page.evaluateHandle(() => (window as any).ble.discoverFail(601));
      },
      setPermission: async (v) => {
        await page.evaluateHandle((v) => (window as any).permission(v), v);
      },
      device: async (id, m, ...args) => {
        await page.evaluateHandle(
          ({ id, m, args }) => (window as any).ble.device(id)[m](args),
          { id, m, args },
        );
      },
      uuids: async (uuids) => {
        await page.evaluateHandle(
          (uuids) => window.localStorage.setItem("devices-ble", uuids),
          JSON.stringify(uuids),
        );
      },
      devices: async (devices) => {
        await page.evaluateHandle(
          (devices) => window.localStorage.setItem("devices-mock", devices),
          JSON.stringify(devices),
        );
      },
    }),
});

export function config(
  pixels: PixelConfig[] | undefined = [],
  pairable: boolean | undefined = true,
  version: string | undefined = "0.1.1",
): DeviceConfig {
  return {
    version: pairable ? version : undefined,
    hardware: "esp-test",
    power: 1500,
    pixels,
  };
}

export function pixel(
  state: boolean,
  enabled: boolean | undefined,
  mode: number,
  color: number,
): PixelConfig {
  return {
    enabled,
    brightness: 100,
    length: 40,
    state: state ? 1 : 0,
    mode: [
      mode,
      { chained: true, duration: 1500, randomized: false, partition: 0 },
    ],
    color: [color, 20, ""],
    colors: [
      [color, 20, ""],
      [color, 40, ""],
      [color, 80, ""],
    ],
  };
}

export function storageState(
  devices: { id: string; config: DeviceConfig }[],
  uuids: [] | undefined = undefined,
) {
  return {
    storageState: {
      cookies: [],
      origins: [
        {
          origin: playwrightConfig.use.baseURL,
          localStorage: [
            {
              name: "devices-mock",
              value: JSON.stringify(devices),
            },
            {
              name: "devices-ble",
              value: JSON.stringify(uuids || devices.map(({ id }) => id)),
            },
          ],
        },
      ],
    },
  };
}

import { defineConfig, devices } from "@playwright/test";

const CI = !!process.env.CI;

export default defineConfig({
  testDir: "./tests",
  fullyParallel: true,
  forbidOnly: CI,
  retries: CI ? 2 : 0,
  workers: undefined,
  expect: {
    toHaveScreenshot: {
      threshold: 0.2,
    },
  },
  reporter: [
    ["list"],
    ["html", { outputFolder: "reports/html" }],
    ["junit", { outputFile: "reports/junit/results.xml" }],
  ],
  use: {
    baseURL: "http://127.0.0.1:8081",
    trace: "on-first-retry",
    locale: "en-GB",
    timezoneId: "Europe/Prague",
    screenshot: "only-on-failure",
    video: "on-first-retry",
    launchOptions: {
      args: [
        "--disable-gpu",
        "--disable-gpu-rasterization",
        "--disable-gpu-compositing",
        "--disable-font-subpixel-positioning",
        "--disable-software-rasterizer",
        "--ppapi-subpixel-rendering-setting=0",
        "--force-device-scale-factor=1",
        "--force-color-profile=srgb",
      ],
    },
  },

  projects: [
    {
      name: "Mobile Light",
      use: {
        ...devices["iPhone X"],
        defaultBrowserType: "chromium",
        colorScheme: "light",
      },
    },
    {
      name: "Mobile Dark",
      use: {
        ...devices["iPhone X"],
        defaultBrowserType: "chromium",
        colorScheme: "dark",
      },
      grepInvert: /@smoke/,
    },
    {
      name: "Mobile Landscape",
      use: {
        ...devices["iPhone X"],
        viewport: {
          height: devices["iPhone X"].viewport.width,
          width: devices["iPhone X"].viewport.height,
        },
        defaultBrowserType: "chromium",
        colorScheme: "dark",
      },
      grepInvert: /@smoke/,
    },
  ],

  webServer: {
    command: "yarn run proxy",
    url: "http://127.0.0.1:8081",
    reuseExistingServer: !CI,
  },
});

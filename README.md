# Ambient [![runs with expo](https://img.shields.io/badge/Runs%20with%20Expo-000.svg?style=flat-square&logo=EXPO&labelColor=f3f3f3&logoColor=000)](https://expo.io/) [![pipeline status](https://gitlab.com/aenniw/ambient/badges/main/pipeline.svg)](https://gitlab.com/aenniw/ambient/-/commits/main) [![Latest Release](https://gitlab.com/aenniw/ambient/-/badges/release.svg)](https://gitlab.com/aenniw/ambient/-/releases) [![Pages](https://gitlab.com/uploads/-/system/group/avatar/500013/pages_group_avatar.png?width=20)](https://aenniw.gitlab.io/ambient/)

<table>
  <tr>
    <td><img src="playwright/tests/navigation/Home.spec.ts-snapshots/Home-should-show-devices-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Device.spec.ts-snapshots/Device-should-show-dimm-options-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Settings.spec.ts-snapshots/Settings-should-show-configuration-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Device.spec.ts-snapshots/Device-should-show-colors-options-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Device.spec.ts-snapshots/Device-should-show-colors-palete-options-1-Mobile-Light-linux.png"></td>
  </tr>
  <tr>
    <td><img src="playwright/tests/navigation/Home.spec.ts-snapshots/Home-should-show-devices-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Device.spec.ts-snapshots/Device-should-show-dimm-options-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Settings.spec.ts-snapshots/Settings-should-show-configuration-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Device.spec.ts-snapshots/Device-should-show-colors-options-1-Mobile-Light-linux.png"></td>
    <td><img src="playwright/tests/navigation/Device.spec.ts-snapshots/Device-should-show-colors-palete-options-1-Mobile-Light-linux.png"></td>
  </tr>
</table>

## Build

```bash
docker run --rm -it -u 1000 -v $(pwd):/workspace -w /workspace registry.gitlab.com/aenniw/ambient/android-sdk:33-node18 /bin/bash

cd expo && \
    yarn install --frozen-lockfile && \
    yarn android:build
```

## Test

```bash
docker run --rm -it -u 1000 -v $(pwd):/workspace -w /workspace cypress/browsers:node-18.20.3-chrome-125.0.6422.141-1-ff-126.0.1-edge-125.0.2535.85-1 /bin/bash

cd expo && \
    yarn install --frozen-lockfile && \
    yarn web:build --dev && \
    cd -

cd playwright &&
    yarn install --frozen-lockfile && \
    yarn test
```

### Cheat-sheet

- `GooglePlay` compatible `keystore`
```bash
keytool -genkey -v -alias ${KEY_ALIAS} -keyalg RSA -keysize 2048 -validity 10950 -storetype JKS \
    -dname "CN=<name>,OU=<unit>,O=<org>,L=<location>,C=<country>" \
    -keypass ${KEY_PASSWORD} \
    -keystore ${KEYSTORE_FILE} \
    -storepass ${KEYSTORE_PASSWORD}
```

- `adb` screenshot
```bash
adb exec-out screencap -p > "screen-$(date +%s).png"
```

- chrome WebBle proxy `chrome://inspect/#devices`

- web mock devices
```javascript
localStorage.setItem(
  "devices-mock",
  JSON.stringify([
    {
      id: "light-1",
      config: {
        version: "0.1.1",
        hardware: "aaaa",
        power: 1500,
        pixels: [
          {
            brightness: 100,
            length: 40,
            state: 1,
            mode: [0, { chained: true, duration: 1500, randomized: false }],
            color: [100, 10, ""],
            colors: [
              [10, 10, ""],
              [10, 10, ""],
              [10, 10, ""],
              [10, 10, ""],
              [10, 10, ""],
              [10, 10, ""],
            ],
          }
        ],
      },
    },
    {
      id: "light-2",
      config: { version: "0.2", hardware: "dwqd", power: 1500, pixels: [] },
    }
  ]),
);
```

### Assets

- [Icons](https://icons.expo.fyi/)
- [AndroidAssetStudio (Legacy)](http://romannurik.github.io/AndroidAssetStudio/index.html)
- [AndroidAssetStudio](https://icon.kitchen/)
- [Colors](https://colorhunt.co/palettes/pastel)
- [Libraries](https://reactnative.directory/?search=text)
- [StoreAssets](https://hotpot.ai/templates/google-play-feature-graphic)
- [Privacy](https://github.com/nisrulz/app-privacy-policy-generator)

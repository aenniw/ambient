const fs = require("fs/promises");

const PROJECT_ID = 34623671;
const PROJECT_NAME = "aenniw/espressif";
const RESOURCES_DIR = "resources/firmwares";

async function fetchApi(path, fallback) {
  try {
    const resp = await fetch(
      `https://gitlab.com/api/v4/projects/${PROJECT_ID}${path}`,
      {
        method: "GET",
        headers: { Accept: "application/json" },
      }
    );

    return await resp.json();
  } catch (_) {
    return fallback;
  }
}

async function fetchBinary({ id, name }) {
  const req = await fetch(
    `https://gitlab.com/${PROJECT_NAME}/-/package_files/${id}/download`
  );
  return fs.writeFile(
    `${RESOURCES_DIR}/${name}`,
    Buffer.from(await req.arrayBuffer())
  );
}

async function fetchFirmwares() {
  const binaries = [];
  const packages = await fetchApi(`/packages`, []);
  for (const { name, id, version } of packages) {
    if (name !== "lights-ble") continue;

    const files = await fetchApi(`/packages/${id}/package_files`, []);
    for (const { id, file_name } of files) {
      if (file_name.indexOf("firmware") === -1) continue;

      binaries.push({
        id,
        name: file_name.replace("firmware", version),
      });
    }
  }

  return Promise.all(binaries.map(fetchBinary));
}

fetchFirmwares();

**Zásady ochrany osobních údajů**

`aenniw` vytvořil aplikaci Ambient jako aplikaci s otevřeným zdrojovým kódem. Tuto SLUŽBU poskytuje „aenniw“ bezplatně a je určena k použití tak, jak je.

Tato stránka slouží k informování návštěvníků o mých zásadách se shromažďováním, používáním a zveřejňováním osobních údajů, pokud se někdo rozhodne používat moji službu.

Pokud se rozhodnete používat moji Službu, souhlasíte se shromažďováním a používáním informací v souvislosti s těmito zásadami. Osobní údaje, které shromažďuji, slouží k poskytování a zlepšování Služby. Nebudu používat ani sdílet vaše informace s nikým jiným, než je popsáno v těchto Zásadách ochrany osobních údajů.

Pojmy použité v těchto Zásadách ochrany osobních údajů mají stejný význam jako v našich Smluvních podmínkách, které jsou dostupné na webu Ambient, pokud není v těchto Zásadách ochrany osobních údajů uvedeno jinak.

**Shromažďování a používání informací**

Pro lepší zážitek při používání naší služby mohu požadovat, abyste nám poskytli určité osobní údaje. Informace, které požaduji, budou uchovány ve vašem zařízení a žádným způsobem je neshromažďuji.

**Zaznamenaná data**

Chci vás informovat, že kdykoli použijete moji Službu, v případě chyby v aplikaci shromažďuji data a informace (prostřednictvím produktů třetích stran) na vašem telefonu s názvem Log Data. Tato data protokolu mohou obsahovat informace, jako je adresa internetového protokolu („IP“) vašeho zařízení, název zařízení, verze operačního systému, konfigurace aplikace při využívání mé služby, čas a datum vašeho používání služby a další statistiky .

**Soubory cookie**

Cookies jsou soubory s malým množstvím dat, které se běžně používají jako anonymní jedinečné identifikátory. Ty se do vašeho prohlížeče odesílají z navštívených webových stránek a ukládají se do vnitřní paměti vašeho zařízení.

Tato služba tyto „cookies“ výslovně nepoužívá. Aplikace však může používat kód a knihovny třetích stran, které používají „cookies“ ke shromažďování informací a zlepšování svých služeb. Máte možnost tyto soubory cookie přijmout nebo odmítnout a vědět, kdy je soubor cookie odeslán do vašeho zařízení. Pokud se rozhodnete odmítnout naše soubory cookie, možná nebudete moci používat některé části této služby.

**Poskytovatelé služeb**

Mohu zaměstnávat společnosti a jednotlivce třetích stran z následujících důvodů:

*   Pro usnadnění naší Služby;
*   Poskytovat Službu naším jménem;
*   K provádění služeb souvisejících se službou; nebo
*   Abychom nám pomohli analyzovat, jak je naše služba používána.

Chci informovat uživatele této služby, že tyto třetí strany mají přístup k jejich osobním údajům. Důvodem je plnění jim svěřených úkolů naším jménem. Jsou však povinni tyto informace nezveřejňovat ani používat k žádnému jinému účelu.

**Bezpečí**

Vážím si vaší důvěry, že nám poskytujete své osobní údaje, a proto se snažíme používat komerčně přijatelné prostředky k jejich ochraně. Pamatujte však, že žádný způsob přenosu přes internet nebo způsob elektronického ukládání není 100% bezpečný a spolehlivý a nemohu zaručit jeho absolutní bezpečnost.

**Odkazy na jiné stránky**

Tato služba může obsahovat odkazy na jiné stránky. Pokud kliknete na odkaz třetí strany, budete přesměrováni na daný web. Upozorňujeme, že tyto externí stránky neprovozuji já. Proto vám důrazně doporučuji, abyste si prostudovali Zásady ochrany osobních údajů těchto webových stránek. Nemám žádnou kontrolu a nepřebírám žádnou odpovědnost za obsah, zásady ochrany osobních údajů nebo postupy jakýchkoli stránek nebo služeb třetích stran.

**Soukromí dětí**

Vědomě neshromažďuji osobní údaje od dětí. Vyzývám všechny děti, aby prostřednictvím aplikace a/nebo služeb nikdy neodesílaly žádné osobně identifikovatelné informace. Vyzývám rodiče a zákonné zástupce, aby sledovali používání internetu svými dětmi a pomáhali prosazovat tyto Zásady tím, že svým dětem nařídím, aby nikdy neposkytovaly osobně identifikovatelné informace prostřednictvím Aplikace a/nebo Služeb bez jejich svolení. Pokud máte důvod se domnívat, že nám dítě prostřednictvím aplikace a/nebo služeb poskytlo osobně identifikovatelné údaje, kontaktujte nás. Pro udělení souhlasu se zpracováním vašich osobních údajů ve vaší zemi vám také musí být alespoň 16 let (v některých zemích můžeme vašemu rodiči nebo zákonnému zástupci umožnit, aby tak učinil vaším jménem).

**Změny těchto zásad ochrany osobních údajů**

Čas od času mohu aktualizovat naše zásady ochrany osobních údajů. Proto vám doporučujeme, abyste tuto stránku pravidelně kontrolovali kvůli případným změnám. O jakýchkoli změnách vás budu informovat zveřejněním nových Zásad ochrany osobních údajů na této stránce.

Tato politika je účinná od 2022-04-18

**Kontaktujte nás**

Pokud máte nějaké dotazy nebo návrhy ohledně mých zásad ochrany osobních údajů, neváhejte mě kontaktovat na aenniw@gmail.com.
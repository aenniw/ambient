**Všeobecné obchodní podmínky**

Stažením nebo používáním aplikace se na vás tyto podmínky budou automaticky vztahovat – měli byste si je proto před použitím aplikace pozorně přečíst. Aplikaci, jakoukoli část aplikace ani naše ochranné známky nesmíte žádným způsobem kopírovat ani upravovat. Nemáte povoleno pokoušet se extrahovat zdrojový kód aplikace a také byste se neměli pokoušet překládat aplikaci do jiných jazyků nebo vytvářet odvozené verze. Samotná aplikace a všechny ochranné známky, autorská práva, databázová práva a další práva duševního vlastnictví s ní související stále patří `aenniw`.

`aenniw` se zavazuje zajistit, že aplikace bude co nejužitečnější a nejefektivnější. Z tohoto důvodu si vyhrazujeme právo provádět změny v aplikaci nebo účtovat poplatky za její služby, a to kdykoli a z jakéhokoli důvodu. Nikdy vám nebudeme účtovat poplatky za aplikaci nebo její služby, aniž bychom vám jasně řekli, za co přesně platíte.

Aplikace Ambient ukládá a zpracovává osobní údaje, které jste nám poskytli, za účelem poskytování mé služby. Je vaší odpovědností udržovat svůj telefon a přístup k aplikaci v bezpečí. Proto doporučujeme, abyste svůj telefon neprováděli jailbreakem nebo rootováním, což je proces odstraňování softwarových omezení a omezení uložených oficiálním operačním systémem vašeho zařízení. Mohlo by to způsobit, že váš telefon bude zranitelný vůči malwaru/virům/škodlivým programům, ohrozí bezpečnostní funkce vašeho telefonu a může to znamenat, že aplikace Ambient nebude fungovat správně nebo vůbec.

Měli byste si být vědomi toho, že jsou určité věci, za které `aenniw` nenese odpovědnost. Některé funkce aplikace budou vyžadovat, aby aplikace měla aktivní připojení k internetu. Připojení může být Wi-Fi nebo poskytované vaším poskytovatelem mobilní sítě, ale `aenniw` nemůže převzít odpovědnost za to, že aplikace nefunguje v plné funkčnosti, pokud nemáte přístup k Wi-Fi a nemáte žádné z zbývá váš datový limit.

Pokud aplikaci používáte mimo oblast s Wi-Fi, měli byste mít na paměti, že podmínky smlouvy s vaším poskytovatelem mobilní sítě budou stále platit. V důsledku toho vám mohou být vaším mobilním operátorem účtovány poplatky za data po dobu připojení při přístupu k aplikaci nebo jiné poplatky třetích stran. Používáním aplikace přebíráte odpovědnost za veškeré takové poplatky, včetně poplatků za roamingová data, pokud aplikaci používáte mimo své domovské území (tj. region nebo zemi) bez vypnutí datového roamingu. Pokud nejste plátcem účtu za zařízení, na kterém aplikaci používáte, uvědomte si prosím, že předpokládáme, že jste od plátce účtu dostali povolení k používání aplikace.

Stejně tak `aenniw` nemůže vždy převzít odpovědnost za způsob, jakým aplikaci používáte, tj. musíte se ujistit, že vaše zařízení zůstane nabité – pokud se mu vybije baterie a nebudete jej moci zapnout, abyste mohli využívat službu, `aenniw` nemůže přijmout odpovědnost.

S ohledem na odpovědnost `aenniw` za vaše používání aplikace, když aplikaci používáte, je důležité mít na paměti, že ačkoliv se snažíme zajistit, aby byla vždy aktuální a správná, spoléháme na třetí strany, aby nám poskytly informace, abychom vám je mohli zpřístupnit. `aenniw` nenese žádnou odpovědnost za jakoukoli ztrátu, přímou nebo nepřímou, kterou zažijete v důsledku toho, že se budete plně spoléhat na tuto funkci aplikace.

V určitém okamžiku můžeme chtít aplikaci aktualizovat. Aplikace je aktuálně dostupná pro Android a iOS – požadavky na oba systémy (a na jakékoli další systémy, na které se rozhodneme rozšířit dostupnost aplikace) se mohou změnit, a pokud si chcete ponechat aktualizace, budete si muset stáhnout aktualizace. pomocí aplikace. `aenniw` neslibuje, že bude vždy aktualizovat aplikaci tak, aby byla pro vás relevantní a/nebo fungovala s verzí Android & iOS, kterou máte nainstalovanou na svém zařízení. Slibujete však, že vždy přijmete aktualizace aplikace, když vám budou nabídnuty. Můžeme si také přát ukončit poskytování aplikace a můžeme ji kdykoli ukončit, aniž bychom vám dali oznámení o ukončení. Pokud vám neřekneme jinak, při jakémkoli ukončení (a) skončí práva a licence udělené vám v těchto podmínkách; (b) musíte přestat používat aplikaci a (v případě potřeby) ji smazat ze svého zařízení.

**Změny těchto podmínek**

Naše smluvní podmínky mohu čas od času aktualizovat. Proto vám doporučujeme, abyste tuto stránku pravidelně kontrolovali kvůli případným změnám. O případných změnách vás budu informovat zveřejněním nových obchodních podmínek na této stránce.

Tyto obchodní podmínky jsou účinné od 2022-04-18

**Kontaktujte nás**

Máte-li jakékoli dotazy nebo návrhy ohledně mých Podmínek, neváhejte mě kontaktovat na aenniw@gmail.com.
import { useColorMode } from "@docusaurus/theme-common";
import useBaseUrl from "@docusaurus/useBaseUrl";
import BrowserOnly from "@docusaurus/BrowserOnly";
import React from "react";

import styles from "./ImageMarker.module.css";

export type Mark = {
  top: number;
  left: number;
};

type MarkerProps = {
  offset?: number;
  markers: Mark[];
  border?: boolean;
};

export function Marker({
  offset = 0,
  children,
  markers,
  border = true,
}: { children: JSX.Element } & MarkerProps) {
  const getItemPosition = (marker: Mark) => {
    return {
      top: `${marker.top}%`,
      left: `${marker.left}%`,
    };
  };

  return (
    <div className={styles.marker}>
      <div
        className={`${styles.marker_container} ${
          border ? styles.marker_container_highlight : undefined
        }`}
      >
        {children}
        {markers.map((marker, i) => (
          <div
            className={styles.marker_indicator}
            style={getItemPosition(marker)}
            key={i}
            data-testid="marker"
          >
            {1 + offset + i}
          </div>
        ))}
      </div>
    </div>
  );
}

export function ImageMarker({
  src,
  ...props
}: {
  src: string;
} & MarkerProps) {
  const { colorMode } = useColorMode();
  const url = useBaseUrl(
    src.replace(
      /Mobile-(Light|Dark)/g,
      colorMode === "dark" ? "Mobile-Dark" : "Mobile-Light",
    ),
  );
  return (
    <BrowserOnly>
      {() => (
        <Marker {...props}>
          <img src={url} style={{ maxWidth: "600px" }} alt="" />
        </Marker>
      )}
    </BrowserOnly>
  );
}

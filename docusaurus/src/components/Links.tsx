import Translate from "@docusaurus/Translate";
import useBaseUrl from "@docusaurus/useBaseUrl";
import React from "react";

import styles from "./Links.module.css";

type AppLinksProps = {
  justifyContent?: "flex-start" | "center";
};
export function AppLinks({
  justifyContent = "center",
}: AppLinksProps): JSX.Element {
  return (
    <div className={styles.apkLinks} style={{ justifyContent }}>
      <a
        href="https://play.google.com/store/apps/details?id=com.github.aenniw.ambient"
        target="_blank"
      >
        <img src={useBaseUrl("google-play.svg")} alt="google play logo" />
      </a>
      <a
        className={styles.webAppLink}
        href={useBaseUrl("/app")}
        target="_blank"
      >
        <Translate id="links.try-web-app">Try web App &gt;</Translate>
      </a>
    </div>
  );
}

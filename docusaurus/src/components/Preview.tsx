import { useColorMode } from "@docusaurus/theme-common";
import useBaseUrl from "@docusaurus/useBaseUrl";
import BrowserOnly from "@docusaurus/BrowserOnly";
import React from "react";

import styles from "./Preview.module.css";

export default function Preview(): JSX.Element {
  const { colorMode } = useColorMode();

  const themedLink = (src: string) =>
    src.replace(
      /theme_(light|dark)/g,
      colorMode === "dark" ? "theme_dark" : "theme_light",
    );

  const frame = useBaseUrl("frame.png");
  const fullscreen = useBaseUrl(
    themedLink("/Home.spec.ts/home.views.theme_light.should_show_devices.png"),
  );
  const screen = useBaseUrl(
    themedLink(
      "/Device.spec.ts/device.views.theme_light.should_show_dimm_options.png",
    ),
  );

  return (
    <BrowserOnly>
      {() => (
        <div className={styles.preview}>
          <div>
            <img className={styles.frame} src={frame} alt="" />
            <div className={styles.screen}>
              <img src={screen} alt="app example #2" />
            </div>
          </div>
          <div>
            <img className={styles.frame} src={frame} alt="" />
            <img
              className={styles.fullscreen}
              src={fullscreen}
              alt="app example #1"
            />
          </div>
        </div>
      )}
    </BrowserOnly>
  );
}

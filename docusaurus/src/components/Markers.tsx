import { useColorMode } from "@docusaurus/theme-common";
import React from "react";

import { ImageMarker, Mark, Marker } from "./ImageMarker";

export function Controller({ resetOnly = false }) {
  return (
    <ImageMarker
      border={false}
      markers={[
        ...(!resetOnly
          ? [
              { top: 40, left: 25 },
              { top: 18, left: 45 },
            ]
          : []),
        { top: 70, left: 48 },
        ...(!resetOnly
          ? [
              { top: 20, left: 27 },
              { top: 37, left: 70 },
            ]
          : []),
      ]}
      src="https://gitlab.com/aenniw/espressif/-/raw/main/docs/img/lights-ble.png"
    />
  );
}

export function Permissions() {
  return (
    <ImageMarker
      markers={[{ top: 62, left: 30 }]}
      src="/App-should-show-missing-permission-view-1-Mobile-Light-linux.png"
    />
  );
}

export function Location() {
  return (
    <ImageMarker
      markers={[{ top: 62, left: 30 }]}
      offset={1}
      src="/App-should-show-location-disabled-view-1-Mobile-Light-linux.png"
    />
  );
}

export function Bluetooth() {
  return (
    <ImageMarker
      markers={[{ top: 62, left: 30 }]}
      offset={2}
      src="/App-should-show-bluetooth-disabled-view-1-Mobile-Light-linux.png"
    />
  );
}

export function Discovery1() {
  return (
    <ImageMarker
      markers={[
        { top: 62, left: 25 },
        { top: 88, left: 77 },
      ]}
      src="/App-should-show-home-view-1-Mobile-Light-linux.png"
    />
  );
}

export function Discovery2() {
  return (
    <ImageMarker
      markers={[{ top: 25, left: -2 }]}
      offset={1}
      src="/label.png"
    />
  );
}

export function Discovery3() {
  return (
    <ImageMarker
      markers={[{ top: 10, left: 2 }]}
      offset={2}
      src="/Discovery-should-show-devices-1-Mobile-Light-linux.png"
    />
  );
}
export function Navigation1() {
  return (
    <ImageMarker
      markers={[
        { top: 3, left: 2 },
        { top: 8, left: 90 },
        { top: 12, left: 5 },
        { top: 15.5, left: 90 },
        { top: 20, left: 90 },
        { top: 41, left: 2 },
        { top: 88, left: 77 },
      ]}
      src="/Home-should-show-devices-1-Mobile-Light-linux.png"
    />
  );
}

export function Navigation2() {
  return (
    <ImageMarker
      markers={[
        { top: 11.5, left: -3 },
        { top: 20, left: 45 },
      ]}
      offset={7}
      src="/Device-should-show-color-options-1-Mobile-Light-linux.png"
    />
  );
}

export function Color() {
  return (
    <ImageMarker
      markers={[
        { top: 34, left: -3 },
        { top: 53, left: -3 },
        { top: 79.75, left: 1 },
      ]}
      src="/Device-should-show-color-options-1-Mobile-Light-linux.png"
    />
  );
}

export function Colors1() {
  return (
    <ImageMarker
      markers={[
        { top: 37, left: 47 },
        { top: 73.5, left: 1 },
        { top: 80.5, left: 1 },
        { top: 86.25, left: 1 },
        { top: 86.25, left: 93 },
        { top: 95, left: 22 },
      ]}
      src="/Device-should-show-colors-options-1-Mobile-Light-linux.png"
    />
  );
}

export function Colors2() {
  return (
    <ImageMarker
      markers={[
        { top: 18, left: 3 },
        { top: 29, left: 76 },
        { top: 48, left: 16 },
        { top: 95, left: 72 },
      ]}
      src="/Device-should-show-colors-palete-options-1-Mobile-Light-linux.png"
    />
  );
}

export function Dimm() {
  return (
    <ImageMarker
      markers={[
        { top: 24.5, left: -3 },
        { top: 43, left: -3 },
        { top: 70, left: 1 },
        { top: 77, left: 1 },
        { top: 84, left: 1 },
        { top: 90, left: 1 },
        { top: 90, left: 93 },
      ]}
      src="/Device-should-show-dimm-options-1-Mobile-Light-linux.png"
    />
  );
}

export function Rainbow() {
  return (
    <ImageMarker
      markers={[
        { top: 37, left: 47 },
        { top: 73.5, left: 1 },
        { top: 80.5, left: 1 },
        { top: 86.5, left: 1 },
        { top: 86.5, left: 93 },
      ]}
      src="/Device-should-show-rainbow-options-1-Mobile-Light-linux.png"
    />
  );
}

export function Configuration() {
  return (
    <ImageMarker
      markers={[
        { top: 11.5, left: -3 },
        { top: 25, left: -3 },
        { top: 39, left: -3 },
        { top: 50, left: -3 },
        { top: 60, left: -3 },
      ]}
      src="/Settings-should-show-configuration-1-Mobile-Light-linux.png"
    />
  );
}

function LightWiring({
  markers,
  svg: Svg,
}: {
  markers: Mark[];
  svg: React.ComponentType<React.ComponentProps<"svg">>;
}) {
  const { colorMode } = useColorMode();
  return (
    <Marker markers={markers} border={false}>
      <Svg
        style={{
          fill: colorMode === "light" ? "black" : "lightgray",
          maxWidth: "100%",
        }}
        role="img"
      />
    </Marker>
  );
}

export function LanternWiring() {
  return (
    <LightWiring
      svg={require("@site/resources/wiring/lanterns.svg").default}
      markers={[
        { top: 17, left: 47 },
        { top: 51, left: 6 },
        { top: 35, left: 76 },
      ]}
    />
  );
}

export function BallWiring() {
  return (
    <LightWiring
      svg={require("@site/resources/wiring/balls.svg").default}
      markers={[
        { top: 16, left: 48 },
        { top: 50, left: 7 },
        { top: 34, left: 78 },
      ]}
    />
  );
}

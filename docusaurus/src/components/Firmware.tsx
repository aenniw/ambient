import BrowserOnly from "@docusaurus/BrowserOnly";
import Translate from "@docusaurus/Translate";
import { EspLoader } from "@toit/esptool.js";
import React, { useEffect, useState } from "react";

import { SerialFailed, SerialFlashed } from "./Admonitions";
import { Sticker } from "./Sticker";

type PortInfo = { usbVendorId: number };

const Wroom32uc3ch: PortInfo = { usbVendorId: 0x1a86 };
const Mhetesp32minikit: PortInfo = { usbVendorId: 0x10c4 };

function modelName({ usbVendorId }: PortInfo) {
  return usbVendorId === Wroom32uc3ch.usbVendorId
    ? "wroom32uc3ch"
    : "mhetesp32minikit";
}

async function withSerialConnection<R>(
  fn: (l: EspLoader, info: PortInfo) => Promise<R>,
): Promise<R> {
  const port = await (navigator as any).serial.requestPort({
    filters: [Wroom32uc3ch, Mhetesp32minikit],
  });
  try {
    await port.open({ baudRate: 115200 });
    const loader = new EspLoader(port, { debug: true, logger: console });
    try {
      await loader.connect();
      return await fn(loader, port.getInfo());
    } finally {
      await loader.disconnect();
    }
  } finally {
    port.close();
  }
}

async function fetchMetadata(): Promise<[string, number, string]> {
  return withSerialConnection(async (loader, info) => {
    const macAddr = (await loader.macAddr()).split(":").reverse().join("");

    let pin = 0;
    let ssid = 0;

    pin |= Number("0x" + macAddr.substring(0, 2));
    pin |= Number("0x" + macAddr.substring(2, 4)) << 8;
    pin |= Number("0x" + macAddr.substring(4, 6)) << 16;

    ssid |= Number("0x" + macAddr.substring(6, 8));
    ssid |= Number("0x" + macAddr.substring(8, 10)) << 8;
    ssid |= Number("0x" + macAddr.substring(10, 12)) << 16;

    pin %= 1000000;
    while (pin / 100000 <= 0) {
      pin *= 10;
    }

    return [
      `Ambient-${ssid.toString(16).padStart(6, "0").toUpperCase()}`,
      pin % 1000000,
      modelName(info),
    ];
  });
}

export const CheckCredentials = () => (
  <BrowserOnly>{() => <CheckCredentialsComponent />}</BrowserOnly>
);

function CheckCredentialsComponent() {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [ssid, setSSID] = useState<string>();
  const [model, setModel] = useState<string>();
  const [pin, setPin] = useState<number>();

  useEffect(() => {
    setSSID(undefined);
    setModel(undefined);
    setPin(undefined);
    setLoading(false);
    setError(false);
  }, []);

  const detect = async () => {
    try {
      setError(false);
      setLoading(true);
      const [ssid, pin, model] = await fetchMetadata();
      setSSID(ssid);
      setModel(model);
      setPin(pin);
    } catch (e) {
      console.warn("Failed to detect controller credentials.", e);
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  if (ssid && pin && model) {
    return <Sticker ssid={ssid} pin={pin} model={model} />;
  }

  return (
    <>
      {error && <SerialFailed />}
      <div
        style={{
          width: "100%",
          height: "60px",
          display: "flex",
          justifyContent: "center",
          marginTop: "40px",
          marginBottom: "25px",
        }}
      >
        {loading ? (
          <LdsBar />
        ) : (
          <button
            onClick={detect}
            disabled={!(navigator as any).serial}
            style={{
              padding: "0 40px 0 40px",
              borderRadius: "10px",
              fontSize: "30px",
              fontWeight: 600,
            }}
          >
            <Translate id="button.detect">Detect</Translate>
          </button>
        )}
      </div>
    </>
  );
}

function LdsBar({ progress }: { progress?: number }) {
  return (
    <span className={progress ? "lds-progress" : "lds-bar"}>
      <span style={progress ? { width: `${progress}%` } : {}} />
    </span>
  );
}

async function flashFirmware(version: string, progress: (p: number) => void) {
  return withSerialConnection(async (loader, info) => {
    const resp = await fetch(
      require(
        `!file-loader!/resources/firmwares/${modelName(info)}.${version}.bin`,
      ).default,
    );
    const data = new Uint8Array(await resp.arrayBuffer());

    await loader.loadStub();
    await loader.setBaudRate(115200, 921600);
    await loader.flashData(data, 0x10000, (idx, cnt) =>
      progress((idx / cnt) * 100),
    );
    await loader.flashFinish(true);
    await new Promise((resolve) => setTimeout(resolve, 100));
  });
}

function FlashFirmwareComponent({ version }: { version: string }) {
  const [loading, setLoading] = useState<boolean>(false);
  const [progress, setProgress] = useState<number>(0);
  const [flashed, setFlashed] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    setLoading(false);
    setFlashed(false);
    setError(false);
    setProgress(0);
  }, []);

  const flash = async () => {
    try {
      setError(false);
      setProgress(0);
      setLoading(true);
      await flashFirmware(version, setProgress);
      setFlashed(true);
    } catch (e) {
      console.warn("Failed to flash controller firmware.", e);
      setError(true);
    } finally {
      setLoading(false);
    }
  };

  if (flashed) {
    return <SerialFlashed />;
  }

  return (
    <>
      {error && <SerialFailed />}
      <div
        style={{
          width: "100%",
          height: "60px",
          display: "flex",
          justifyContent: "center",
          marginTop: "40px",
          marginBottom: "25px",
        }}
      >
        {loading ? (
          <LdsBar progress={progress} />
        ) : (
          <button
            onClick={flash}
            disabled={!(navigator as any).serial}
            style={{
              padding: "0 40px 0 40px",
              borderRadius: "10px",
              fontSize: "30px",
              fontWeight: 600,
            }}
          >
            <Translate id="button.upload">Upload</Translate>
          </button>
        )}
      </div>
    </>
  );
}

export const FlashFirmware = (props: any) => (
  <BrowserOnly>{() => <FlashFirmwareComponent {...props} />}</BrowserOnly>
);

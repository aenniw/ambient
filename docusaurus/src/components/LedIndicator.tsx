import React from "react";

export function LedIndicator({ color, text }: { color: string; text: string }) {
  return (
    <span
      style={{
        backgroundColor: color,
        padding: "2px 10px",
        fontWeight: 700,
        borderRadius: 5,
        color: "white",
      }}
    >
      {text}
    </span>
  );
}

import useBaseUrl from "@docusaurus/useBaseUrl";
import React from "react";

export function Sticker({
  ssid,
  pin,
  model,
}: {
  ssid: string;
  pin: number;
  model: string;
}) {
  return (
    <svg
      {...{
        xmlns: "http://www.w3.org/2000/svg",
        xmlnsXlink: "http://www.w3.org/1999/xlink",
      }}
      viewBox="0 0 1392 542"
    >
      <image xlinkHref={useBaseUrl("/label.png")} />
      <rect x="300" y="25" width="40%" height="15%" />
      <text
        x="305"
        y="92"
        fill="white"
        style={{
          fontSize: "43",
          fontWeight: 600,
          userSelect: "none",
        }}
      >
        MODEL: {model}
      </text>
      <rect x="40" y="120" width="45%" height="25%" />
      <text
        x="40"
        y="160"
        fill="white"
        style={{
          fontSize: "43",
          fontWeight: 600,
          userSelect: "none",
        }}
      >
        SSID: {ssid}
      </text>
      <text
        x="40"
        y="210"
        fill="white"
        style={{
          fontSize: "44",
          fontWeight: 600,
          userSelect: "none",
        }}
      >
        PIN: {pin}
      </text>
    </svg>
  );
}

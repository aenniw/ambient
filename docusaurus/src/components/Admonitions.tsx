import BrowserOnly from "@docusaurus/BrowserOnly";
import Translate from "@docusaurus/Translate";
import Admonition from "@theme/Admonition";
import React from "react";

export function WebSerialWarning() {
  return (
    <BrowserOnly>
      {() =>
        !(navigator as any).serial ? (
          <Admonition type="caution">
            <Translate id="admonitions.serial.unsupported">
              Serial is not supported in the browser you use. We currently only
              support desktop versions of Google Chrome and Microsoft Edge
            </Translate>
          </Admonition>
        ) : (
          <></>
        )
      }
    </BrowserOnly>
  );
}

export function SerialFailed() {
  return (
    <Admonition type="danger">
      <Translate id="admonitions.serial.fialed">
        Serial transfer failed try restarting device and retrying the action
      </Translate>
    </Admonition>
  );
}

export function SerialFlashed() {
  return (
    <Admonition type="info">
      <Translate id="admonitions.serial.uploaded">
        New firmware sucessfully uploaded, restart device to apply changes
      </Translate>
    </Admonition>
  );
}

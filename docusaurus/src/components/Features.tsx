import Translate from "@docusaurus/Translate";
import clsx from "clsx";
import React from "react";

import styles from "./Features.module.css";

type FeatureItem = {
  title: JSX.Element;
  Svg: React.ComponentType<React.ComponentProps<"svg">>;
  description: JSX.Element;
};

const FeatureList: FeatureItem[] = [
  {
    title: <Translate id="feature.usability.title">Easy to Use</Translate>,
    Svg: require("@site/resources/easy-to-use.svg").default,
    description: (
      <Translate id="feature.usability.description">
        Designed from the ground up to be easily installed and used to get your
        lights up and running quickly.
      </Translate>
    ),
  },
  {
    title: (
      <Translate id="feature.language.title">Powered by React Native</Translate>
    ),
    Svg: require("@site/resources/react-native.svg").default,
    description: (
      <Translate id="feature.language.description">
        React Native combines the best parts of native development with React, a
        best-in-class JavaScript library for building user interfaces.
      </Translate>
    ),
  },
];

function Feature({ title, Svg, description }: FeatureItem) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function Features(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className={clsx("row", styles.featuresRow)}>
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}

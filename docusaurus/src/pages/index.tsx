import Translate from "@docusaurus/Translate";
import useBaseUrl from "@docusaurus/useBaseUrl";
import Layout from "@theme/Layout";
import React from "react";

import styles from "./index.module.css";
import Features from "../components/Features";
import { AppLinks } from "../components/Links";
import Preview from "../components/Preview";

export default function Home(): JSX.Element {
  return (
    <Layout>
      <main>
        <div className={styles.info}>
          <div>
            <h2 className={styles.header}>
              <img
                className={styles.logo}
                src={useBaseUrl("logo.svg")}
                alt="app logo"
              />
              <Translate id="headings.moto">
                Your lights all in one place
              </Translate>
            </h2>
            <AppLinks />
          </div>
          <Preview />
        </div>
        <Features />
      </main>
    </Layout>
  );
}

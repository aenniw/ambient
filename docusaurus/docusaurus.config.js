// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const darkCodeTheme = require("prism-react-renderer/themes/dracula");
const lightCodeTheme = require("prism-react-renderer/themes/github");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "Ambient",
  url: "https://aenniw.gitlab.io",
  baseUrl: "/ambient/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "favicon.png",
  staticDirectories: [
    "resources",
    "../expo/resources",
    "../playwright/tests/App.spec.ts-snapshots",
    "../playwright/tests/navigation/Device.spec.ts-snapshots",
    "../playwright/tests/navigation/Discovery.spec.ts-snapshots",
    "../playwright/tests/navigation/Home.spec.ts-snapshots",
    "../playwright/tests/navigation/Settings.spec.ts-snapshots",
  ],

  organizationName: "aenniw",
  projectName: "ambient",

  i18n: {
    defaultLocale: "en",
    locales: ["en", "cs"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          lastVersion: "current",
        },
        blog: {
          showReadingTime: false,
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      colorMode: {
        respectPrefersColorScheme: true,
      },
      navbar: {
        title: "Ambient",
        logo: {
          alt: "Ambient logo",
          src: "logo.svg",
        },
        items: [
          {
            type: "doc",
            docId: "intro/wiring",
            position: "left",
            label: "Docs",
          },
          { to: "firmware", label: "Firmware", position: "left" },
          {
            type: "docsVersionDropdown",
            position: "right",
          },
          {
            href: "https://gitlab.com/aenniw/ambient/",
            label: "GitLab",
            position: "right",
          },
          {
            type: "localeDropdown",
            position: "left",
          },
        ],
      },
      footer: {
        links: [
          {
            to: "privacy",
            position: "right",
            label: "Privacy Policy",
          },
          {
            to: "terms",
            position: "right",
            label: "Terms & Conditions",
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Ambient, Inc. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;

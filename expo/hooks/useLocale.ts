import { getLabel, LocaleSchemeName } from "@common/localization/locale";
import * as Localization from "expo-localization";
import { TranslateOptions } from "i18n-js/typings/typing";

import { useStorage } from "./useStorage";

const LOCALE_KEY = "settings.locale";

export function useLocale() {
  return useStorage<LocaleSchemeName>(LOCALE_KEY, "no-preference");
}

export function useLabel(key: string, options: TranslateOptions = {}): string {
  let [locale] = useLocale();

  if (locale === "no-preference") {
    locale = Localization.locale.substring(0, 2) as LocaleSchemeName;
  }

  return getLabel(key, Object.assign({ locale }, options));
}

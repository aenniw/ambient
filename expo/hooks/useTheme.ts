import { DarkTheme, DefaultTheme, Theme } from "@react-navigation/native";
import { useMemo } from "react";
import {
  ColorSchemeName,
  useColorScheme as useColorSchemenative,
} from "react-native";
import tinycolor from "tinycolor2";

import { useStorage } from "./useStorage";

const THEME_KEY = "settings.theme";

export function useColorScheme(): ColorSchemeName {
  let [theme] = useStorage<ColorSchemeName>(THEME_KEY, undefined);
  const nativeTheme = useColorSchemenative();

  if (!theme) {
    theme = nativeTheme;
  }

  return theme;
}

type SchemeFields = keyof typeof DefaultTheme.colors &
  keyof typeof DarkTheme.colors;

export function useThemeColor(name: SchemeFields) {
  return useTheme().colors[name];
}

export function useTheme(): Theme {
  let scheme = useColorScheme();
  if (!scheme) {
    scheme = "light";
  }

  const theme = scheme === "light" ? DefaultTheme : DarkTheme;

  return useMemo(
    () => ({
      ...theme,
      fonts: {
        regular: { fontFamily: "Roboto", fontWeight: "normal" },
        medium: { fontFamily: "Roboto", fontWeight: "normal" },
        bold: { fontFamily: "Roboto", fontWeight: "600" },
        heavy: { fontFamily: "Roboto", fontWeight: "700" },
      },
    }),
    [theme],
  );
}

export function useContrastColor(name: SchemeFields, color?: string): string {
  const themeColor = useThemeColor(name);
  if (color === undefined) {
    return themeColor;
  }

  const threshold = tinycolor.readability(color, "black");
  if (threshold >= 18) {
    return "#000000";
  }
  return "#ffffff";
}

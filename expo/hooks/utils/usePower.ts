import BleDevice from "@common/ble-plx/BleDevice";
import { UtilCharacteristics } from "@common/ble-plx/UUIDs";

import { useCharacteristic } from "../ble/useCharacteristic";

export function usePower(device: BleDevice) {
  return useCharacteristic<number>(
    device,
    UtilCharacteristics.Power,
    decode,
    encode,
  );
}

export function decode(c: string): number {
  return c[0].charCodeAt(0) + (c[1].charCodeAt(0) << 8);
}

export function encode(c: number) {
  return String.fromCharCode(c & 0xff) + String.fromCharCode(c >> 8);
}

import BleDevice from "@common/ble-plx/BleDevice";
import { UtilCharacteristics } from "@common/ble-plx/UUIDs";
import { parseSemver } from "@common/firmware";
import { useEffect, useState } from "react";

import { useConnection } from "../ble/useConnection";

export function useVersion(device: BleDevice): string | undefined {
  const [connected] = useConnection(device);
  const [version, setVersion] = useState<string>();

  useEffect(() => {
    if (connected) {
      device
        .read(UtilCharacteristics.Version)
        .then(parseSemver)
        .then(setVersion);
    } else {
      setVersion(undefined);
    }
  }, [connected]);

  return version;
}

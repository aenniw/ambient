import BleDevice from "@common/ble-plx/BleDevice";
import { UtilCharacteristics } from "@common/ble-plx/UUIDs";
import { useEffect, useState } from "react";

export function useSecret(
  device: BleDevice,
): [string, (v: string) => void, (v: string) => Promise<void>] {
  const [secret, setSecret] = useState<string>("");

  useEffect(() => {
    setSecret("");
  }, []);

  return [
    secret,
    setSecret,
    async (v: string) => {
      await device.write(UtilCharacteristics.Secret, v);
      setSecret("");
    },
  ];
}

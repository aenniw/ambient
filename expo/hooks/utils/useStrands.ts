import BleDevice from "@common/ble-plx/BleDevice";
import { PixelServices } from "@common/ble-plx/UUIDs";
import { useEffect, useState } from "react";

import { useConnection } from "../ble/useConnection";

export function useStrands(device: BleDevice) {
  const [connected] = useConnection(device);
  const [strands, setStrands] = useState<number>();

  useEffect(() => {
    if (connected) {
      device
        .serviceUUIDs()
        .then((s) => s.filter((uuid) => PixelServices.includes(uuid)).length)
        .then(setStrands);
    } else {
      setStrands(undefined);
    }
  }, [connected]);

  return strands;
}

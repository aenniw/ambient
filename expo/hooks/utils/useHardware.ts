import BleDevice from "@common/ble-plx/BleDevice";
import { UtilCharacteristics } from "@common/ble-plx/UUIDs";
import { useEffect, useState } from "react";

import { useConnection } from "../ble/useConnection";

export function useHardware(device: BleDevice): string | undefined {
  const [connected] = useConnection(device);
  const [hardware, setHardware] = useState<string>();

  useEffect(() => {
    if (connected) {
      device.read(UtilCharacteristics.Hardware).then(setHardware);
    } else {
      setHardware(undefined);
    }
  }, [connected]);

  return hardware;
}

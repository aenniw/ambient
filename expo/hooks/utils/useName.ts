import BleDevice from "@common/ble-plx/BleDevice";
import { UtilCharacteristics } from "@common/ble-plx/UUIDs";
import { useState } from "react";

export function useName(
  device: BleDevice,
): [string, (v: string) => void, () => Promise<void>] {
  const [name, setName] = useState<string>(device.getName());

  return [
    name,
    setName,
    async () => {
      await device.write(UtilCharacteristics.Name, name);
    },
  ];
}

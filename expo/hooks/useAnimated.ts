import { polar } from "@common/trigonometry";
import { useRef } from "react";
import { Animated, NativeTouchEvent } from "react-native";
import { ColorFormats } from "tinycolor2";

export function usePanAnimate(
  size: number,
  setColor: (c: ColorFormats.HSV) => void,
) {
  const x = useRef(new Animated.Value(30));
  const y = useRef(new Animated.Value(30));

  const move = Animated.event<NativeTouchEvent>(
    [{ nativeEvent: { locationX: x.current, locationY: y.current } }, null],
    {
      useNativeDriver: false,
      listener: ({ nativeEvent }) => {
        const { deg, radius } = polar(nativeEvent, size);
        const color = {
          h: deg < 0 ? 360 + deg : deg,
          s: radius,
          v: 1,
        };

        setColor(color);
      },
    },
  );

  return {
    x,
    y,
    move,
  };
}

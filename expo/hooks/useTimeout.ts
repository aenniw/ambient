import { log } from "@common/logger";
import { useEffect, useRef } from "react";

export default function useInterval(
  callback: () => Promise<void>,
  delay: number,
) {
  const savedCallback = useRef<() => Promise<void>>();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    let inProgress = false;

    function tick() {
      if (!inProgress && savedCallback.current) {
        inProgress = true;
        savedCallback
          .current()
          .catch((e) => {
            log.error("interval - callback failed", e);
          })
          .finally(() => {
            inProgress = false;
          });
      }
    }
    if (delay !== null) {
      const id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

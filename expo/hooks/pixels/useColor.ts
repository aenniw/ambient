import BleDevice from "@common/ble-plx/BleDevice";
import { PixelCharacteristics } from "@common/ble-plx/UUIDs";
import { PixelColor } from "@common/ble-plx/types";
import tinycolor from "tinycolor2";

import { useCharacteristic } from "../ble/useCharacteristic";

export function toColor(h: number, s: number): PixelColor {
  return [h, s, tinycolor({ h, s, v: 1 }).toHexString()];
}

export function useColor(
  device: BleDevice,
  strand: number,
): [PixelColor | undefined, (v: [number, number]) => Promise<void>] {
  const [color, setColor] = useCharacteristic<PixelColor>(
    device,
    PixelCharacteristics[strand].Color,
    decode,
    encode,
  );

  return [
    color,
    ([h, s]: [number, number]) => {
      h = Math.min(360, Math.round(h));
      s = Math.min(100, Math.round(s));
      return setColor(toColor(h, s));
    },
  ];
}

export function decode(bytes: string): PixelColor {
  const color: number =
    bytes.length < 2
      ? 0
      : bytes[0].charCodeAt(0) + (bytes[1].charCodeAt(0) << 8);
  return toColor(color & 0x1ff, color >> 9);
}

export function encode([hue, sat]: PixelColor): string {
  const color = (sat << 9) + hue;
  return String.fromCharCode(color & 0xff) + String.fromCharCode(color >> 8);
}

import BleDevice from "@common/ble-plx/BleDevice";
import { PixelsState } from "@common/ble-plx/types";

import { useBrightness } from "./useBrightness";
import { useColor } from "./useColor";
import { useColors } from "./useColors";
import { useEnabled } from "./useEnabled";
import { useMode } from "./useMode";
import { useState } from "./useState";
import { useConnection } from "../ble/useConnection";
import { useVersion } from "../utils/useVersion";

export function usePixel(device: BleDevice, strand: number) {
  const [connected] = useConnection(device);

  const [brightness, setBrightness] = useBrightness(device, strand);
  const [color, setColor] = useColor(device, strand);
  const [colors, setColors] = useColors(device, strand);
  const [mode, setMode, setOptions] = useMode(device, strand);
  const [state, setState] = useState(device, strand);
  const enabled = useEnabled(device, strand);
  const version = useVersion(device);

  return {
    ready:
      connected &&
      brightness !== undefined &&
      color !== undefined &&
      colors !== undefined &&
      mode !== undefined &&
      state !== undefined &&
      version !== undefined &&
      enabled !== undefined,
    connected,
    enabled,
    version,
    brightness,
    setBrightness,
    color,
    setColor,
    colors,
    setColors,
    mode,
    setMode,
    setOptions,
    state,
    setState,
    toggle: () =>
      setState(state === PixelsState.ON ? PixelsState.OFF : PixelsState.ON),
  };
}

import BleDevice from "@common/ble-plx/BleDevice";
import { CharacteristicKey, PixelCharacteristics } from "@common/ble-plx/UUIDs";
import { useEffect, useState } from "react";

import { useStrands } from "../utils/useStrands";

export function useEnabled(d: BleDevice): boolean[] | undefined;
export function useEnabled(d: BleDevice, s: number): boolean | undefined;
export function useEnabled(device: BleDevice, strand?: number) {
  const [value, setValues] = useState<boolean[]>();
  const strands = useStrands(device);

  useEffect(() => {
    readValues(
      Array.from({ length: strands || 0 }).map(
        (_, i) => PixelCharacteristics[i].Enabled,
      ),
    );
  }, [strands]);

  const readValues = async (keys: CharacteristicKey[]) => {
    if (keys.length === 0) return;

    const values: boolean[] = [];
    for (const key of keys) {
      const value = (await device.has(key)) ? await device.read(key) : "1";
      if (value === undefined) return;
      values.push(decode(value));
    }
    setValues(values);
  };

  return strand === undefined ? value : value?.[strand];
}

export function decode(c: string): boolean {
  return c[0].charCodeAt(0) !== 0;
}

export function encode(c: boolean) {
  return String.fromCharCode(c ? 1 : 0);
}

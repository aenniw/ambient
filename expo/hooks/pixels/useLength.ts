import BleDevice from "@common/ble-plx/BleDevice";
import { CharacteristicKey, PixelCharacteristics } from "@common/ble-plx/UUIDs";
import { useEffect, useState } from "react";

import { useCharacteristics } from "../ble/useCharacteristic";
import { useStrands } from "../utils/useStrands";

export function useLength(device: BleDevice) {
  const [keys, setKeys] = useState<CharacteristicKey[]>([]);
  const strands = useStrands(device);

  useEffect(() => {
    setKeys(
      Array.from({ length: strands || 0 }).map(
        (_, i) => PixelCharacteristics[i].Length,
      ),
    );
  }, [strands]);

  return useCharacteristics<number>(device, keys, decode, encode);
}

export function decode(c: string): number {
  return c[0].charCodeAt(0);
}

export function encode(c: number) {
  return String.fromCharCode(c);
}

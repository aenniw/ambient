import BleDevice from "@common/ble-plx/BleDevice";
import { PixelCharacteristics } from "@common/ble-plx/UUIDs";
import { PixelsMode, PixelsOptions } from "@common/ble-plx/types";

import { useCharacteristic } from "../ble/useCharacteristic";

export const PIXEL_MODES = [
  PixelsMode.STATIC,
  PixelsMode.FADE,
  PixelsMode.RAINBOW,
  PixelsMode.TRANSITION,
];

export function useMode(
  device: BleDevice,
  strand: number,
): [
  [PixelsMode, PixelsOptions] | undefined,
  (m: PixelsMode) => Promise<void>,
  (d: PixelsOptions) => Promise<void>,
] {
  const [mode, setMode] = useCharacteristic<[PixelsMode, PixelsOptions]>(
    device,
    PixelCharacteristics[strand].Mode,
    decode,
    encode,
  );

  return [
    mode,
    async (m: PixelsMode) => {
      if (mode) {
        await setMode([m, mode[1]]);
      }
    },
    async (o: PixelsOptions) => {
      if (mode) {
        await setMode([mode[0], o]);
      }
    },
  ];
}

export function decode(v: string): [PixelsMode, PixelsOptions] {
  const bytes: number[] = v.split("").map((c) => c.charCodeAt(0));

  return [
    bytes[0],
    {
      duration: bytes[1] + (bytes[2] << 8),
      chained: (bytes[3] & 0x1) !== 0,
      randomized: (bytes[3] & 0x2) !== 0,
      partition: (bytes[3] >> 2) & 0xf,
    },
  ];
}

export function encode([t, { duration, chained, randomized, partition }]: [
  PixelsMode,
  PixelsOptions,
]) {
  return (
    String.fromCharCode(t) +
    String.fromCharCode(duration & 0xff) +
    String.fromCharCode(duration >> 8) +
    String.fromCharCode(
      (chained ? 0x1 : 0) + (randomized ? 0x2 : 0) + (partition << 2),
    ) +
    String.fromCharCode(0)
  );
}

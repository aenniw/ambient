import BleDevice from "@common/ble-plx/BleDevice";
import { PixelCharacteristics } from "@common/ble-plx/UUIDs";
import { PixelsState } from "@common/ble-plx/types";

import { useCharacteristic } from "../ble/useCharacteristic";

export function useState(device: BleDevice, strand: number) {
  return useCharacteristic<PixelsState>(
    device,
    PixelCharacteristics[strand].State,
    decode,
    encode,
  );
}

export function decode(c: string): PixelsState {
  return c[0].charCodeAt(0);
}

export function encode(c: PixelsState) {
  return String.fromCharCode(c);
}

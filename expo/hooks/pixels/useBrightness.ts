import BleDevice from "@common/ble-plx/BleDevice";
import { PixelCharacteristics } from "@common/ble-plx/UUIDs";

import { useCharacteristic } from "../ble/useCharacteristic";

/* 0-100 */
export function useBrightness(
  device: BleDevice,
  strand: number,
): [number | undefined, (v: number) => Promise<void>] {
  const [brightness, setBrightness] = useCharacteristic<number>(
    device,
    PixelCharacteristics[strand].Brightness,
    decode,
    encode,
  );

  return [
    brightness,
    (b: number) => setBrightness(Math.min(100, Math.round(b))),
  ];
}

export function decode(v: string): number {
  return Math.round(v[0].charCodeAt(0) / 2.55);
}

export function encode(b: number) {
  return String.fromCharCode(Math.min(Math.round(b * 2.55), 255));
}

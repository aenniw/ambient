import BleDevice from "@common/ble-plx/BleDevice";
import { PixelCharacteristics } from "@common/ble-plx/UUIDs";
import { PixelColor } from "@common/ble-plx/types";

import {
  decode as decodeColor,
  encode as encodeColor,
  toColor,
} from "./useColor";
import { useCharacteristic } from "../ble/useCharacteristic";

export function useColors(
  device: BleDevice,
  strand: number,
): [PixelColor[] | undefined, (colors: PixelColor[]) => Promise<void>] {
  const [colors, setColors] = useCharacteristic<PixelColor[]>(
    device,
    PixelCharacteristics[strand].Colors,
    decode,
    encode,
  );

  return [
    colors,
    (colors: PixelColor[]) => setColors(colors.map((c) => toColor(c[0], c[1]))),
  ];
}

export function decode(n: string): PixelColor[] {
  const colors: PixelColor[] = [];
  for (let i = 0; i < n.length; i += 2) {
    const color = n.substr(i, 2);
    colors.push(decodeColor(color));
  }
  return colors;
}

export function encode(c: PixelColor[]): string {
  return c.map(encodeColor).join("");
}

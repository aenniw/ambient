import BleDevice from "@common/ble-plx/BleDevice";
import { useFocusEffect } from "@react-navigation/core";
import { useCallback, useEffect, useState } from "react";

export function useConnection(device: BleDevice): [boolean | undefined] {
  const [value, setValue] = useState<boolean>();

  const readState = () => setValue(device.isConnected());

  useFocusEffect(useCallback(readState, []));
  useEffect(() => {
    const cb = device.onConnection(setValue);
    return () => cb();
  }, []);
  useEffect(readState, []);

  return [value];
}

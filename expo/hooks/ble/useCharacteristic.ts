import BleDevice from "@common/ble-plx/BleDevice";
import { CharacteristicKey } from "@common/ble-plx/UUIDs";
import { useFocusEffect } from "@react-navigation/core";
import { useCallback, useEffect, useState } from "react";

import { useConnection } from "./useConnection";

export function useCharacteristic<R>(
  device: BleDevice,
  k: CharacteristicKey,
  decode: (v: string) => R,
  encode: (v: R) => string,
): [R | undefined, (v: R) => Promise<void>] {
  const [key] = useState([k]);
  const [value, setValue] = useCharacteristics(device, key, decode, encode);

  return [value?.[0], (v: R) => setValue(0, v)];
}

export function useCharacteristics<R>(
  device: BleDevice,
  keys: CharacteristicKey[],
  decode: (v: string) => R,
  encode: (v: R) => string,
): [R[] | undefined, (i: number, v: R) => Promise<void>] {
  const [values, setValues] = useState<(R | undefined)[]>([]);
  const [connected] = useConnection(device);

  const readValues = () => {
    if (!connected) {
      setValues([]);
    } else {
      keys.forEach((k, i) =>
        device.read(k).then(
          (v) => onRead(i, v),
          () => onRead(i),
        ),
      );
    }
  };
  const setValue = (index: number, v?: R) =>
    setValues((values) => {
      const next = [
        ...(values.length === 0
          ? Array.from<undefined>({ length: keys.length })
          : values),
      ];

      next[index] = v;

      return next;
    });

  useFocusEffect(useCallback(readValues, [connected, keys]));
  useEffect(readValues, [connected, keys]);
  useEffect(() => {
    const cbs = keys.map((k, i) => device.onChange(k, (v) => onRead(i, v)));

    return () => cbs.forEach((cb) => cb());
  }, [connected, keys]);

  const onRead = (index: number, v?: string) => {
    if (v !== undefined) {
      setValue(index, decode(v));
    } else {
      setValue(index);
    }
  };

  const loaded = values.length !== 0 && !values.includes(undefined);
  return [
    loaded ? (values as R[]) : undefined,
    async (i, v) => {
      setValue(i, v);
      await device.write(keys[i], encode(v));
    },
  ];
}

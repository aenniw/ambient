import { log } from "@common/logger";
import { useCallback, useEffect, useState } from "react";
import { BleManager, LogLevel, State } from "react-native-ble-plx";

export type BleState = "ready" | "error" | "no-connectivity" | "no-location";

export function useBle(
  ble: BleManager,
): [BleState | undefined, (s: BleState) => void] {
  const [state, setState] = useState<BleState>();

  const setConnectivity = useCallback((s?: State) => {
    log.info(`ble - state ${s}`);
    if (s === State.PoweredOn) {
      setState("ready");
    } else if (s === State.PoweredOff || State.Unsupported || State.Resetting) {
      setState("no-connectivity");
    } else {
      setState("error");
    }
  }, []);

  useEffect(() => {
    ble.setLogLevel(__DEV__ ? LogLevel.Debug : LogLevel.Error);
    const cb = ble.onStateChange(setConnectivity);

    return () => cb.remove();
  }, [ble, setConnectivity]);

  useEffect(() => {
    if (state === undefined) {
      ble.state().then(setConnectivity);
    }
  }, [state, ble, setConnectivity]);

  return [state, setState];
}

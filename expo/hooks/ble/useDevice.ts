import { useBleContext } from "@contexts/bleContext";
import { useGlobalSearchParams } from "expo-router";
import { useMemo } from "react";

export function useDevice() {
  const { getDevice } = useBleContext();
  const { id, strand } = useGlobalSearchParams<{
    id: string;
    strand: string;
  }>();

  return useMemo(
    () => ({
      device: id ? getDevice(id) : undefined,
      strand: strand !== undefined ? Number.parseInt(strand, 10) : undefined,
    }),
    [id, strand, getDevice],
  );
}

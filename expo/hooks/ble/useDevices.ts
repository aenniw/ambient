import BleDevice from "@common/ble-plx/BleDevice";
import { log } from "@common/logger";
import { useCallback, useEffect, useState } from "react";
import {
  BleError,
  BleErrorCode,
  BleManager,
  Device,
  ScanCallbackType,
  ScanMode,
} from "react-native-ble-plx";

import { BleState } from "./useBle";

const scanOptions = {
  callbackType: ScanCallbackType.AllMatches,
  scanMode: ScanMode.Balanced,
};

export function useDevices(
  ble: BleManager,
  manufacturer: string,
  setBleState: (s: BleState) => void,
): [BleDevice[], (s: boolean) => void] {
  const [state, setState] = useState<boolean>(false);
  const [devices, setDevices] = useState<BleDevice[]>([]);

  const onNewDevice = useCallback(
    (error: BleError | null, device: Device | null) => {
      if (error) {
        if (error.errorCode === BleErrorCode.LocationServicesDisabled) {
          setBleState("no-location");
        } else {
          log.warn(`ble - scan ${error.errorCode}`);
          ble.disable();
        }
      } else if (
        device &&
        device.localName &&
        device.manufacturerData === manufacturer
      ) {
        setDevices((devices) => {
          const name = device.localName || "";
          const oldDevice = devices.find(({ id }) => id === device.id);
          if (oldDevice) {
            if (oldDevice.getName() !== name) {
              log.info(`ble - name changed ${device.id}`);
              oldDevice.setName(name);
            }
            return devices;
          }
          log.info(`ble - found ${device.id}`);
          return [...devices, new BleDevice(device)];
        });
      }
    },
    [ble, manufacturer, setBleState],
  );

  useEffect(() => {
    if (state) {
      log.info("ble - start scan");
      ble.startDeviceScan([], scanOptions, onNewDevice);
    } else {
      log.info("ble - disable scan");
      ble.stopDeviceScan();
    }
  }, [state, ble, onNewDevice]);

  useEffect(() => setState(false), []);

  return [devices, setState];
}

import { check, request, onChange, AppPermissions } from "@common/permissions";
import { useEffect, useState } from "react";

export function usePermissions(): [
  AppPermissions | undefined,
  () => Promise<void>,
] {
  const [state, setState] = useState<AppPermissions>();

  useEffect(() => {
    const cb = onChange(setState);
    check().then(setState, () => setState("prompt"));
    return () => cb();
  }, []);

  return [state, async () => setState(await request())];
}

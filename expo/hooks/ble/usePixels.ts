import BleDevice from "@common/ble-plx/BleDevice";
import { encodeb64 } from "@common/common";
import { log } from "@common/logger";
import { useCallback, useEffect, useState } from "react";
import { BleManager } from "react-native-ble-plx";

import { BleState } from "./useBle";
import { useDevices } from "./useDevices";
import { useStorage } from "../useStorage";
import useInterval from "../useTimeout";

export const manufacturer = encodeb64("0242ac130003");

export function usePixels(
  ble: BleManager,
  setBleState: (s: BleState) => void,
  persistence?: boolean,
) {
  const [devices, setScanning] = useDevices(ble, manufacturer, setBleState);
  const [knownIds, setKnownIds] = useStorage<string[]>(
    "devices-ble",
    [],
    !persistence,
  );

  const [known, setKnown] = useState<BleDevice[]>([]);
  const [unknown, setUnknown] = useState<BleDevice[]>([]);

  useEffect(() => {
    setUnknown([]);
    setKnown([]);
  }, []);

  useEffect(() => {
    const known = [],
      unknown = [],
      sorted = devices.sort((a, b) => a.id.localeCompare(b.id));

    for (const device of sorted) {
      if (knownIds.includes(device.id)) {
        known.push(device);
      } else {
        unknown.push(device);
      }
    }

    setKnown(known);
    setUnknown(unknown);
  }, [devices, knownIds]);

  useInterval(async () => {
    for (const device of known) {
      if (device.isConnected()) {
        continue;
      }

      const [attached, connected] = await device.connect();
      if (attached && !connected) {
        setKnownIds(knownIds.filter((id) => id !== device.id));
        log.info(`pixels - revoked - ${device.id} - ${[attached, connected]}`);
      } else if (!attached) {
        log.info(`pixels - offline - ${device.id} - ${[attached, connected]}`);
      } else {
        log.info(
          `pixels - connected - ${device.id} - ${[attached, connected]}`,
        );
      }
    }
  }, 1000);

  const addKnownId = useCallback(
    (id: string) => setKnownIds([...knownIds, id]),
    [knownIds, setKnownIds],
  );

  return {
    known,
    unknown,
    setScanning,
    addKnownId,
  };
}

import TestDevice from "@common/ble-plx/TestDevice";
import { PixelServices, UtilService } from "@common/ble-plx/UUIDs";
import { WebBleDevice } from "@common/ble-plx/WebBleDevice";
import { IDevice } from "@common/ble-plx/WebDevice";
import { WebBleManager } from "@common/ble-plx/WebManager";
import { DeviceConfig } from "@common/ble-plx/types";
import { useStorage } from "@hooks/useStorage";
import { useEffect, useMemo } from "react";
import { Platform } from "react-native";
import { BleManager } from "react-native-ble-plx";

import { manufacturer } from "./usePixels";

export function useBleManager(): {
  bleManager: BleManager;
  discoverDevice?: () => Promise<void>;
} {
  const [mocks] = useStorage<{ id: string; config: DeviceConfig }[]>(
    "devices-mock",
    [],
  );

  const { bleManager, discover } = useMemo(() => {
    if (Platform.OS === "web") {
      const bleManager = WebBleManager();
      return {
        bleManager,
        discover: (device: IDevice) => bleManager.discover(device),
      };
    }
    return { bleManager: new BleManager(), discover: undefined };
  }, []);

  useEffect(() => {
    if (!discover) return;
    (window as any).ble = bleManager;

    if (!__DEV__) return;
    mocks.forEach(({ id, config = {} }) =>
      discover(new TestDevice(id, manufacturer, bleManager).setConfig(config)),
    );
  }, [bleManager, discover, mocks]);

  return useMemo(
    () => ({
      bleManager,
      discoverDevice:
        navigator.bluetooth && discover
          ? async () => {
              const device = await navigator.bluetooth.requestDevice({
                filters: [{ services: [UtilService] }],
                optionalServices: [...PixelServices, UtilService],
              });
              discover(new WebBleDevice(device, manufacturer, bleManager));
            }
          : undefined,
    }),
    [bleManager, discover],
  );
}

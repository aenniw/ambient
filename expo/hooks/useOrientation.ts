import { unlockAsync } from "expo-screen-orientation";
import { useWindowDimensions, Platform } from "react-native";

if (Platform.OS !== "web") {
  unlockAsync();
}

type OrientationInfo = {
  landscape: boolean;
  porttrait: boolean;
};
export function useOrientation(): OrientationInfo {
  const { width, height } = useWindowDimensions();

  return {
    landscape: width > height,
    porttrait: width <= height,
  };
}

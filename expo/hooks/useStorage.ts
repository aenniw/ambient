import AsyncStorage from "@react-native-async-storage/async-storage";
import { useCallback, useEffect, useState } from "react";

type setArg<T> = T | ((c: T) => T);
type setValue<T> = (c: setArg<T>) => void;

export function useStorage<T>(
  key: string,
  init: T,
  readOnly?: boolean,
): [T, setValue<T>] {
  const [value, setValue] = useState<T>(init);

  useEffect(() => {
    AsyncStorage.getItem(key)
      .then((v) => {
        return v === null ? init : JSON.parse(v);
      })
      .then(setValue);
  }, []);

  const persistValue = useCallback(
    async (v: setArg<T>) => {
      const valueToStore = v instanceof Function ? v(value) : v;
      if (!readOnly) {
        await AsyncStorage.setItem(key, JSON.stringify(valueToStore));
      }

      setValue(valueToStore);
    },
    [value, key, readOnly],
  );

  return [value, persistValue];
}

/// <reference types="web-bluetooth" />

import { View } from "@components/View";
import { FontsProvider, useFontsContext } from "@contexts/fontContext";
import { useTheme } from "@hooks/useTheme";
import { ThemeProvider } from "@react-navigation/native";
import { Slot } from "expo-router";
import * as SplashScreen from "expo-splash-screen";
import { StatusBar } from "expo-status-bar";
import React, { useCallback } from "react";
import { StyleSheet } from "react-native";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import { SafeAreaProvider } from "react-native-safe-area-context";

SplashScreen.preventAutoHideAsync();

SplashScreen.setOptions({
  duration: 1000,
  fade: true,
});

export { ErrorBoundary } from "expo-router";

export default function App() {
  const theme = useTheme();

  return (
    <SafeAreaProvider>
      <FontsProvider>
        <ThemeProvider value={theme}>
          <GestureHandlerRootView style={styles.container}>
            <SplashScreenSlot />
            <StatusBar
              style={theme.dark ? "light" : "dark"}
              backgroundColor={theme.colors.background}
            />
          </GestureHandlerRootView>
        </ThemeProvider>
      </FontsProvider>
    </SafeAreaProvider>
  );
}

function SplashScreenSlot() {
  const { loaded } = useFontsContext();

  const onLayout = useCallback(() => {
    if (loaded) {
      SplashScreen.hide();
    }
  }, [loaded]);

  return (
    <View style={styles.container} onLayout={onLayout}>
      <Slot />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

import BleDevice from "@common/ble-plx/BleDevice";
import { Loading } from "@components/Loading";
import { useDevice } from "@hooks/ble/useDevice";
import { usePixel } from "@hooks/pixels/usePixel";
import { DeviceColors } from "@navigation/device/Palette";
import React from "react";

type DeviceProps = {
  device: BleDevice;
  strand: number;
};

function Colors({ device, strand }: DeviceProps) {
  const { colors, setColors } = usePixel(device, strand);

  return <DeviceColors colors={colors} setColors={setColors} />;
}

const MemoizedColors = React.memo(() => {
  const { device, strand } = useDevice();
  if (!device || strand === undefined) {
    return <Loading />;
  }
  return <Colors device={device} strand={strand} />;
});
export default MemoizedColors;

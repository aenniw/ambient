import BleDevice from "@common/ble-plx/BleDevice";
import { PixelsMode, PixelsState } from "@common/ble-plx/types";
import { DeviceOff, NoPixels, PixelsOff } from "@components/InfoView";
import { Loading } from "@components/Loading";
import { View } from "@components/View";
import { useDevice } from "@hooks/ble/useDevice";
import { usePixel } from "@hooks/pixels/usePixel";
import { useLabel } from "@hooks/useLocale";
import { useOrientation } from "@hooks/useOrientation";
import { useThemeColor } from "@hooks/useTheme";
import { DeviteModes } from "@navigation/device/Modes";
import {
  MaterialTopTabNavigationEventMap,
  MaterialTopTabNavigationOptions,
  createMaterialTopTabNavigator,
} from "@react-navigation/material-top-tabs";
import { ParamListBase, TabNavigationState } from "@react-navigation/native";
import { Stack, router, withLayoutContext } from "expo-router";
import React from "react";
import { StyleSheet } from "react-native";

const { Navigator } = createMaterialTopTabNavigator();

export const MaterialTopTabs = withLayoutContext<
  MaterialTopTabNavigationOptions,
  typeof Navigator,
  TabNavigationState<ParamListBase>,
  MaterialTopTabNavigationEventMap
>(Navigator);

type LayoutProps = {
  device: BleDevice;
  strand: number;
};
function Layout({ device, strand }: LayoutProps) {
  const { landscape } = useOrientation();
  const backgound = useThemeColor("background");
  const text = useThemeColor("text");

  const { ready, connected, enabled, state, mode, setMode, setState } =
    usePixel(device, strand);

  if (connected === false) {
    return <DeviceOff onPress={router.dismissAll} />;
  }

  if (enabled === false) {
    return <NoPixels onPress={router.dismissAll} />;
  }

  if (state === PixelsState.OFF) {
    return <PixelsOff onPress={() => setState(PixelsState.ON)} />;
  }

  if (!ready) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      {!landscape && (
        <DeviteModes style={styles.modes} mode={mode} setMode={setMode} />
      )}
      <MaterialTopTabs
        tabBarPosition="bottom"
        screenOptions={{
          swipeEnabled: true,
          tabBarShowLabel: false,
          tabBarIndicatorStyle: {
            backgroundColor: text,
          },
          tabBarStyle: {
            height: !mode || mode[0] !== PixelsMode.TRANSITION ? 0 : 2,
            overflow: "hidden",
          },
          sceneStyle: { backgroundColor: backgound },
        }}
      >
        <MaterialTopTabs.Screen name="color" />
        <MaterialTopTabs.Screen
          name="colors"
          redirect={mode && mode[0] !== PixelsMode.TRANSITION}
        />
      </MaterialTopTabs>
    </View>
  );
}

const MemoizedLayout = React.memo(() => {
  const deviceTitle = useLabel("routes.device");
  const { device, strand } = useDevice();
  if (!device || strand === undefined) {
    return <Loading />;
  }
  return (
    <>
      <Stack.Screen options={{ title: deviceTitle, headerShown: true }} />
      <Layout device={device} strand={strand} />
    </>
  );
});
export default MemoizedLayout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  modes: {
    marginVertical: 15,
  },
});

import BleDevice from "@common/ble-plx/BleDevice";
import { Loading } from "@components/Loading";
import { useDevice } from "@hooks/ble/useDevice";
import { usePixel } from "@hooks/pixels/usePixel";
import { DeviceControl } from "@navigation/device/Controll";
import React from "react";

type ColorProps = {
  device: BleDevice;
  strand: number;
};
function Color({ device, strand }: ColorProps) {
  const {
    version,
    mode,
    color,
    brightness,
    setMode,
    setOptions,
    setColor,
    setBrightness,
  } = usePixel(device, strand);

  return (
    <DeviceControl
      version={version}
      color={color}
      setColor={setColor}
      brightness={brightness}
      setBrightness={setBrightness}
      mode={mode}
      setMode={setMode}
      setOptions={setOptions}
    />
  );
}

const MemoizedColor = React.memo(() => {
  const { device, strand } = useDevice();
  if (!device || strand === undefined) {
    return <Loading />;
  }
  return <Color device={device} strand={strand} />;
});
export default MemoizedColor;

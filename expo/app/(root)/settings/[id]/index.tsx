import BleDevice from "@common/ble-plx/BleDevice";
import { DeviceOff } from "@components/InfoView";
import { DevicePin } from "@components/InputPin";
import InputSpinner from "@components/InputSpinner";
import { Loading } from "@components/Loading";
import { Text } from "@components/Text";
import { useConnection } from "@hooks/ble/useConnection";
import { useDevice } from "@hooks/ble/useDevice";
import { useLength } from "@hooks/pixels/useLength";
import { useBackButton } from "@hooks/useBackButton";
import { useLabel } from "@hooks/useLocale";
import { useThemeColor } from "@hooks/useTheme";
import { useHardware } from "@hooks/utils/useHardware";
import { useName } from "@hooks/utils/useName";
import { usePower } from "@hooks/utils/usePower";
import { useSecret } from "@hooks/utils/useSecret";
import { useVersion } from "@hooks/utils/useVersion";
import { ChipFirmware } from "@navigation/settings/Firmware";
import { Setting } from "@navigation/settings/Setting";
import { Stack, router } from "expo-router";
import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, TextInput } from "react-native";

type PixelsLengthProps = {
  strand: number;
  disabled: boolean;
  length: number;
  setLength: (v: number) => void;
};
function PixelsLength({
  strand,
  disabled,
  length,
  setLength,
}: PixelsLengthProps) {
  return (
    <Setting
      label="description.size"
      description="label.strand"
      descriptionOptions={{ strand: strand + 1 }}
      icon="string-lights"
    >
      <InputSpinner
        max={60}
        min={0}
        step={4}
        value={length}
        onChange={setLength}
        disabled={disabled}
        testID={`strand-${strand}-length`}
      />
    </Setting>
  );
}

type SettingsProps = {
  device: BleDevice;
};
function Settings({ device }: SettingsProps) {
  const [disabled, setDisabled] = useState(false);
  const [name, setName, submitName] = useName(device);
  const [secret, setSecret, submitSecret] = useSecret(device);
  const [power, setPower] = usePower(device);
  const [connected] = useConnection(device);
  const [lengths, setLength] = useLength(device);
  const version = useVersion(device);
  const hardware = useHardware(device);
  const textColor = useThemeColor("text");
  const settingsTitle = useLabel("routes.settings");

  useEffect(() => setDisabled(false), []);
  useBackButton(() => {
    device.cancelUpgrade();
    return false;
  });

  if (!connected) {
    return <DeviceOff onPress={router.dismissAll} />;
  }

  if (!power || !lengths || !version || !hardware) {
    return <Loading />;
  }

  return (
    <>
      <Stack.Screen options={{ title: settingsTitle, headerShown: true }} />
      <ScrollView contentContainerStyle={styles.content}>
        <Setting label="label.name" icon="rename-box">
          <TextInput
            onChangeText={setName}
            onEndEditing={submitName}
            value={name}
            maxLength={30}
            keyboardType="ascii-capable"
            editable={!disabled}
            style={[
              styles.name,
              {
                color: textColor,
                borderColor: textColor,
              },
            ]}
            testID="input-name"
          />
        </Setting>

        <Setting label="label.pin" icon="security">
          <DevicePin
            onFulfill={submitSecret}
            value={secret}
            onTextChange={setSecret}
            editable={!disabled}
          />
        </Setting>

        <Setting
          label="label.power"
          description="description.power"
          icon="lightning-bolt"
        >
          <InputSpinner
            max={2500}
            min={1000}
            step={100}
            value={power}
            onChange={setPower}
            disabled={disabled}
            testID="power"
          />
        </Setting>

        {lengths.map((length, strand) => (
          <PixelsLength
            key={strand}
            strand={strand}
            disabled={disabled}
            length={length}
            setLength={(v) => setLength(strand, v)}
          />
        ))}

        <Setting
          label="label.firmware"
          description={<Text>{version}</Text>}
          icon="chip"
        >
          <ChipFirmware
            device={device}
            version={version}
            hardware={hardware}
            onUpgrade={setDisabled}
          />
        </Setting>
      </ScrollView>
    </>
  );
}

const MemoizedSettings = React.memo(() => {
  const { device } = useDevice();
  if (!device) {
    return <Loading />;
  }
  return <Settings device={device} />;
});
export default MemoizedSettings;

const styles = StyleSheet.create({
  content: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  name: {
    width: 240,
    borderBottomWidth: 2,
    fontSize: 20,
    paddingBottom: 4,
    paddingTop: 6,
    letterSpacing: 1.5,
    textAlign: "center",
  },
});

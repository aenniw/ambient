import {
  PermissionsNotSupported,
  NoPermissions,
  NoLocation,
  NoBluetooth,
} from "@components/InfoView";
import { Loading } from "@components/Loading";
import { BleProvider, useBleContext } from "@contexts/bleContext";
import { usePermissions } from "@hooks/ble/usePermissions";
import { Stack } from "expo-router";
import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

export { ErrorBoundary } from "expo-router";

export default function Layout() {
  const [permissions, grantPermissions] = usePermissions();

  return (
    <SafeAreaView style={styles.content}>
      {permissions === undefined && <Loading />}
      {permissions === "prompt" && <NoPermissions onPress={grantPermissions} />}
      {permissions === "denied" && (
        <NoPermissions onPress={grantPermissions} disabled />
      )}
      {permissions === "not-supported" && <PermissionsNotSupported />}
      {permissions === "granted" && (
        <BleProvider>
          <BleLayout />
        </BleProvider>
      )}
    </SafeAreaView>
  );
}

function BleLayout() {
  const { isLoading, ble, enableLocation, enableBluetooth } = useBleContext();

  if (isLoading || !ble) {
    return <Loading />;
  }
  if (ble === "no-location") {
    return <NoLocation onPress={enableLocation} />;
  }
  if (ble !== "ready") {
    return <NoBluetooth onPress={enableBluetooth} />;
  }

  return (
    <Stack
      initialRouteName="home"
      screenOptions={{
        headerShown: false,
        headerTitleAlign: "center",
      }}
    />
  );
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
});

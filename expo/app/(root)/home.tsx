import BleDevice from "@common/ble-plx/BleDevice";
import { IconButton } from "@components/Button";
import { NoControll } from "@components/InfoView";
import { View } from "@components/View";
import { useBleContext } from "@contexts/bleContext";
import { useOrientation } from "@hooks/useOrientation";
import { useThemeColor } from "@hooks/useTheme";
import { PixelsControll } from "@navigation/home/PixelsControll";
import { Stack, router } from "expo-router";
import React from "react";
import { ScrollView, StyleSheet } from "react-native";

function Home() {
  const { known: devices, discover } = useBleContext();
  const { landscape } = useOrientation();
  const primaryColor = useThemeColor("primary");

  const onSearch = () => {
    router.navigate("discovery");
    if (discover) {
      discover();
    }
  };
  const onSelect = (device: BleDevice, strand: number) =>
    router.navigate(`/device/${device.id}/${strand}`);

  return (
    <>
      <Stack.Screen options={{ headerShown: false }} />
      <View style={styles.container}>
        {!devices.length ? (
          <NoControll />
        ) : (
          <ScrollView
            contentContainerStyle={[
              styles.content,
              {
                paddingBottom: landscape ? 20 : 80,
              },
            ]}
          >
            {devices.map((device) => (
              <PixelsControll
                key={device.id}
                device={device}
                onPress={onSelect}
              />
            ))}
          </ScrollView>
        )}

        <View style={styles.fab}>
          <IconButton
            style={{
              flex: 1,
              backgroundColor: primaryColor,
              borderRadius: 25,
              alignItems: "center",
              justifyContent: "center",
            }}
            onPress={onSearch}
            name="add"
          />
        </View>
      </View>
    </>
  );
}

const MemoizedHome = React.memo(Home);
export default MemoizedHome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    paddingTop: 20,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
  fab: {
    width: 50,
    height: 50,
    position: "absolute",
    right: 30,
    bottom: 30,
    backgroundColor: "transparent",
  },
});

import BleDevice from "@common/ble-plx/BleDevice";
import { View } from "@components/View";
import { useBleContext } from "@contexts/bleContext";
import { useBackButton } from "@hooks/useBackButton";
import { useLabel } from "@hooks/useLocale";
import useInterval from "@hooks/useTimeout";
import { DevicePairing } from "@navigation/discovery/DevicePairing";
import { PairableDevices } from "@navigation/discovery/PairableDevices";
import { Stack, router } from "expo-router";
import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";

function Discovery() {
  const { unknown: devices, setScanning, addDevice } = useBleContext();
  const [unknownDevices, setUnknownDevices] = useState<BleDevice[]>([]);
  const [device, setDevice] = useState<BleDevice>();
  const discoveryTitle = useLabel("routes.discovery");

  useBackButton(() => {
    if (device) {
      cancelPairing();
      return true;
    }
    return false;
  });

  useEffect(() => {
    setDevice(undefined);
    setUnknownDevices([]);
  }, []);

  useInterval(async () => {
    const unknownDevices = [];
    for (const device of devices) {
      if (await device.attach()) {
        unknownDevices.push(device);
      }
    }
    setUnknownDevices(unknownDevices);
  }, 1000);

  const onPairing = (d: BleDevice) => {
    setScanning(false);
    setDevice(d);
  };
  const cancelPairing = () => {
    setScanning(true);
    setDevice(undefined);
    router.back();
  };
  const sucessPairing = ({ id }: BleDevice) => {
    setScanning(true);
    addDevice(id);
  };

  return (
    <>
      <Stack.Screen options={{ title: discoveryTitle, headerShown: true }} />
      <View style={styles.container}>
        {device ? (
          <DevicePairing
            device={device}
            onBack={cancelPairing}
            onSuccess={sucessPairing}
          />
        ) : (
          <PairableDevices devices={unknownDevices} onPair={onPairing} />
        )}
      </View>
    </>
  );
}

const MemoizedDiscovery = React.memo(Discovery);
export default MemoizedDiscovery;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

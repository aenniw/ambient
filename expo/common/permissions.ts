import { Platform } from "react-native";

export type AppPermissions = PermissionState | "not-supported";
type Callback = (v: AppPermissions) => void;

const listeners: Callback[] = [];

if (Platform.OS === "web" && __DEV__) {
  (window as any).permission = (v: AppPermissions) =>
    listeners.forEach((cb) => cb(v));
}

export interface IPermissions {
  check: () => Promise<AppPermissions>;
  request: () => Promise<AppPermissions>;
  onChange: (cb: Callback) => () => void;
}

function permissions(): IPermissions {
  const onChange = (cb: Callback) => {
    listeners.push(cb);

    return () => {
      const index = listeners.indexOf(cb);
      if (index >= 0) {
        listeners.splice(index, 1);
      }
    };
  };

  if (Platform.OS !== "web") {
    const {
      check,
      requestMultiple,
      RESULTS,
      PERMISSIONS,
      Permission,
      PermissionStatus,
    } = require("react-native-permissions");

    const REQUIRED_PERMISSIONS: (typeof Permission)[] =
      Platform.OS === "android"
        ? [
            PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
            PERMISSIONS.ANDROID.BLUETOOTH_CONNECT,
            PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
          ]
        : [PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL];

    const accepted = (status: typeof PermissionStatus): boolean => {
      return status !== RESULTS.BLOCKED && status !== RESULTS.DENIED;
    };

    return {
      check: async () => {
        const permisions = await Promise.all(REQUIRED_PERMISSIONS.map(check));
        if (permisions.every(accepted)) {
          return "granted";
        } else if (permisions.some((p) => p === RESULTS.BLOCKED)) {
          return "denied";
        }
        return "prompt";
      },
      request: async () => {
        const permisions = await requestMultiple(REQUIRED_PERMISSIONS);
        if (Object.values(permisions).every(accepted)) {
          return "granted";
        } else if (
          Object.values(permisions).some((p) => p === RESULTS.BLOCKED)
        ) {
          return "denied";
        }
        return "prompt";
      },
      onChange,
    };
  }

  const state: () => Promise<AppPermissions> = async () =>
    __DEV__ || navigator.bluetooth ? "granted" : "not-supported";
  return {
    check: state,
    request: state,
    onChange,
  };
}

export const { check, request, onChange } = permissions();

import { I18n } from "i18n-js";
import { TranslateOptions } from "i18n-js/typings/typing";

import cs from "./locales/cs.json";
import de from "./locales/de.json";
import en from "./locales/en.json";
import es from "./locales/es.json";
import fr from "./locales/fr.json";
import it from "./locales/it.json";
import pl from "./locales/pl.json";
import sk from "./locales/sk.json";

const i18n = new I18n(
  { cs, de, en, es, fr, it, pl, sk },
  { enableFallback: true },
);

export type LocaleSchemeName =
  | "cs"
  | "de"
  | "en"
  | "es"
  | "fr"
  | "it"
  | "pl"
  | "sk"
  | "no-preference";

export function getLabel(key: string, vals?: TranslateOptions): string {
  return i18n.t(key, vals);
}

import semver, { SemVer } from "semver";

import { Firmware } from "./ble-plx/types";
import { fetchBlob } from "./common";
import { md5 } from "./hashes";
import { log } from "./logger";

const PROJECT_ID = 34623671;
const PROJECT_NAME = "espressif";

type Package = {
  id: number;
  name: string;
  version: string;
};

type PackageFile = {
  id: number;
  file_name: string;
  size: number;
  file_md5: string | null;
  file_sha1: string | null;
  file_sha256: string | null;
};

export function parseSemver(
  v: string | undefined = "",
  fallback: string = "N/A",
): string {
  const coerced = semver.coerce(v);
  if (!coerced) {
    return fallback;
  }

  const prerelease = v.indexOf("-");
  const sv = semver.parse(
    coerced.version + (prerelease >= 0 ? v.substring(prerelease) : ""),
  );
  return sv ? sv.version : fallback;
}

async function fetchApi<T>(path: string, fallback: T): Promise<T> {
  try {
    const resp = await fetch(`https://gitlab.com/api/v4/${path}`, {
      method: "GET",
      headers: { Accept: "application/json" },
    });

    return await resp.json();
  } catch (e) {
    log.warn(`fetch failed`, e);
    return fallback;
  }
}

async function fetchPackages(
  currentVersion: SemVer | string,
): Promise<{ id: number; version: string }[]> {
  const packages = await fetchApi<Package[]>(
    `projects/${PROJECT_ID}/packages`,
    [],
  );

  return packages
    .filter(({ name }) => name === "lights-ble")
    .map(({ id, version }) => ({ id, version: parseSemver(version) }))
    .sort((a, b) => semver.compare(b.version, a.version))
    .filter(({ version }) => semver.gt(version, currentVersion));
}

async function fetchPackageFiles(
  id: number,
  filter: RegExp,
): Promise<PackageFile | undefined> {
  const files = await fetchApi<PackageFile[]>(
    `projects/${PROJECT_ID}/packages/${id}/package_files`,
    [],
  );
  return files.filter(({ file_name }) => filter.test(file_name))[0];
}

export async function getFirmware(
  board: string,
  version?: string,
): Promise<Firmware | undefined> {
  const packages = await fetchPackages(semver.coerce(version) || "0.0.0");
  if (packages.length === 0) {
    return undefined;
  }

  const latestPackage = packages[0];
  const oldestPackage = packages[packages.length - 1];

  let packageVersion = oldestPackage.version;
  let binary = await fetchPackageFiles(
    oldestPackage.id,
    new RegExp(`^(${board})\\.firmware\\.delta$`),
  );

  if (!binary) {
    packageVersion = latestPackage.version;
    binary = await fetchPackageFiles(
      latestPackage.id,
      new RegExp(`^(${board})\\.firmware\\.bin$`),
    );
  }

  if (!binary) {
    return undefined;
  }

  const data = await fetchBlob(
    `https://gitlab.com/aenniw/${PROJECT_NAME}/-/package_files/${binary.id}/download`,
  );

  return {
    name: binary.file_name,
    size: binary.size,
    version: packageVersion,
    md5: binary.file_md5 ? binary.file_md5 : md5(data),
    type: /^.*\.delta$/.test(binary.file_name) ? 1 : 0,
    data,
  };
}

import { NativeTouchEvent, PanResponderGestureState } from "react-native";

export function radianPoint(rad: number, radius: number, size: number) {
  const r = radius + size / 2;
  return {
    x: r + radius * Math.sin(rad),
    y: r + radius * Math.cos(rad),
  };
}

export function cartesian(deg: number, radius: number, size: number) {
  const r = (radius * size) / 2;
  const rad = (Math.PI * deg) / 180;
  const x = r * Math.cos(rad);
  const y = r * Math.sin(rad);
  return {
    left: size / 2 + x,
    top: size / 2 - y,
  };
}

export function polar(
  { locationX, locationY }: NativeTouchEvent,
  size: number,
) {
  const [x, y] = [locationX - size / 2, locationY - size / 2];
  return {
    deg: Math.atan2(y, x) * (-180 / Math.PI),
    radius: Math.sqrt(y * y + x * x) / (size / 2),
  };
}

export function outOfWheel(nativeEvent: NativeTouchEvent, size: number) {
  return polar(nativeEvent, size).radius > 1;
}

export type Dimensions = {
  x: number;
  y: number;
  width: number;
  height: number;
};

export function outOfBox(
  dimensions: Dimensions | undefined,
  { moveX, moveY }: PanResponderGestureState,
) {
  if (dimensions === undefined) {
    return true;
  }
  const { x, y, width, height } = dimensions;
  return !(
    moveX >= x &&
    moveX <= x + width &&
    moveY >= y &&
    moveY <= y + height
  );
}

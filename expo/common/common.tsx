import base64 from "react-native-base64";

export function wait(timeout: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
}

export async function timeout<T>(
  p: Promise<T>,
  t: number,
): Promise<T | undefined> {
  let timer: NodeJS.Timeout;

  return Promise.race([
    p,
    new Promise<undefined>((s, _) => {
      timer = setTimeout(() => s(undefined), t);
    }),
  ]).finally(() => clearTimeout(timer));
}

export function encodeb64(v: string | undefined | null): string {
  if (!v) {
    return "";
  }
  return base64.encode(v) as string;
}

export function decodeb64(v: string | undefined | null): string | undefined {
  if (v === undefined || v === null) {
    return undefined;
  }
  if (v.length === 0) {
    return "";
  }
  return base64.decode(v) as string;
}

export function toHHMMSS(elapsed: number) {
  const hours = Math.floor(elapsed / 3600);
  const minutes = Math.floor((elapsed - hours * 3600) / 60);
  const seconds = Math.floor(elapsed - hours * 3600 - minutes * 60);

  let time = "";
  if (hours) time += ` ${hours}h`;
  if (minutes) time += ` ${minutes}m`;
  if (seconds) time += ` ${seconds}s`;
  return time.trim();
}

export async function fetchBlob(
  url: string,
  headers?: { [_: string]: any },
): Promise<string> {
  const resp = await fetch(url, { method: "GET", headers });
  const blob = await resp.blob();

  return await new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () =>
      resolve(decodeb64((reader.result as string).split(",")[1]) || "");
    reader.onerror = () => reject(new Error("Fialed to load content."));
    reader.readAsDataURL(blob);
  });
}

import {
  BleError,
  Device,
  UUID,
  Subscription,
  NativeDevice,
  BleManager,
  DeviceId,
  Characteristic,
  Service,
  Base64,
  NativeService,
} from "react-native-ble-plx";

export interface IDevice {
  readonly device: Device;
  isConnected(): boolean;
  connect(): Promise<Device>;
  disconnect(): Device;
  onDisconnect(l: (e: BleError | null, d: Device | null) => void): Subscription;
  services(): Promise<Service[]>;
  characteristics(s: UUID): Promise<Characteristic[]>;
  read(s: UUID, c: UUID): Promise<Characteristic>;
  write(s: UUID, c: UUID, v: Base64, n: boolean): Promise<Characteristic>;
  monitor(
    s: UUID,
    c: UUID,
    l: (e: BleError | null, c: Characteristic | null) => void,
  ): Subscription;
}

export class WebDevice implements IDevice {
  public readonly nativeDevice: NativeDevice;
  public readonly device: Device;

  private readonly manager: BleManager;
  private readonly disconnectListeners: ((
    e: BleError | null,
    d: Device | null,
  ) => void)[] = [];
  private readonly data: {
    [_: UUID]: {
      [_: UUID]: {
        value: string | null;
        listeners: ((v: Characteristic) => void)[];
      };
    };
  } = {};
  private connected = false;

  constructor(id: DeviceId, manufacturerData: string, m: BleManager) {
    this.nativeDevice = {
      id,
      name: id,
      localName: id,
      mtu: 20,
      manufacturerData,
      serviceData: {},
      serviceUUIDs: [] as string[],
      txPowerLevel: null,
      solicitedServiceUUIDs: null,
      isConnectable: true,
      overflowServiceUUIDs: null,
    } as NativeDevice;
    this.device = new Device(this.nativeDevice, m);
    this.manager = m;
  }

  private characteristic(s: UUID, c: UUID) {
    if (!(s in this.data)) {
      this.data[s] = {};
    }
    if (!(c in this.data[s])) {
      this.data[s][c] = {
        value: null,
        listeners: [],
      };
    }
    return this.data[s][c];
  }

  public isConnected() {
    return this.connected;
  }

  public async connect() {
    if (this.nativeDevice.isConnectable) {
      this.connected = true;
    }
    return this.device;
  }

  public disconnect(pernament?: boolean) {
    if (this.nativeDevice.isConnectable) {
      this.connected = false;
      this.disconnectListeners.forEach((l) => l(null, this.device));
    }
    if (pernament) {
      this.nativeDevice.isConnectable = false;
    }
    return this.device;
  }

  public onDisconnect(l: (e: BleError | null, d: Device | null) => void) {
    this.disconnectListeners.push(l);
    return {
      remove: () =>
        this.disconnectListeners.splice(this.disconnectListeners.indexOf(l), 1),
    };
  }

  public async services() {
    return Object.keys(this.data).map(
      (uuid, id) =>
        new Service(
          {
            id,
            uuid,
            deviceID: this.nativeDevice.id,
            isPrimary: true,
          } as NativeService,
          this.manager,
        ),
    );
  }

  public async characteristics(s: UUID) {
    return Object.keys(this.data[s] || {}).map((uuid) => {
      return {
        value: this.data[s][uuid].value,
        uuid,
      } as Characteristic;
    });
  }

  public async read(s: UUID, c: UUID) {
    return {
      value: ((this.data[s] || {})[c] || { value: null }).value,
    } as Characteristic;
  }

  public async write(s: UUID, c: UUID, v: Base64, n: boolean) {
    this.characteristic(s, c).value = v;
    if (n) {
      this.data[s][c].listeners.forEach((l) =>
        l({ value: v } as Characteristic),
      );
    }
    return this.read(s, c);
  }

  public monitor(
    s: UUID,
    c: UUID,
    l: (e: BleError | null, c: Characteristic | null) => void,
  ) {
    const cb = (c: Characteristic) => {
      l(null, c);
    };

    const characteristic = this.characteristic(s, c);
    characteristic.listeners.push(cb);

    return {
      remove: () => {
        characteristic.listeners.splice(
          characteristic.listeners.indexOf(cb),
          1,
        );
      },
    };
  }
}

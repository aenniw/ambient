import { encode as encodeBrightness } from "@hooks/pixels/useBrightness";
import { encode as encodeColor } from "@hooks/pixels/useColor";
import { encode as encodeColors } from "@hooks/pixels/useColors";
import { encode as encodeEnabled } from "@hooks/pixels/useEnabled";
import { encode as encodeLength } from "@hooks/pixels/useLength";
import { encode as encodeMode } from "@hooks/pixels/useMode";
import { encode as encodeState } from "@hooks/pixels/useState";
import { encode as encodePower } from "@hooks/utils/usePower";
import { UUID } from "react-native-ble-plx";

import { UtilCharacteristics, PixelCharacteristics } from "./UUIDs";
import { WebDevice } from "./WebDevice";
import { DeviceConfig } from "./types";
import { encodeb64 } from "../common";

export default class TestDevice extends WebDevice {
  private set([s, c]: UUID[], v: string, n?: boolean): TestDevice {
    this.write(s, c, encodeb64(v), !!n);
    return this;
  }

  public setConfig(
    { version, hardware, power, pixels = [] }: DeviceConfig,
    n: boolean | undefined = true,
  ): TestDevice {
    if (version) this.set(UtilCharacteristics.Version, version, n);
    if (hardware) this.set(UtilCharacteristics.Hardware, hardware, n);
    if (power) this.set(UtilCharacteristics.Power, encodePower(power), n);

    pixels.forEach(
      ({ enabled, state, mode, length, brightness, color, colors }, s) => {
        if (enabled !== undefined)
          this.set(PixelCharacteristics[s].Enabled, encodeEnabled(enabled), n);
        if (state !== undefined)
          this.set(PixelCharacteristics[s].State, encodeState(state), n);
        if (mode !== undefined)
          this.set(PixelCharacteristics[s].Mode, encodeMode(mode), n);
        if (length !== undefined)
          this.set(PixelCharacteristics[s].Length, encodeLength(length), n);
        if (brightness !== undefined)
          this.set(
            PixelCharacteristics[s].Brightness,
            encodeBrightness(brightness),
            n,
          );
        if (color !== undefined)
          this.set(PixelCharacteristics[s].Color, encodeColor(color), n);
        if (colors !== undefined)
          this.set(PixelCharacteristics[s].Colors, encodeColors(colors), n);
      },
    );

    return this;
  }
}

import {
  BleError,
  BleErrorCode,
  Characteristic,
  ConnectionPriority,
  Device,
  Subscription,
  UUID,
} from "react-native-ble-plx";

import {
  CharacteristicCallback,
  CharacteristicKey,
  UtilCharacteristics,
} from "./UUIDs";
import { Firmware } from "./types";
import { decodeb64, encodeb64, timeout } from "../common";
import { log } from "../logger";

const BleCommonErrors = [
  BleErrorCode.DeviceNotConnected,
  BleErrorCode.DeviceDisconnected,
  BleErrorCode.OperationTimedOut,
  BleErrorCode.ServiceNotFound,
  BleErrorCode.ServicesNotDiscovered,
  BleErrorCode.CharacteristicNotFound,
  BleErrorCode.CharacteristicsNotDiscovered,
];
const BleNotificationErrors = [
  BleErrorCode.DeviceNotConnected,
  BleErrorCode.DeviceDisconnected,
  BleErrorCode.CharacteristicNotifyChangeFailed,
];

export type ConnectionCallback = (v: boolean) => void;

export default class BleDevice {
  public readonly id: string;
  private readonly device: Device;
  private name: string;
  private connected: boolean = false;
  private isConnect: boolean = false;
  private connecting: Promise<[boolean, boolean]> = Promise.resolve([
    false,
    false,
  ]);

  private readonly cache: Map<CharacteristicKey, string | undefined>;
  private readonly subscriptions: Map<CharacteristicKey, Subscription>;
  private readonly characteristicListeners: Map<
    CharacteristicKey,
    CharacteristicCallback[]
  >;
  private readonly connectionListeners: ConnectionCallback[] = [];
  private isUpgrade: boolean = false;

  constructor(device: Device) {
    this.id = device.id;
    this.name = device.localName || "";
    this.device = device;
    this.cache = new Map();
    this.characteristicListeners = new Map();
    this.subscriptions = new Map();

    this.device.onDisconnected(() => {
      log.info(`disconected - ${this.id}`);
      this.setConnected(false);
    });
  }

  private setCharacteristic(
    key: CharacteristicKey,
    ch: Characteristic | undefined = undefined,
  ): string | undefined {
    let value = undefined;
    const lastValue = this.cache.get(key);
    if (ch) {
      value = decodeb64(ch.value) as string;
    }
    if (value !== lastValue) {
      this.cache.set(key, value);

      const listeners = this.characteristicListeners.get(key) || [];
      for (const listener of listeners) {
        listener(value);
      }
    }
    return value;
  }

  private async setConnected(
    value: boolean,
    validator?: () => Promise<boolean>,
  ): Promise<void> {
    if (this.connected === value) {
      return;
    }

    if (value) {
      await this.device.discoverAllServicesAndCharacteristics();
    } else {
      for (const [characteristic] of this.cache) {
        this.setCharacteristic(characteristic, undefined);
      }
    }

    this.connected = value && validator ? await validator() : value;

    for (const listener of this.connectionListeners) {
      listener(this.connected);
    }
  }

  public async serviceUUIDs(): Promise<UUID[]> {
    if (!this.isConnected()) {
      return [];
    }
    return (await this.device.services()).map((s) => s.uuid);
  }

  public isAttached(): Promise<boolean> {
    return this.device.isConnected();
  }

  public async attach(): Promise<boolean> {
    let attached = await this.isAttached();
    if (!attached) {
      try {
        await this.device.connect({
          timeout: 1500,
          refreshGatt: "OnConnected",
        });
        attached = await this.isAttached();
      } catch (error) {
        if (
          error instanceof BleError &&
          error.errorCode === BleErrorCode.DeviceAlreadyConnected
        ) {
          attached = true;
        } else {
          log.warn(`attach failed - ${this.id}`);
        }
      }
    }

    return attached;
  }

  public isConnected(): boolean {
    return this.connected;
  }

  public async disconnect(): Promise<void> {
    if (this.connected) {
      await this.device.cancelConnection();
    }
  }

  public cancelConnect(): void {
    this.isConnect = false;
  }

  public connect(retries: number = 1): Promise<[boolean, boolean]> {
    this.connecting = this.connecting.then(async () => {
      this.isConnect = true;
      const attached = await this.attach();

      try {
        await this.setConnected(attached, async () => {
          let version: string | undefined;
          for (
            let i = 0;
            this.isConnect && version === undefined && i < retries;
            i++
          ) {
            version = await timeout(
              this.read(UtilCharacteristics.Version, false),
              1000,
            );
          }

          return version !== undefined;
        });
      } catch (e) {
        log.warn(`connect failed - ${this.id}`, e);
      }

      return [attached, this.connected];
    });

    return this.connecting;
  }

  public onConnection(cb: ConnectionCallback) {
    this.connectionListeners.push(cb);

    return () => {
      const index = this.connectionListeners.indexOf(cb);
      if (index >= 0) {
        this.connectionListeners.splice(index, 1);
      }
    };
  }

  public getName() {
    return this.name;
  }

  public setName(name: string) {
    this.name = name;
  }

  public async has(characteristic: CharacteristicKey): Promise<boolean> {
    const characteristics = await this.device.characteristicsForService(
      characteristic[0],
    );
    return (
      characteristics.find(({ uuid }) => uuid === characteristic[1]) !==
      undefined
    );
  }

  public async read(
    characteristic: CharacteristicKey,
    useCache: boolean = true,
  ): Promise<string | undefined> {
    const value = this.cache.get(characteristic);
    if (useCache && value !== undefined) {
      return value;
    }

    log.debug(`read - ${this.id} - ${characteristic}`);
    try {
      return this.setCharacteristic(
        characteristic,
        await this.device.readCharacteristicForService(
          characteristic[0],
          characteristic[1],
        ),
      );
    } catch (error) {
      if (
        error instanceof BleError &&
        BleCommonErrors.includes(error.errorCode)
      ) {
        this.setConnected(false);
      } else {
        log.error(`read failed - ${this.id} - ${characteristic}`, error);
      }
    }
  }

  public async write(
    characteristic: CharacteristicKey,
    value: string,
  ): Promise<void> {
    log.debug(`write - ${this.id} - ${characteristic}`);
    try {
      await this.device.writeCharacteristicWithoutResponseForService(
        characteristic[0],
        characteristic[1],
        encodeb64(value),
      );
      this.cache.set(characteristic, value);
    } catch (error) {
      if (
        error instanceof BleError &&
        BleCommonErrors.includes(error.errorCode)
      ) {
        this.setConnected(false);
      } else {
        log.error(`write failed - ${this.id} - ${characteristic}`, error);
      }
    }
  }

  public async upgrade(
    firmware: Firmware,
    progress: (n: number) => void,
  ): Promise<void> {
    log.debug(`upgrade - ${this.id} - ${firmware.version}/${firmware.type}`);
    const MTU = 512;
    const data =
      String.fromCharCode(
        firmware.size & 0xff,
        (firmware.size >> 8) & 0xff,
        (firmware.size >> 16) & 0xff,
        (firmware.size >> 24) & 0xff,
        firmware.type & 0xff,
        0,
        0,
        0,
        ...(firmware.md5.match(/.{1,2}/g) || [])
          .map((b) => parseInt(b, 16))
          .reverse(),
      ) + firmware.data;

    await this.device.requestConnectionPriority(ConnectionPriority.High);
    await this.device.requestMTU(MTU);

    let offset = 0;
    this.isUpgrade = true;
    while (this.isUpgrade) {
      const chunk = data.slice(offset, offset + (MTU - 3));
      if (!chunk.length) {
        break;
      }

      await this.device.writeCharacteristicWithResponseForService(
        UtilCharacteristics.Firmware[0],
        UtilCharacteristics.Firmware[1],
        encodeb64(chunk),
      );

      offset += chunk.length;
      progress(offset / data.length);
    }

    await this.device.requestConnectionPriority(ConnectionPriority.Balanced);
    await this.device.requestMTU(this.device.mtu);
  }

  public cancelUpgrade(): void {
    log.debug(`upgrade - ${this.id} - canceled`);
    this.isUpgrade = false;
  }

  public onChange(
    characteristic: CharacteristicKey,
    cb: CharacteristicCallback,
  ) {
    if (!this.subscriptions.get(characteristic)) {
      const subscription = this.device.monitorCharacteristicForService(
        characteristic[0],
        characteristic[1],
        (e, c) => {
          log.debug(
            `notification - ${this.id} - ${characteristic}`,
            e ? e.errorCode : "",
          );

          if (e && BleNotificationErrors.includes(e.errorCode)) {
            this.subscriptions.get(characteristic)?.remove();
            this.subscriptions.delete(characteristic);
          } else if (e || !c) {
            this.setCharacteristic(characteristic);
          } else {
            this.setCharacteristic(characteristic, c);
          }
        },
      );

      this.subscriptions.set(characteristic, subscription);
    }

    const listeners = this.characteristicListeners.get(characteristic) || [];
    this.characteristicListeners.set(characteristic, [...listeners, cb]);

    return () => {
      const listeners = this.characteristicListeners.get(characteristic) || [];
      const index = listeners.indexOf(cb);
      if (index >= 0) {
        listeners.splice(index, 1);
      }
    };
  }
}

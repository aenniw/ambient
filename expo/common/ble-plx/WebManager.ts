import {
  BleError,
  Device,
  ScanOptions,
  State,
  Subscription,
  UUID,
  LogLevel,
  BleManager,
  DeviceId,
  ConnectionOptions,
  Characteristic,
  Service,
  ConnectionPriority,
  Descriptor,
  Base64,
} from "react-native-ble-plx";

import { IDevice } from "./WebDevice";

interface IWebBleManager {
  discoverFail: (e: number) => void;
  discover: (device: IDevice) => IDevice;
  device: (id: DeviceId) => IDevice;
}

export function WebBleManager(): IWebBleManager & BleManager {
  let state: State = State.PoweredOn;
  const stateListeners: ((s: State) => void)[] = [];
  const scanListeners: ((e: BleError | null, d: Device | null) => void)[] = [];
  const devices: { [_: DeviceId]: IDevice } = {};

  const manager = {
    destroy: (): void => {},

    setLogLevel: (_: LogLevel): void => {},

    logLevel: (): Promise<LogLevel> => Promise.resolve(LogLevel.None),

    cancelTransaction: (_: string): void => {},

    enable: (_?: string): Promise<BleManager> => {
      state = State.PoweredOn;
      stateListeners.forEach((l) => l(state));
      return Promise.resolve(manager);
    },

    disable: (_?: string): Promise<BleManager> => {
      state = State.PoweredOff;
      stateListeners.forEach((l) => l(state));
      return Promise.resolve(manager);
    },
    state: (): Promise<State> => Promise.resolve(state),

    onStateChange: (l: (s: State) => void, e?: boolean): Subscription => {
      stateListeners.push(l);
      if (e) {
        l(state);
      }
      return {
        remove: () => stateListeners.splice(stateListeners.indexOf(l), 1),
      };
    },

    startDeviceScan: (
      _: UUID[] | null,
      __: ScanOptions | null,
      l: (e: BleError | null, d: Device | null) => void,
    ): void => {
      scanListeners.push(l);

      Object.values(devices).forEach((d) => {
        l(null, d.device);
      });
    },

    stopDeviceScan: (): void => {},

    requestConnectionPriorityForDevice: (
      id: DeviceId,
      _: ConnectionPriority,
      __?: string,
    ): Promise<Device> => Promise.resolve(devices[id].device),

    readRSSIForDevice: (id: DeviceId, _?: string): Promise<Device> =>
      Promise.resolve(devices[id].device),

    requestMTUForDevice: (
      id: DeviceId,
      _: number,
      __?: string,
    ): Promise<Device> => Promise.resolve(devices[id].device),

    devices: (ids: DeviceId[]): Promise<Device[]> =>
      Promise.resolve(
        Object.values(devices)
          .filter((d) => ids.includes(d.device.id))
          .map((d) => d.device),
      ),

    connectedDevices: async (ids: UUID[]): Promise<Device[]> => {
      const connected: Device[] = [];
      for (const { isConnected, services, device } of Object.values(devices)) {
        if (!isConnected()) {
          continue;
        }
        const knownServices = await services();
        if (ids.some((id) => knownServices.map((s) => s.uuid).includes(id))) {
          connected.push(device);
        }
      }
      return connected;
    },

    connectToDevice: (id: DeviceId, _?: ConnectionOptions): Promise<Device> =>
      devices[id].connect(),

    cancelDeviceConnection: (id: DeviceId): Promise<Device> =>
      Promise.resolve(devices[id].disconnect()),

    onDeviceDisconnected: (
      id: DeviceId,
      l: (e: BleError | null, d: Device | null) => void,
    ): Subscription => devices[id].onDisconnect(l),

    isDeviceConnected: (id: DeviceId): Promise<boolean> =>
      Promise.resolve(devices[id].isConnected()),

    discoverAllServicesAndCharacteristicsForDevice: (
      id: DeviceId,
      _?: string,
    ): Promise<Device> => Promise.resolve(devices[id].device),

    servicesForDevice: (id: DeviceId): Promise<Service[]> =>
      Promise.resolve(devices[id].services()),

    characteristicsForDevice: (
      id: DeviceId,
      s: UUID,
    ): Promise<Characteristic[]> => devices[id].characteristics(s),

    readCharacteristicForDevice: (
      id: DeviceId,
      s: UUID,
      c: UUID,
      _?: string,
    ): Promise<Characteristic> => devices[id].read(s, c),

    writeCharacteristicWithResponseForDevice: (
      id: DeviceId,
      s: UUID,
      c: UUID,
      v: Base64,
      _?: string,
    ): Promise<Characteristic> => devices[id].write(s, c, v, true),

    writeCharacteristicWithoutResponseForDevice: (
      id: DeviceId,
      s: UUID,
      c: UUID,
      v: Base64,
      _?: string,
    ): Promise<Characteristic> => devices[id].write(s, c, v, true),

    monitorCharacteristicForDevice: (
      id: DeviceId,
      s: UUID,
      c: UUID,
      l: (e: BleError | null, c: Characteristic | null) => void,
      _?: string,
    ): Subscription => devices[id].monitor(s, c, l),

    descriptorsForDevice: (
      _: DeviceId,
      __: UUID,
      ___: UUID,
    ): Promise<Descriptor[]> =>
      Promise.reject(new Error("Method not implemented.")),

    readDescriptorForDevice: (
      _: DeviceId,
      __: UUID,
      ___: UUID,
      ____: UUID,
      _____?: string,
    ): Promise<Descriptor> =>
      Promise.reject(new Error("Method not implemented.")),

    writeDescriptorForDevice: (
      _: DeviceId,
      __: UUID,
      ___: UUID,
      ____: UUID,
      _____: Base64,
      ______?: string,
    ): Promise<Descriptor> =>
      Promise.reject(new Error("Method not implemented.")),

    discover: (device: IDevice) => {
      devices[device.device.id] = device;
      scanListeners.forEach((l) => l(null, device.device));
      return device;
    },

    discoverFail: (errorCode: number) => {
      scanListeners.forEach((l) =>
        l(
          {
            name: `Error ${errorCode}`,
            message: `Error message ${errorCode}`,
            errorCode,
            attErrorCode: null,
            iosErrorCode: null,
            androidErrorCode: null,
            reason: null,
          },
          null,
        ),
      );
    },

    device: (id: DeviceId) => devices[id],
  };

  return manager;
}

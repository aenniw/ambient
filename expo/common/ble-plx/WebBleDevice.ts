import {
  BleError,
  Device,
  UUID,
  BleManager,
  Characteristic,
  Service,
  Base64,
} from "react-native-ble-plx";

import { IDevice } from "./WebDevice";
import { decodeb64, encodeb64, wait } from "../common";
import { log } from "../logger";

export class WebBleDevice implements IDevice {
  public readonly nativeDevice: BluetoothDevice;
  public readonly device: Device;

  private readonly manager: BleManager;
  private readonly disconnectListeners: ((
    e: BleError | null,
    d: Device | null,
  ) => void)[] = [];
  private readonly characteristicListeners: {
    [_: UUID]: {
      [_: UUID]: {
        listeners: ((e: BleError | null, c: Characteristic | null) => void)[];
      };
    };
  } = {};

  constructor(
    nativeDevice: BluetoothDevice,
    manufacturerData: string,
    m: BleManager,
  ) {
    const id = nativeDevice.id
      .split("")
      .map((x) => x.charCodeAt(0).toString(16))
      .join("");

    this.nativeDevice = nativeDevice;
    this.device = new Device(
      {
        id,
        name: nativeDevice.name || id,
        localName: nativeDevice.name || id,
        manufacturerData,
        serviceData: {},
        serviceUUIDs: [],
        isConnectable: this.nativeDevice.gatt !== undefined,
        txPowerLevel: null,
        solicitedServiceUUIDs: null,
        overflowServiceUUIDs: null,
        mtu: 20,
        rssi: 0,
      },
      m,
    );
    this.manager = m;
  }

  private async getValue(
    characteristic: BluetoothRemoteGATTCharacteristic,
  ): Promise<string | null> {
    let value: DataView | undefined = characteristic.value;

    let retries = 0;
    while (!value && retries++ < 5) {
      try {
        value = await characteristic.readValue();
      } catch (e) {
        log.debug(`failed to read ${characteristic.uuid}`, e);
        await wait(250 + retries * 50);
      }
    }

    if (!value) {
      log.error(`failed to get value ${characteristic.uuid}`);
      return null;
    }

    const decoder = new TextDecoder("ascii");
    return encodeb64(value ? decoder.decode(value) : "");
  }

  private encode(value: string): BufferSource {
    const decodedValue = decodeb64(value) || "";

    const chars: number[] = [];
    for (let i = 0; i < decodedValue.length; i++) {
      chars[i] = decodedValue.charCodeAt(i);
    }

    return new Uint8Array(chars);
  }

  private characteristic(s: UUID, c: UUID) {
    if (!(s in this.characteristicListeners)) {
      this.characteristicListeners[s] = {};
    }
    if (!(c in this.characteristicListeners[s])) {
      this.characteristicListeners[s][c] = {
        listeners: [],
      };
    }
    return this.characteristicListeners[s][c];
  }

  private async nativeCharacteristic(s: UUID, c: UUID) {
    const service = await this.nativeDevice.gatt?.getPrimaryService(s);
    return await service?.getCharacteristic(c);
  }

  public isConnected() {
    return this.nativeDevice.gatt?.connected || false;
  }

  public async connect() {
    if (!this.isConnected()) {
      await this.nativeDevice.gatt?.connect();

      this.nativeDevice.addEventListener("gattserverdisconnected", () =>
        this.disconnectListeners.forEach((l) => l(null, this.device)),
      );
    }
    return this.device;
  }

  public disconnect(pernament?: boolean) {
    if (this.isConnected()) {
      this.nativeDevice.gatt?.disconnect();
      this.disconnectListeners.forEach((l) => l(null, this.device));
    }
    if (pernament) {
      this.nativeDevice.forget();
    }
    return this.device;
  }

  public onDisconnect(l: (e: BleError | null, d: Device | null) => void) {
    this.disconnectListeners.push(l);
    return {
      remove: () =>
        this.disconnectListeners.splice(this.disconnectListeners.indexOf(l), 1),
    };
  }

  public async services() {
    const services = (await this.nativeDevice.gatt?.getPrimaryServices()) || [];
    return services.map(
      ({ uuid, isPrimary }, id) =>
        new Service(
          {
            id,
            uuid,
            deviceID: this.nativeDevice.id,
            isPrimary,
          },
          this.manager,
        ),
    );
  }

  public async characteristics(s: UUID) {
    const service = await this.nativeDevice.gatt?.getPrimaryService(s);
    const characteristics = (await service?.getCharacteristics()) || [];

    const values: Characteristic[] = [];
    for (const characteristic of characteristics) {
      values.push({
        value: await this.getValue(characteristic),
        uuid: characteristic.uuid,
      } as Characteristic);
    }
    return values;
  }

  public async read(s: UUID, c: UUID) {
    const characteristic = await this.nativeCharacteristic(s, c);

    if (!characteristic) {
      return { value: null } as Characteristic;
    }

    return {
      value: await this.getValue(characteristic),
    } as Characteristic;
  }

  public async write(s: UUID, c: UUID, value: Base64, n: boolean) {
    const characteristic = await this.nativeCharacteristic(s, c);

    if (!characteristic) {
      return { value: null } as Characteristic;
    }

    characteristic.writeValue(this.encode(value));

    if (n) {
      this.characteristicListeners[s][c].listeners.forEach((l) =>
        l(null, { value } as Characteristic),
      );
    }

    return { value } as Characteristic;
  }

  public monitor(
    s: UUID,
    c: UUID,
    l: (e: BleError | null, c: Characteristic | null) => void,
  ) {
    const { listeners } = this.characteristic(s, c);

    const cb = async () => {
      const characteristic = await this.nativeCharacteristic(s, c);
      if (!characteristic) return;

      const value = {
        value: await this.getValue(characteristic),
      } as Characteristic;
      for (const listener of listeners) {
        listener(null, value);
      }
    };

    this.nativeCharacteristic(s, c).then(async (characteristic) => {
      if (!characteristic) return;
      if (listeners.length === 0) {
        characteristic.addEventListener("characteristicvaluechanged", cb);

        let retries = 0;
        let notify: BluetoothRemoteGATTCharacteristic | undefined = undefined;
        while (!notify && retries++ < 50) {
          try {
            notify = await characteristic.startNotifications();
          } catch (e) {
            log.debug(
              `failed to start notifications ${characteristic.uuid}`,
              e,
            );
            await wait(250 + retries * 50);
          }
        }
        if (!notify) {
          log.error(`failed to monitor ${characteristic.uuid}`);
        }
      }
      listeners.push(l);
    });

    return {
      remove: () => {
        listeners.splice(listeners.indexOf(cb), 1);

        if (listeners.length === 0) {
          this.nativeCharacteristic(s, c).then((characteristic) => {
            if (!characteristic) return;
            characteristic.removeEventListener(
              "characteristicvaluechanged",
              cb,
            );

            return characteristic.stopNotifications();
          });
        }
      },
    };
  }
}

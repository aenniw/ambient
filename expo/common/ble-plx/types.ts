export enum PixelsState {
  OFF,
  ON,
}

export type PixelColor = [
  number, // 0-360
  number, // 0-100
  string, // hex-code
];

export enum PixelsMode {
  STATIC,
  FADE,
  RAINBOW,
  TRANSITION,
}
export type PixelsOptions = {
  duration: number;
  chained: boolean;
  randomized: boolean;
  partition: number;
};

export type PixelConfig = {
  enabled?: boolean;
  state?: PixelsState;
  mode?: [PixelsMode, PixelsOptions];
  length?: number;
  brightness?: number;
  color?: PixelColor;
  colors?: PixelColor[];
};

export type DeviceConfig = {
  version?: string;
  hardware?: string;
  power?: number;
  pixels?: PixelConfig[];
};

export type Firmware = {
  name: string;
  type: FirmwareType;
  data: string;
  size: number;
  md5: string;
  version: string;
};
export type FirmwareType = 0 | 1;

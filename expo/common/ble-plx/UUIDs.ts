import { fullUUID } from "react-native-ble-plx";

export type CharacteristicKey = [string, string];
export type CharacteristicCallback = (v: string | undefined) => void;

export const PixelServices = ["ab5ff770", "ab5ff771", "ab5ff772"].map(fullUUID);
export const PixelCharacteristics: { [_: string]: CharacteristicKey }[] =
  PixelServices.map((uuid) => ({
    Enabled: [uuid, fullUUID("b537fb4e")],
    State: [uuid, fullUUID("b533fb4e")],
    Color: [uuid, fullUUID("05f3704e")],
    Colors: [uuid, fullUUID("b532fb4e")],
    Brightness: [uuid, fullUUID("604d979d")],
    Mode: [uuid, fullUUID("a7601c29")],
    Power: [uuid, fullUUID("b534fb4e")],
    Length: [uuid, fullUUID("b535fb4e")],
  }));

export const UtilService = fullUUID("ab5fa770");
export const UtilCharacteristics: { [_: string]: CharacteristicKey } = {
  Version: [UtilService, fullUUID("02f3704e")],
  Hardware: [UtilService, fullUUID("04f3704e")],
  Name: [UtilService, fullUUID("02f3714e")],
  Power: [UtilService, fullUUID("b534fb4e")],
  Secret: [UtilService, fullUUID("02f3724e")],
  Firmware: [UtilService, fullUUID("03f3704e")],
};

import { logger, consoleTransport } from "react-native-logs";

const config = {
  levels: {
    debug: 0,
    info: 1,
    warn: 2,
    error: 3,
  },
  transport: consoleTransport,
  severity: __DEV__ ? "debug" : "error",
  transportOptions: {
    colors: {
      debug: "blueBright",
      warn: "yellowBright",
      error: "redBright",
    },
  },
  async: true,
};

const log = logger.createLogger(config);

export { log };

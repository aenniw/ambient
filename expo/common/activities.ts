import { ActivityAction, startActivityAsync } from "expo-intent-launcher";
import { Platform } from "react-native";

export async function locationActivity(): Promise<void> {
  if (Platform.OS !== "web") {
    await startActivityAsync(ActivityAction.LOCATION_SOURCE_SETTINGS);
  }
}

export async function bluetoothActivity(): Promise<void> {
  if (Platform.OS !== "web") {
    await startActivityAsync(ActivityAction.BLUETOOTH_SETTINGS);
  }
}

import { createContext, useContext } from "react";

export function createGenericContext<T extends unknown>() {
  const genericContext = createContext<T | undefined>(undefined);

  const useGenericContext = () => {
    const contextIsDefined = useContext(genericContext);
    if (contextIsDefined === undefined || contextIsDefined === null) {
      throw new Error("useGenericContext must be used within a Provider");
    }
    return contextIsDefined;
  };

  return [useGenericContext, genericContext.Provider] as const;
}

export function genericContext<T extends unknown>() {
  const genericContext = createContext<T | undefined>(undefined);

  return [() => useContext(genericContext), genericContext.Provider] as const;
}

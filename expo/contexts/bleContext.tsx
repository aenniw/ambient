import { bluetoothActivity, locationActivity } from "@common/activities";
import BleDevice from "@common/ble-plx/BleDevice";
import { wait } from "@common/common";
import { BleState, useBle } from "@hooks/ble/useBle";
import { useBleManager } from "@hooks/ble/useBleManager";
import { usePixels } from "@hooks/ble/usePixels";
import useInterval from "@hooks/useTimeout";
import { useCallback, useEffect, useState } from "react";
import { State } from "react-native-ble-plx";

import { createGenericContext } from "./genericContext";

type BleContext = {
  ble?: BleState;
  isLoading: boolean;
  known: BleDevice[];
  unknown: BleDevice[];
  enableLocation: () => Promise<void>;
  enableBluetooth: () => Promise<void>;
  setScanning: (s: boolean) => void;
  addDevice: (id: string) => void;
  getDevice: (id: string) => BleDevice | undefined;
  discover?: () => Promise<void>;
};
const [useBleContext, Provider] = createGenericContext<BleContext>();

type BleProviderProps = {
  children: React.ReactNode;
};
export function BleProvider({ children }: BleProviderProps) {
  const { bleManager, discoverDevice } = useBleManager();

  const [loading, setLoading] = useState<boolean>(false);

  const [ble, setBleState] = useBle(bleManager);
  const {
    known,
    unknown,
    setScanning,
    addKnownId: addDevice,
  } = usePixels(bleManager, setBleState, discoverDevice === undefined);

  useEffect(() => {
    setScanning(ble === "ready");

    return () => setScanning(false);
  }, [ble, setScanning]);

  useInterval(async () => {
    const state = await bleManager.state();

    if (ble === "no-location" && state === State.PoweredOn) {
      try {
        setLoading(true);
        setScanning(true);
        setBleState("ready");
      } finally {
        setLoading(false);
      }
    }
  }, 1000);

  const configure = useCallback(async (dependency: Promise<any>) => {
    try {
      setLoading(true);
      await dependency;
      await wait(500);
    } finally {
      setLoading(false);
    }
  }, []);

  const enableLocation = useCallback(
    () => configure(locationActivity()),
    [configure],
  );
  const enableBluetooth = useCallback(
    () => configure(bluetoothActivity()),
    [configure],
  );

  const getDevice = useCallback(
    (dId: string): BleDevice | undefined => {
      return known.find(({ id }) => id === dId);
    },
    [known],
  );

  return (
    <Provider
      value={{
        ble,
        known,
        unknown,
        isLoading: loading,
        enableBluetooth,
        enableLocation,
        setScanning,
        addDevice,
        getDevice,
        discover: discoverDevice,
      }}
    >
      {children}
    </Provider>
  );
}

export { useBleContext };

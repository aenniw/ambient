import { useFonts } from "expo-font";
import { useMemo } from "react";

import { createGenericContext } from "./genericContext";

type Fonts = {
  loaded: boolean;
  Roboto?: string;
};
const [useFontsContext, Provider] = createGenericContext<Fonts>();

type FontsProviderProps = {
  children: React.ReactNode;
};
export function FontsProvider({ children }: FontsProviderProps) {
  const fontDefinitions = useMemo(
    () => ({
      Roboto: require("@resources/Roboto-Regular.ttf"),
    }),
    [],
  );
  const [loaded] = useFonts(fontDefinitions);

  const fonts: Fonts = useMemo(
    () =>
      Object.keys(fontDefinitions).reduce(
        (acc, key) => ({ ...acc, [key]: loaded ? key : undefined }),
        { loaded: !!loaded },
      ),
    [loaded, fontDefinitions],
  );

  return <Provider value={fonts}>{children}</Provider>;
}

export { useFontsContext };

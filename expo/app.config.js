const { version } = require("./package.json");

const versionCode = Number.parseInt(
  version
    .split(".")
    .map((n) => n.padStart(3, "0"))
    .join(""),
  10,
);

const __DEV__ = process.env.NODE_ENV === "development";

export default ({ config }) => ({
  ...config,
  version,
  web: {
    ...config.web,
    output: __DEV__ ? "single" : "static",
  },
  ios: { ...config.ios, buildNumber: `${versionCode}` },
  android: { ...config.android, versionCode },
  experiments: {
    ...config.experiments,
    baseUrl: __DEV__ ? undefined : config.experiments.baseUrl,
  },
});

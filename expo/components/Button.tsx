import { MaterialIcons } from "@expo/vector-icons";
import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import {
  Platform,
  StyleSheet,
  Switch as NativeSwitch,
  TouchableOpacity,
  TouchableOpacityProps,
} from "react-native";

import { LocaleProps, TextLabel } from "./Text";

type ButtonProps = {
  children: React.ReactNode | React.ReactNode[];
} & TouchableOpacityProps;

export function Button({
  style = styles.button,
  disabled,
  children,
  ...props
}: ButtonProps) {
  const backgroundColor = useThemeColor("primary");

  return (
    <TouchableOpacity
      style={[
        {
          backgroundColor: disabled ? "gray" : backgroundColor,
        },
        style,
      ]}
      disabled={disabled}
      {...props}
    >
      {children}
    </TouchableOpacity>
  );
}

export function TextButton({
  label,
  options,
  ...props
}: TouchableOpacityProps & LocaleProps) {
  return (
    <Button testID={`button-${label}`} {...props}>
      <TextLabel label={label} options={options} style={styles.label} />
    </Button>
  );
}

type IconProps = {
  name: React.ComponentProps<typeof MaterialIcons>["name"];
  size?: number;
} & TouchableOpacityProps;

export function Icon({ name, size = 36, ...props }: IconProps) {
  const color = useThemeColor("text");

  return (
    <TouchableOpacity testID={`button-${name}`} {...props}>
      <MaterialIcons name={name} size={size} color={color} />
    </TouchableOpacity>
  );
}

type IconButtonProps = {
  color?: string;
  label?: LocaleProps["label"];
  options?: LocaleProps["options"];
} & IconProps &
  TouchableOpacityProps;

export function IconButton({
  name,
  size = 36,
  color = "white",
  label,
  options,
  ...props
}: IconButtonProps) {
  return (
    <Button testID={`button-${name}`} {...props}>
      <MaterialIcons name={name} size={size} color={color} />
      {label && (
        <TextLabel
          label={label}
          options={options}
          style={[
            styles.label,
            {
              marginTop: 2,
              marginLeft: 10,
            },
          ]}
        />
      )}
    </Button>
  );
}

type SwitchProps = {
  value?: boolean;
  testID?: string;
  onValueChange: (v?: boolean) => void;
};
export function Switch({ value, testID, onValueChange }: SwitchProps) {
  const primaryColor = useThemeColor("primary");
  const trackColor = "#fff";

  return (
    <NativeSwitch
      trackColor={{ false: trackColor, true: trackColor }}
      thumbColor={primaryColor}
      {...Platform.select({
        web: {
          activeThumbColor: primaryColor,
        },
      })}
      onValueChange={onValueChange}
      value={value}
      testID={testID ? `switch-${testID}` : undefined}
    />
  );
}

const styles = StyleSheet.create({
  button: {
    minHeight: 50,
    minWidth: 140,
    paddingHorizontal: 20,
    paddingVertical: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    flexDirection: "row",
  },
  label: {
    color: "white",
  },
});

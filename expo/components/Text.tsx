import { useFontsContext } from "@contexts/fontContext";
import { useLabel } from "@hooks/useLocale";
import { useThemeColor } from "@hooks/useTheme";
import { TranslateOptions } from "i18n-js/typings/typing";
import React from "react";
import { Text as NativeText, TextProps as NativeTextProps } from "react-native";

export type LocaleProps = {
  label: string;
  options?: TranslateOptions;
};

type TextProps = {
  children: React.ReactNode | React.ReactNode[];
} & NativeTextProps;

export function Text({ children, style, ...props }: TextProps) {
  const { Roboto } = useFontsContext();
  const color = useThemeColor("text");

  return (
    <NativeText style={[{ color, fontFamily: Roboto }, style]} {...props}>
      {children}
    </NativeText>
  );
}

type TextLabelProps = LocaleProps & NativeTextProps;

export function TextLabel({ label, options, ...props }: TextLabelProps) {
  const value = useLabel(label, options);

  return <Text {...props}>{value}</Text>;
}

import { MaterialIcons } from "@expo/vector-icons";
import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import { View, StyleSheet, Linking } from "react-native";

import { IconButton, TextButton } from "./Button";
import { TextLabel } from "./Text";
import { LandscapeView } from "./View";

type InfoViewProps = {
  icon: React.ComponentProps<typeof MaterialIcons>["name"];
  info: string;
  link?: string;
  button?: string;
  disabled?: boolean;
  onPress?: () => void;
};

export function InfoView({
  icon,
  info,
  link,
  button,
  disabled,
  onPress,
}: InfoViewProps) {
  const color = useThemeColor("text");

  return (
    <LandscapeView>
      <MaterialIcons
        name={icon}
        size={200}
        color={color}
        style={styles.icon}
        testID={`icon-${info}`}
      />

      <View style={styles.content}>
        <TextLabel style={styles.description} label={info} />

        {link && (
          <IconButton
            name="book"
            size={24}
            label={link}
            onPress={onPress}
            disabled={disabled}
          />
        )}
        {button && (
          <TextButton onPress={onPress} label={button} disabled={disabled} />
        )}
      </View>
    </LandscapeView>
  );
}

export function NoControll() {
  return (
    <InfoView
      icon="blur-off"
      info="pixel.none"
      link="label.documentation"
      onPress={() =>
        Linking.openURL("https://aenniw.gitlab.io/ambient/docs/intro/wiring")
      }
    />
  );
}

type ButtonProps = {
  onPress: () => void;
};

export function NoPixels({ onPress }: ButtonProps) {
  return (
    <InfoView
      icon="power-off"
      info="pixel.disabled"
      button="button.back"
      onPress={onPress}
    />
  );
}

export function PixelsOff({ onPress }: ButtonProps) {
  return (
    <InfoView
      icon="flash-off"
      info="pixel.off"
      button="button.enable"
      onPress={onPress}
    />
  );
}

export function DeviceOff({ onPress }: ButtonProps) {
  return (
    <InfoView
      icon="bluetooth-disabled"
      info="pixel.disconnected"
      button="button.back"
      onPress={onPress}
    />
  );
}

export function NoBluetooth({ onPress }: ButtonProps) {
  return (
    <InfoView
      icon="bluetooth-disabled"
      info="bluetooth.disabled"
      button="button.enable"
      onPress={onPress}
    />
  );
}

export function NoLocation({ onPress }: ButtonProps) {
  return (
    <InfoView
      icon="location-disabled"
      info="location.disabled"
      button="button.enable"
      onPress={onPress}
    />
  );
}

export function NoPermissions({
  onPress,
  disabled,
}: { disabled?: boolean } & ButtonProps) {
  return (
    <InfoView
      icon="perm-device-info"
      info={"permissions." + (disabled ? "denied" : "none")}
      button="button.enable"
      onPress={onPress}
      disabled={disabled}
    />
  );
}

export function NoWifi({ onPress }: ButtonProps) {
  return (
    <InfoView
      icon="signal-wifi-off"
      info="wifi.disabled"
      button="button.enable"
      onPress={onPress}
    />
  );
}

export function PermissionsNotSupported() {
  return (
    <InfoView icon="browser-not-supported" info="permissions.not-supported" />
  );
}

const styles = StyleSheet.create({
  content: {
    alignItems: "center",
    justifyContent: "center",
    maxWidth: "100%",
  },
  icon: {
    marginBottom: 30,
  },
  description: {
    marginBottom: 30,
    paddingHorizontal: 20,
    textAlign: "center",
  },
});

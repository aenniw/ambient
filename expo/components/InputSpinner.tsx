import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import { StyleProp, StyleSheet, View, ViewStyle } from "react-native";

import { IconButton } from "./Button";
import { Text } from "./Text";

export interface ReactNativeInputSpinnerProps {
  min?: number;
  max?: number;
  value?: number;
  step: number;
  colorMax?: string;
  colorMin?: string;
  disabled?: boolean;
  style?: StyleProp<ViewStyle> | undefined;
  testID?: string;
  onChange(v: number): void;
}

export default function InputSpinner({
  style,
  value,
  max,
  min,
  disabled,
  step,
  testID,
  colorMax = "#D57E7E",
  colorMin = "#C6D57E",
  onChange: setValue,
}: ReactNativeInputSpinnerProps) {
  let color = useThemeColor("primary");

  if (max !== undefined && value !== undefined && value >= max) {
    color = colorMax;
  } else if (min !== undefined && value !== undefined && value <= min) {
    color = colorMin;
  }

  const decrease = () => {
    if (value === undefined) {
      return;
    }
    const v = value - step;
    setValue(min !== undefined ? Math.max(min, v) : v);
  };

  const increase = () => {
    if (value === undefined) {
      return;
    }
    const v = value + step;
    setValue(max !== undefined ? Math.min(max, v) : v);
  };

  const radiusSize = 5;
  const borderSize = 2;
  const iconSize = 24;

  return (
    <View style={[styles.container, style]}>
      <IconButton
        style={[
          styles.button,
          {
            backgroundColor: color,
            borderTopLeftRadius: radiusSize,
            borderBottomLeftRadius: radiusSize,
          },
        ]}
        disabled={
          disabled || value === undefined || (min !== undefined && value <= min)
        }
        onPress={decrease}
        testID={testID ? `button-${testID}-decrease` : undefined}
        name="remove"
        size={iconSize}
        color="white"
      />
      <View
        style={{
          borderTopWidth: borderSize,
          borderBottomWidth: borderSize,
          borderColor: color,
          flex: 1,
          height: "100%",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Text
          style={styles.value}
          testID={testID ? `${testID}-value` : undefined}
        >
          {value}
        </Text>
      </View>
      <IconButton
        style={[
          styles.button,
          {
            backgroundColor: color,
            borderTopRightRadius: radiusSize,
            borderBottomRightRadius: radiusSize,
          },
        ]}
        disabled={
          disabled || value === undefined || (max !== undefined && value >= max)
        }
        onPress={increase}
        testID={testID ? `button-${testID}-increase` : undefined}
        name="add"
        size={iconSize}
        color="white"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 175,
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    overflow: "hidden",
  },
  button: {
    width: 50,
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  value: {
    marginHorizontal: 10,
    fontSize: 22,
    fontWeight: "500",
  },
});

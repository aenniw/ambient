import { useThemeColor } from "@hooks/useTheme";
import React, { useEffect, useRef, useState } from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  I18nManager,
  Animated,
  TextInputProps,
  NativeSyntheticEvent,
  TextInputKeyPressEventData,
  KeyboardTypeOptions,
  ViewStyle,
  StyleProp,
  TextStyle,
  Keyboard,
} from "react-native";

export type InputPinProps = {
  value: string;
  codeLength: number;
  cellSize: number;
  cellSpacing?: number;
  placeholder?: string;
  mask?: string;
  maskDelay?: number;
  password?: boolean;
  autoFocus?: boolean;
  restrictToNumbers?: boolean;
  animated?: boolean;

  containerStyle?: StyleProp<ViewStyle>;
  cellStyle?: StyleProp<ViewStyle>;
  cellStyleFocused?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  textStyleFocused?: StyleProp<TextStyle>;
  cellStyleFilled?: StyleProp<ViewStyle>;

  onFulfill: (v: string) => void;
  onBackspace?: () => void;
  onTextChange: (v: string) => void;
  onFocus?: () => void;
  onBlur?: () => void;
  editable?: boolean;
  inputProps?: TextInputProps;
  keyboardType?: KeyboardTypeOptions;
  testID?: string;
  disableFullscreenUI?: boolean;
};

export function InputPin({
  value = "",
  codeLength = 4,
  cellSize = 48,
  cellSpacing = 4,
  placeholder = "",
  password = false,
  mask = "*",
  maskDelay = 200,
  keyboardType = "numeric",
  autoFocus = false,
  restrictToNumbers = false,
  animated = true,
  editable = true,
  inputProps = {},
  disableFullscreenUI = true,
  containerStyle = styles.containerDefault,
  cellStyle,
  cellStyleFocused,
  textStyle,
  textStyleFocused,
  cellStyleFilled,
  onFulfill,
  onBackspace,
  onTextChange,
  testID,
  onFocus,
  onBlur,
}: InputPinProps) {
  const [maskDelayed, setMaskDelayed] = useState(false);
  const [focused, setFocused] = useState(false);

  const maskTimeout = useRef<ReturnType<typeof setTimeout>>();
  const input = useRef<TextInput>(null);

  useEffect(() => {
    const cb = Keyboard.addListener("keyboardDidHide", () => {
      input.current?.blur();
    });

    return () => {
      cb.remove();
      clearTimeout(maskTimeout.current);
    };
  }, []);

  const onChangeText = (code: string) => {
    if (restrictToNumbers) {
      code = (code.match(/[0-9]/g) || []).join("");
    }

    if (onTextChange) {
      onTextChange(code);
    }
    if (code.length === codeLength && onFulfill) {
      onFulfill(code);
    }

    const maskDelayed = password && code.length > value.length;
    setMaskDelayed(maskDelayed);

    if (maskDelayed) {
      clearTimeout(maskTimeout.current);
      maskTimeout.current = setTimeout(() => {
        setMaskDelayed(false);
      }, maskDelay);
    }
  };

  const onKeyPress = (
    event: NativeSyntheticEvent<TextInputKeyPressEventData>,
  ) => {
    if (event.nativeEvent.key === "Backspace") {
      if (value === "" && onBackspace) {
        onBackspace();
      }
    }
  };

  const onFocused = () => {
    setFocused(true);
    if (onFocus) {
      onFocus();
    }
  };

  const onBlurred = () => {
    setFocused(false);
    if (onBlur) {
      onBlur();
    }
  };

  return (
    <View
      style={[
        {
          alignItems: "stretch",
          flexDirection: "row",
          justifyContent: "center",
          position: "relative",
          width: (cellSize * CellAnimationScale + cellSpacing) * codeLength,
          height: cellSize * CellAnimationScale,
        },
        containerStyle,
      ]}
    >
      <View
        style={{
          position: "absolute",
          margin: 0,
          height: "100%",
          flexDirection: I18nManager.isRTL ? "row-reverse" : "row",
          alignItems: "center",
        }}
      >
        {Array.from({ length: codeLength }).map((_, idx) => (
          <InputCell
            idx={idx}
            key={idx}
            animated={animated}
            cellSize={cellSize}
            cellSpacing={cellSpacing}
            focused={focused}
            mask={mask}
            maskDelayed={maskDelayed}
            password={password}
            placeholder={placeholder}
            value={value}
            cellStyle={cellStyle}
            cellStyleFilled={cellStyleFilled}
            cellStyleFocused={cellStyleFocused}
            textStyle={textStyle}
            textStyleFocused={textStyleFocused}
          />
        ))}
      </View>
      <TextInput
        ref={input}
        disableFullscreenUI={disableFullscreenUI}
        value={value}
        onChangeText={onChangeText}
        onKeyPress={onKeyPress}
        onFocus={onFocused}
        onBlur={onBlurred}
        spellCheck={false}
        autoFocus={autoFocus}
        keyboardType={keyboardType}
        numberOfLines={1}
        caretHidden
        maxLength={codeLength}
        selection={{
          start: value.length,
          end: value.length,
        }}
        style={styles.textInput}
        testID={testID}
        editable={editable}
        {...inputProps}
      />
    </View>
  );
}

const CellAnimationScale = 1.1;

type InputCellProps = {
  idx: number;
  focused: boolean;
  value: string;
  password: boolean;
  maskDelayed: boolean;
  placeholder: string;
  mask: string;
  cellSize: number;
  cellSpacing: number;
  animated: boolean;
  cellStyle?: StyleProp<ViewStyle>;
  cellStyleFocused?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  textStyleFocused?: StyleProp<TextStyle>;
  cellStyleFilled?: StyleProp<ViewStyle>;
};

function InputCell({
  idx,
  cellSize,
  cellSpacing,
  mask,
  focused,
  value,
  password,
  maskDelayed,
  placeholder,
  animated,
  cellStyle = styles.cellDefault,
  cellStyleFocused = styles.cellFocusedDefault,
  textStyle = styles.textStyleDefault,
  textStyleFocused = styles.textStyleFocusedDefault,
  cellStyleFilled = {},
}: InputCellProps) {
  const animationValue = useRef(new Animated.Value(cellSize));
  const maxCellSize = cellSize * CellAnimationScale;

  useEffect(() => {
    if (idx === value.length && focused && animated) {
      const animation = Animated.loop(
        Animated.sequence([
          Animated.timing(animationValue.current, {
            toValue: maxCellSize,
            duration: 500,
            useNativeDriver: false,
          }),
          Animated.timing(animationValue.current, {
            toValue: cellSize,
            duration: 500,
            useNativeDriver: false,
          }),
        ]),
      );

      animation.start();

      return () => animation.stop();
    } else {
      animationValue.current.setValue(cellSize);
    }
  }, [idx, value, focused, animated]);

  const cellFocused = focused && idx === value.length;
  const filled = idx < value.length;
  const last = idx === value.length - 1;
  const showMask = filled && password && (!maskDelayed || !last);
  const pinCodeChar = value.charAt(idx);

  let cellText;

  if (filled) {
    if (showMask) {
      cellText = mask;
    } else if (!filled) {
      cellText = placeholder;
    } else if (pinCodeChar) {
      cellText = pinCodeChar;
    }
  }

  const maskComponent = showMask ? mask : null;
  const isCellText = cellText !== undefined;

  return (
    <View
      style={{
        width: maxCellSize,
        height: maxCellSize,
        marginLeft: cellSpacing / 2,
        marginRight: cellSpacing / 2,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Animated.View
        key={idx}
        style={[
          {
            width: animationValue.current,
            height: animationValue.current,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
          },
          cellStyle,
          cellFocused ? cellStyleFocused : {},
          filled ? cellStyleFilled : {},
        ]}
      >
        <Text style={[textStyle, cellFocused ? textStyleFocused : {}]}>
          {isCellText
            ? !maskComponent
              ? cellText
              : maskComponent
            : placeholder}
        </Text>
      </Animated.View>
    </View>
  );
}

type DevicePinProps = {
  value: string;
  codeLength?: number;
  editable?: boolean;
  onFulfill: (v: string) => void;
  onTextChange: (v: string) => void;
};
export function DevicePin({ codeLength = 6, ...props }: DevicePinProps) {
  const textColor = useThemeColor("text");

  return (
    <InputPin
      password
      restrictToNumbers
      mask="﹡"
      cellStyleFocused={{
        borderColor: textColor,
      }}
      cellSize={36}
      codeLength={codeLength}
      {...props}
    />
  );
}

const styles = StyleSheet.create({
  textInput: {
    flex: 1,
    opacity: 0,
    textAlign: "center",
  },
  containerDefault: {},
  cellDefault: {
    borderColor: "gray",
    borderWidth: 1,
  },
  cellFocusedDefault: {
    borderColor: "black",
    borderWidth: 2,
  },
  textStyleDefault: {
    color: "gray",
    fontSize: 24,
  },
  textStyleFocusedDefault: {
    color: "black",
  },
});

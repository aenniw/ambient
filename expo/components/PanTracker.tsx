import {
  Dimensions,
  cartesian,
  outOfBox,
  outOfWheel,
} from "@common/trigonometry";
import { usePanAnimate } from "@hooks/useAnimated";
import React, { useEffect, useRef, useState } from "react";
import { Animated, PanResponder, StyleSheet, View } from "react-native";
import tinycolor, { ColorFormats } from "tinycolor2";

type PanTrackerProps = {
  color: ColorFormats.HSV;
  thumbSize: number;
  wheelSize: number;
  onColorChange: (c: ColorFormats.HSV) => void;
  onColorChangeComplete: (c: ColorFormats.HSV) => void;
};

export function PanTracker({
  color,
  onColorChange,
  thumbSize,
  wheelSize,
  onColorChangeComplete,
}: PanTrackerProps) {
  const [thumbOpacity, setThumbOpacity] = useState(0);
  const [dimensions, setDimensions] = useState<Dimensions>();
  const { x, y, move } = usePanAnimate(wheelSize, onColorChange);

  const wheel = useRef<View>(null);

  useEffect(() => {
    setThumbOpacity(0);
    setDimensions(undefined);
  }, []);

  useEffect(() => {
    const { left, top } = cartesian(color.h, color.s, wheelSize);
    x.current.setValue(left);
    y.current.setValue(top);
  }, [x, y, color, wheelSize]);

  const wheelPanResponder = PanResponder.create({
    onPanResponderRelease: () => {
      const { h, s, v } = color;
      onColorChangeComplete({
        h: Math.min(360, Math.round(h)),
        s: Math.min(100, Math.round(s * 100)),
        v: Math.min(100, Math.round(v * 100)),
      });
    },
    onStartShouldSetPanResponderCapture: (event, gestureState) => {
      if (outOfWheel(event.nativeEvent, wheelSize)) {
        return false;
      }
      move(event, gestureState);
      return true;
    },
    onPanResponderMove: (event, gestureState) => {
      if (
        outOfWheel(event.nativeEvent, wheelSize) ||
        outOfBox(dimensions, gestureState)
      ) {
        return;
      }
      move(event, gestureState);
    },
  });

  return (
    <>
      <Animated.View
        style={[
          styles.thumb,
          {
            width: thumbSize,
            height: thumbSize,
            borderRadius: thumbSize / 2,
            backgroundColor: tinycolor(color).toHexString(),
            transform: [
              { translateX: -thumbSize / 2 },
              { translateY: -thumbSize / 2 },
            ],
            left: x.current,
            top: y.current,
            opacity: thumbOpacity,
          },
        ]}
        testID="wheel-thumb"
      />
      <View
        style={styles.cover}
        onLayout={() =>
          wheel.current?.measureInWindow((x, y, width, height) => {
            setDimensions({ x, y, width, height });
            setThumbOpacity(1);
          })
        }
        {...wheelPanResponder.panHandlers}
        ref={wheel}
      />
    </>
  );
}

const styles = StyleSheet.create({
  thumb: {
    position: "absolute",
    backgroundColor: "#EEEEEE",
    borderWidth: 3,
    borderColor: "#EEEEEE",
    elevation: 4,
    shadowColor: "rgb(46, 48, 58)",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  },
  cover: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
  },
});

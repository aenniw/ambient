import { MaterialIcons } from "@expo/vector-icons";
import { useThemeColor } from "@hooks/useTheme";
import React, { useEffect, useState } from "react";
import {
  Image,
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";
import tinycolor, { ColorFormats } from "tinycolor2";

import { PanTracker } from "./PanTracker";

const colorWheel = require("@resources/color-wheel.png");

export type ColorPickerProps = {
  color: string;
  thumbSize?: number;
  style?: StyleProp<ViewStyle>;
  onColorChangeComplete: (c: ColorFormats.HSV) => void;
};

export function ColorPicker({
  color: hexColor,
  thumbSize = 40,
  style,
  onColorChangeComplete,
}: ColorPickerProps) {
  const text = useThemeColor("text");

  const [wheelSize, setWheelSize] = useState(0);
  const [color, setColor] = useState<ColorFormats.HSV>(
    tinycolor(hexColor).toHsv(),
  );

  useEffect(() => {
    setWheelSize(0);
    setColor(tinycolor(hexColor).toHsv());
  }, []);
  useEffect(() => setColor(tinycolor(hexColor).toHsv()), [hexColor]);

  const resetColor = () => {
    setColor({ h: 0, s: 0, v: 1 });
    onColorChangeComplete({ h: 0, s: 0, v: 100 });
  };

  return (
    <View style={style} testID="color-picker">
      <View style={styles.container}>
        <View
          style={styles.content}
          onLayout={({ nativeEvent }) => {
            const { width, height } = nativeEvent.layout;
            setWheelSize(Math.min(width, height));
          }}
        >
          {wheelSize > 0 && (
            <View
              style={{
                padding: thumbSize / 2,
                width: wheelSize,
                height: wheelSize,
              }}
            >
              <View style={styles.imgContainer} testID="wheel-wrap">
                <Image style={styles.img} source={colorWheel} />
                <PanTracker
                  color={color}
                  onColorChange={setColor}
                  thumbSize={thumbSize}
                  wheelSize={wheelSize - thumbSize}
                  onColorChangeComplete={onColorChangeComplete}
                />
              </View>
            </View>
          )}
        </View>
      </View>

      <TouchableOpacity
        onPress={resetColor}
        testID="button-wheel-reset"
        style={styles.button}
      >
        <MaterialIcons name="format-color-reset" size={36} color={text} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    overflow: "visible",
  },
  content: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    overflow: "visible",
    width: "100%",
  },
  imgContainer: {
    width: "100%",
    height: "100%",
  },
  img: {
    width: "100%",
    height: "100%",
  },
  button: {
    position: "absolute",
    marginTop: "5%",
    marginLeft: "5%",
    borderRadius: 20,
  },
});

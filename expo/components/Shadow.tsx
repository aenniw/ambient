import { useTheme } from "@hooks/useTheme";
import React from "react";
import {
  Shadow as NativeShadow,
  ShadowProps as NativeShadowProps,
} from "react-native-shadow-2";

type ShadowProps = {
  children: React.ReactNode | React.ReactNode[];
} & NativeShadowProps;

export function Shadow({ children, ...props }: ShadowProps) {
  const theme = useTheme();

  return (
    <NativeShadow
      startColor={theme.dark ? "#ffffff20" : "#00000020"}
      {...props}
    >
      {children}
    </NativeShadow>
  );
}

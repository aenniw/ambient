import { radianPoint } from "@common/trigonometry";
import React, { useEffect, useRef, useState } from "react";
import {
  StyleSheet,
  View,
  PanResponder,
  ViewStyle,
  GestureResponderEvent,
  PanResponderGestureState,
  NativeTouchEvent,
} from "react-native";
import Svg, {
  Path,
  Defs,
  LinearGradient,
  Stop,
  Circle,
} from "react-native-svg";

type CircularSliderProps = {
  radius?: number;
  strokeWidth?: number;
  backgroundTrackColor?: string;
  linearGradient?: { stop: string; color: string }[];
  min?: number;
  max?: number;
  value?: number;
  buttonRadius?: number;
  buttonBorderColor?: string;
  buttonFillColor?: string;
  buttonStrokeWidth?: number;
  style?: ViewStyle;
  contentContainerStyle: ViewStyle;
  children: JSX.Element;
  disabled?: boolean;
  onChange: (v: number) => void;
  onComplete?: (v: number) => void;
};

export function CircularSlider({
  children,
  style,
  contentContainerStyle,
  radius = 100,
  strokeWidth = 20,
  backgroundTrackColor = "#e8e8e8",
  linearGradient = [
    { stop: "0%", color: "#1890ff" },
    { stop: "100%", color: "#f5222d" },
  ],
  value: initValue,
  min = 0,
  max = 100,
  disabled,
  buttonRadius = 12,
  buttonBorderColor = "#fff",
  buttonStrokeWidth = 1,
  buttonFillColor,
  onChange,
  onComplete,
}: CircularSliderProps) {
  const ref = useRef<View>(null);
  const [value, setValue] = useState<number>(initValue || min);

  useEffect(() => setValue(initValue || min), []);

  const extraSize = Math.max(
    strokeWidth,
    (buttonRadius + buttonStrokeWidth) * 2,
  );
  const openingRadian = Math.PI / 4;
  const svgSize = radius * 2 + extraSize;
  const startRadian = 2 * Math.PI - openingRadian;
  const currentRadian =
    ((Math.PI - openingRadian) * 2 * (max - value)) / (max - min) +
    openingRadian;

  const startPoint = radianPoint(startRadian, radius, extraSize);
  const endPoint = radianPoint(openingRadian, radius, extraSize);
  const curPoint = radianPoint(currentRadian, radius, extraSize);

  const polar = (x: number, y: number, size: number) => {
    const distance = radius + size / 2;
    if (x === distance) {
      return y > distance ? 0 : Math.PI / 2;
    }
    const a = Math.atan((y - distance) / (x - distance));
    return (x < distance ? (Math.PI * 3) / 2 : Math.PI / 2) - a;
  };

  const move = (event: GestureResponderEvent, _: PanResponderGestureState) => {
    const rad = polar(
      event.nativeEvent.locationX,
      event.nativeEvent.locationY,
      extraSize,
    );
    const diff = max - min;
    const ratio = (currentRadian - rad) / ((Math.PI - openingRadian) * 2);

    const val: number = Math.round(
      Math.max(min, Math.min(max, value + Math.round(ratio * diff))),
    );
    setValue(val);
    onChange(val);
  };

  const outOfBound = (
    { locationX, locationY }: NativeTouchEvent,
    buttonRadius: number,
  ) => {
    const center = svgSize / 2;
    const distance =
      Math.pow(locationX - center, 2) + Math.pow(locationY - center, 2);
    const minDistance = Math.pow(
      radius - strokeWidth / 2 - (buttonRadius + buttonStrokeWidth),
      2,
    );
    const maxDistance = Math.pow(
      radius + strokeWidth / 2 + (buttonRadius + buttonStrokeWidth),
      2,
    );
    const radian = polar(locationX, locationY, Math.sqrt(distance));
    return (
      distance <= minDistance ||
      distance >= maxDistance ||
      radian <= openingRadian ||
      radian >= startRadian - openingRadian / 2
    );
  };

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponderCapture: (event, gestureState) => {
      if (disabled || outOfBound(event.nativeEvent, buttonRadius)) {
        return false;
      }
      move(event, gestureState);
      return true;
    },
    onPanResponderMove: (event, gestureState) => {
      if (disabled || outOfBound(event.nativeEvent, buttonRadius)) {
        return;
      }
      move(event, gestureState);
    },
    onPanResponderRelease: () => {
      if (disabled || !onComplete) {
        return;
      }
      onComplete(value);
    },
  });

  return (
    <View
      ref={ref}
      style={[styles.container, style]}
      {...panResponder.panHandlers}
    >
      <View style={[styles.content, contentContainerStyle]}>{children}</View>
      <Svg width={svgSize} height={svgSize}>
        <Defs>
          <LinearGradient x1="0%" y1="100%" x2="100%" y2="0%" id="gradient">
            {linearGradient.map((item, index) => (
              <Stop key={index} offset={item.stop} stopColor={item.color} />
            ))}
          </LinearGradient>
        </Defs>
        <Path
          strokeWidth={strokeWidth}
          stroke={backgroundTrackColor}
          fill="none"
          strokeLinecap="round"
          d={`M${startPoint.x},${startPoint.y} A ${radius},${radius},0,${
            startRadian - openingRadian >= Math.PI ? "1" : "0"
          },1,${endPoint.x},${endPoint.y}`}
        />
        <Path
          strokeWidth={strokeWidth}
          stroke="url(#gradient)"
          fill="none"
          strokeLinecap="round"
          d={`M${startPoint.x},${startPoint.y} A ${radius},${radius},0,${
            startRadian - currentRadian >= Math.PI ? "1" : "0"
          },1,${curPoint.x},${curPoint.y}`}
        />
        <Circle
          cx={curPoint.x}
          cy={curPoint.y}
          r={buttonRadius}
          fill={buttonFillColor || buttonBorderColor}
          stroke={buttonBorderColor}
          strokeWidth={buttonStrokeWidth}
        />
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    position: "absolute",
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
  },
});

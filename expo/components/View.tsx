import { useOrientation } from "@hooks/useOrientation";
import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import {
  View as NativeView,
  ViewProps as NativeViewProps,
  StyleSheet,
} from "react-native";

export type ViewProps = {
  children?: React.ReactNode | React.ReactNode[];
} & NativeViewProps;
export function View({ style, children, ...props }: ViewProps) {
  const backgroundColor = useThemeColor("background");

  return (
    <NativeView style={[{ backgroundColor }, style]} {...props}>
      {children}
    </NativeView>
  );
}

export function LandscapeView({ children, style }: ViewProps) {
  const { landscape } = useOrientation();
  return (
    <View
      style={[
        styles.container,
        landscape ? styles.reverseContainer : {},
        style,
      ]}
    >
      {children}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  reverseContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
});

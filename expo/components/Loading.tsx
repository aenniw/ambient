import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import { ActivityIndicator, StyleSheet } from "react-native";

import { TextLabel } from "./Text";
import { LandscapeView } from "./View";

export function Loading({
  size = 200,
  label,
}: {
  size?: number;
  label?: string;
}) {
  const color = useThemeColor("text");

  return (
    <LandscapeView>
      <ActivityIndicator
        size={size}
        color={color}
        style={styles.indicator}
        testID="loading"
      />
      {label && <TextLabel style={styles.description} label={label} />}
    </LandscapeView>
  );
}

export const styles = StyleSheet.create({
  indicator: {
    marginBottom: 30,
  },
  description: {
    marginBottom: 30,
  },
});

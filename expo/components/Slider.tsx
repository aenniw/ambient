import { MaterialIcons } from "@expo/vector-icons";
import { useThemeColor } from "@hooks/useTheme";
import { Slider as NativeSlider } from "@miblanchard/react-native-slider";
import React from "react";
import { StyleProp, StyleSheet, View, ViewStyle } from "react-native";
import { useDebouncedCallback } from "use-debounce";

import { CircularSlider as NativeCircularSlider } from "./CircularSlider";

type SliderOnChangeCallback = (value: number) => void | Promise<void>;
type SliderProps = {
  style?: ViewStyle;
  disabled?: boolean;
  maximumTrackTintColor?: string;
  maximumValue: number;
  minimumValue: number;
  onSlidingComplete?: SliderOnChangeCallback;
  onValueChange?: SliderOnChangeCallback;
  thumbStyle?: ViewStyle;
  thumbTouchSize?: {
    height: number;
    width: number;
  };
  trackClickable?: boolean;
  trackStyle?: ViewStyle;
  value?: number;
  vertical?: boolean;
};

export function Slider({
  maximumTrackTintColor,
  disabled,
  style,
  onSlidingComplete,
  onValueChange,
  ...props
}: SliderProps) {
  const primaryColor = useThemeColor("primary");

  return (
    <NativeSlider
      step={1}
      containerStyle={style}
      minimumTrackTintColor={disabled ? "lightgray" : primaryColor}
      maximumTrackTintColor={disabled ? "lightgray" : maximumTrackTintColor}
      disabled={disabled}
      thumbTintColor={disabled ? "lightgray" : primaryColor}
      onSlidingComplete={
        onSlidingComplete
          ? (v) => {
              if (!Array.isArray(v)) {
                onSlidingComplete(v);
              }
            }
          : undefined
      }
      onValueChange={
        onValueChange
          ? (v) => {
              if (!Array.isArray(v)) {
                onValueChange(v);
              }
            }
          : undefined
      }
      {...props}
    />
  );
}

type CircularSliderProps = {
  value?: number;
  minimumValue: number;
  maximumValue: number;
  radius?: number;
  strokeWidth?: number;
  buttonRadius?: number;
  onSlidingComplete: SliderOnChangeCallback;
  name: React.ComponentProps<typeof MaterialIcons>["name"];
  size?: number;
  style?: StyleProp<ViewStyle>;
};
export function CircularSlider({
  name,
  size = 60,
  value,
  minimumValue,
  maximumValue,
  radius = 160,
  strokeWidth = 15,
  buttonRadius = 24,
  style,
  onSlidingComplete,
}: CircularSliderProps) {
  const primaryColor = useThemeColor("primary");
  const textColor = useThemeColor("text");
  const debounced = useDebouncedCallback(
    (value) => onSlidingComplete(value),
    100,
  );

  return (
    <View style={[styles.circularSlider, style]}>
      <NativeCircularSlider
        min={minimumValue}
        max={maximumValue}
        value={value}
        onChange={debounced}
        contentContainerStyle={styles.circularSliderContainer}
        radius={radius}
        strokeWidth={strokeWidth}
        buttonRadius={buttonRadius}
        buttonBorderColor={primaryColor}
        linearGradient={[
          { stop: "0%", color: textColor },
          { stop: "100%", color: primaryColor },
        ]}
      >
        <View style={styles.iconContainer} testID={`slider-${name}`}>
          <MaterialIcons name={name} size={size} color={textColor} />
        </View>
      </NativeCircularSlider>
    </View>
  );
}

type IconSliderProps = {
  name: React.ComponentProps<typeof MaterialIcons>["name"];
  size?: number;
} & Omit<SliderProps, "style">;
export function IconSlider({ name, size = 36, ...props }: IconSliderProps) {
  const textColor = useThemeColor("text");

  return (
    <View style={styles.iconSlider} testID={`slider-${name}`}>
      <MaterialIcons
        style={styles.icon}
        name={name}
        size={size}
        color={textColor}
      />
      <Slider style={styles.slider} {...props} />
    </View>
  );
}

const styles = StyleSheet.create({
  slider: {
    flex: 1,
  },
  iconSlider: {
    marginTop: 15,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
  },
  circularSlider: {
    justifyContent: "center",
    alignItems: "center",
  },
  circularSliderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    marginRight: 20,
    marginLeft: 10,
  },
  iconContainer: {
    flex: 1,
    position: "relative",
    alignSelf: "center",
    justifyContent: "center",
  },
});

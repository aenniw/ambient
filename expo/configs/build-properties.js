const { default: withBuildProperties } = require("expo-build-properties");

module.exports = (config, { android = {}, ...props }) => {
  const { extraProguardRules, ...androidProps } = android;

  return withBuildProperties(config, {
    android: {
      extraProguardRules: Array.isArray(extraProguardRules)
        ? extraProguardRules.join("\n")
        : extraProguardRules,
      ...androidProps,
    },
    ...props,
  });
};

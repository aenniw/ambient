const fs = require("fs");
const translate = require("translate");

const LANGUAGES = ["cs", "de", "es", "fr", "it", "pl", "sk"];
const LOCALE_DIR = "src/localization/locales/";
const LOCALE = JSON.parse(fs.readFileSync(`${LOCALE_DIR}/en.json`));

translate.from = "en";

// docker run -ti --rm -p 5000:5000 libretranslate/libretranslate
// translate.engine = "libre";
// translate.url = "http://localhost:5000/translate";

translate.url = "https://api-free.deepl.com";
translate.engine = "deepl";
translate.key = process.env.DEEPL_KEY;

for (const lang of LANGUAGES) {
  generateLocalization(LOCALE, lang)
    .then(() => {
      console.info(`Generated ${lang} locale.`);
    })
    .catch(() => {
      console.warn(`Failed to generate ${lang} locale.`);
    });
}

async function generateLocalization(locale, lang) {
  const jsons = JSON.stringify(
    await localizeObject(locale, (v) => translate(v, lang)),
    null,
    4
  );
  fs.writeFileSync(`${LOCALE_DIR}//${lang}.json`, jsons);
}

async function localizeObject(object, translate = async (v) => v) {
  if (typeof object === "string") {
    return restoreParams(object, await translate(object));
  } else if (Array.isArray(object)) {
    const value = [];
    for (const val of object) {
      value.push(await localizeObject(val, translate));
    }
    return value;
  } else if (typeof object === "object") {
    const value = {};
    for (const key of Object.keys(object)) {
      value[key] = await localizeObject(object[key], translate);
    }
    return value;
  }
  return undefined;
}

function restoreParams(object, transalation) {
  const re = /%{.*}/g;
  const original = object.match(re) || [];
  const translated = transalation.match(re) || [];
  for (let i = 0; i < translated.length && i < original.length; i++) {
    transalation = transalation.replace(translated[i], original[i]);
  }
  return transalation;
}

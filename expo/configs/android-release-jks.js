const { withAppBuildGradle } = require("@expo/config-plugins");

const signConfig = `
        if (System.getenv('RELEASE_KEYSTORE_FILE')) {
            release {
                storeFile file(System.getenv('RELEASE_KEYSTORE_FILE'))
                storePassword System.getenv('RELEASE_KEYSTORE_PASSWORD')
                keyAlias System.getenv('RELEASE_KEY_ALIAS')
                keyPassword System.getenv('RELEASE_KEY_PASSWORD')
            }
        }
`;

module.exports = (config) =>
  withAppBuildGradle(config, async (config) => {
    config.modResults.contents = config.modResults.contents.replace(
      /\/\/ Caution! In production, you need to generate your own keystore file\..*signingConfig signingConfigs.debug/s,
      'signingConfig signingConfigs.hasProperty("release") ? signingConfigs.release : signingConfigs.debug'
    );
    config.modResults.contents = config.modResults.contents.replace(
      /signingConfigs \{\n        debug \{/s,
      "signingConfigs {" + signConfig + "        debug {"
    );
    return config;
  });

const { withAppBuildGradle } = require("@expo/config-plugins");

const pluginConfig = `
plugins {
    id 'com.android.application'
    id 'com.github.triplet.play' version '3.8.1'
}
`;

const publisherConfig = `
play {
    if (System.getenv('PLAYSTORE_KEY_FILE')) {
        serviceAccountCredentials = file(System.getenv('PLAYSTORE_KEY_FILE'))
        enabled = true
    } else {
        enabled = false
    }

    track = System.getenv('PLAYSTORE_TRACK') ?: "internal"
    artifactDir.set(file("build/outputs/bundle/release/"))
}
`;

module.exports = (config) =>
  withAppBuildGradle(config, async (config) => {
    config.modResults.contents = config.modResults.contents.replace(
      `apply plugin: "com.android.application"`,
      pluginConfig
    );

    if (!config.modResults.contents.match(/play \{[^\}]*\}/s)) {
      config.modResults.contents += publisherConfig;
    }

    return config;
  });

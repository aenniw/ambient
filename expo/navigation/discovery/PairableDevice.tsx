import BleDevice from "@common/ble-plx/BleDevice";
import { Shadow } from "@components/Shadow";
import { Text } from "@components/Text";
import { View } from "@components/View";
import { MaterialIcons } from "@expo/vector-icons";
import { useOrientation } from "@hooks/useOrientation";
import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

type PairableDeviceProps = {
  device: BleDevice;
  onPress: () => void;
};
export function PairableDevice({ device, onPress }: PairableDeviceProps) {
  const { landscape } = useOrientation();
  const color = useThemeColor("text");

  return (
    <Shadow
      style={styles.shadow}
      containerStyle={[
        styles.container,
        {
          width: landscape ? "40%" : "100%",
        },
      ]}
    >
      <TouchableOpacity
        onPress={onPress}
        style={styles.content}
        testID={`button-device-${device.id}`}
      >
        <MaterialIcons size={42} color={color} name="bluetooth" />

        <View style={styles.info}>
          <Text style={{ fontWeight: "700" }}>{device.getName()}</Text>
          <Text style={{ opacity: 0.8 }}>{device.id}</Text>
        </View>

        <MaterialIcons size={42} color={color} name="navigate-next" />
      </TouchableOpacity>
    </Shadow>
  );
}

const styles = StyleSheet.create({
  shadow: {
    width: "100%",
  },
  container: {
    marginTop: 20,
    alignItems: "center",
  },
  content: {
    flexDirection: "row",
    padding: 10,
    alignItems: "center",
    height: 80,
  },
  info: {
    flex: 1,
    marginLeft: 15,
  },
});

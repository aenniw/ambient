import BleDevice from "@common/ble-plx/BleDevice";
import { Loading } from "@components/Loading";
import React from "react";
import { ScrollView, StyleSheet } from "react-native";

import { PairableDevice } from "./PairableDevice";

type PairableDevicesProps = {
  devices: BleDevice[];
  onPair: (d: BleDevice) => void;
};
export function PairableDevices({ devices, onPair }: PairableDevicesProps) {
  if (devices.length === 0) {
    return <Loading />;
  }
  return (
    <ScrollView style={styles.container} contentContainerStyle={styles.content}>
      {devices.map((device) => (
        <PairableDevice
          key={device.id}
          device={device}
          onPress={() => onPair(device)}
        />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 5,
  },
  content: {
    paddingBottom: 60,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingTop: 10,
  },
});

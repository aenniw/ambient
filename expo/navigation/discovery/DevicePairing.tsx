import BleDevice from "@common/ble-plx/BleDevice";
import { wait } from "@common/common";
import { TextButton } from "@components/Button";
import { TextLabel } from "@components/Text";
import { LandscapeView, View } from "@components/View";
import { MaterialIcons } from "@expo/vector-icons";
import { useThemeColor } from "@hooks/useTheme";
import React, { useEffect, useState } from "react";
import { ActivityIndicator, StyleSheet } from "react-native";

type PairStatus = "failed" | "success" | "pending";
type DeviceParingProps = {
  device: BleDevice;
  onBack: () => void;
  onSuccess: (d: BleDevice) => void;
};

export function DevicePairing({
  device,
  onBack,
  onSuccess,
}: DeviceParingProps) {
  const [status, setStatus] = useState<PairStatus>("pending");

  const color = useThemeColor("text");
  const statusSize = 200;

  useEffect(() => {
    pairDevice();
  }, [device]);

  const onCancel = () => {
    device.cancelConnect();
    onBack();
  };
  const pairDevice = async () => {
    setStatus("pending");
    await wait(2000);
    const [attached, connected] = await device.connect(40);
    if (attached && connected) {
      onSuccess(device);
      setStatus("success");
    } else {
      setStatus("failed");
    }
  };

  return (
    <LandscapeView>
      {status === "pending" && (
        <ActivityIndicator size={statusSize} color={color} />
      )}
      {status === "failed" && (
        <MaterialIcons
          size={statusSize}
          color={color}
          name="error-outline"
          testID="icon-error-outline"
        />
      )}
      {status === "success" && (
        <MaterialIcons
          size={statusSize}
          color={color}
          name="check-circle-outline"
          testID="icon-check-circle-outline"
        />
      )}

      <View style={styles.content}>
        {status === "pending" && (
          <TextLabel style={styles.description} label="description.bonding" />
        )}

        <TextButton
          onPress={status === "failed" ? pairDevice : onCancel}
          label={`button.${status === "failed" ? "retry" : "back"}`}
        />
      </View>
    </LandscapeView>
  );
}

const styles = StyleSheet.create({
  content: {
    marginTop: 50,
  },
  description: {
    marginVertical: 20,
  },
});

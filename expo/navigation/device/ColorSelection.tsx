import { PixelColor } from "@common/ble-plx/types";
import { IconButton } from "@components/Button";
import { ColorPicker } from "@components/ColorPicker";
import { LandscapeView, View } from "@components/View";
import { useOrientation } from "@hooks/useOrientation";
import { useHeaderHeight } from "@react-navigation/elements";
import React, { useEffect, useState } from "react";
import { StyleSheet, useWindowDimensions } from "react-native";

const BASE_COLOR: PixelColor = [0, 0, "#ffffff"];

type ColorSelectionProps = {
  baseColor?: PixelColor;
  onSelected: (c: PixelColor) => void;
  onCancel: () => void;
};
export function ColorSelection({
  baseColor = BASE_COLOR,
  onSelected,
  onCancel,
}: ColorSelectionProps) {
  const [color, setColor] = useState<PixelColor>(baseColor);
  const { height, width } = useWindowDimensions();
  const { landscape } = useOrientation();
  const headerHeight = useHeaderHeight();

  useEffect(() => setColor(baseColor), [baseColor]);

  const dimmesions = {
    height: landscape ? height - headerHeight * 2 : width,
    width: landscape ? height - headerHeight * 2 : width,
  };

  return (
    <LandscapeView>
      <ColorPicker
        color={baseColor[2]}
        onColorChangeComplete={({ h, s }) => setColor([h, s, ""])}
        style={dimmesions}
      />
      <View
        style={{
          flexDirection: landscape ? "column" : "row",
          justifyContent: "center",
          alignItems: "center",
          marginVertical: 45,
          marginHorizontal: 20,
        }}
      >
        <IconButton
          style={styles.button}
          name="close"
          size={36}
          color="white"
          onPress={onCancel}
        />
        <IconButton
          style={styles.button}
          name="check"
          size={36}
          color="white"
          onPress={() => {
            onSelected(color);
            onCancel();
          }}
        />
      </View>
    </LandscapeView>
  );
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    width: 120,
    justifyContent: "center",
    alignItems: "center",
    margin: 15,
    borderRadius: 10,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
});

import { PixelColor } from "@common/ble-plx/types";
import { Loading } from "@components/Loading";
import { View } from "@components/View";
import React, { useEffect, useState } from "react";
import { StyleSheet } from "react-native";

import { ColorSelection } from "./ColorSelection";
import { PaletteColors } from "./PaletteColors";

type ColorsViewType = {
  type: "palette" | "selection";
  selection?: {
    index: number;
    color: PixelColor;
  };
};

type DeviceColorsProps = {
  colors?: PixelColor[];
  setColors: (c: PixelColor[]) => void;
};
export function DeviceColors({ colors, setColors }: DeviceColorsProps) {
  const [{ type, selection }, setState] = useState<ColorsViewType>({
    type: "palette",
  });

  useEffect(() => setState({ type: "palette" }), []);

  const onColorAdded = (color: PixelColor) => {
    setColors([...(colors || []), color]);
  };
  const onColorEdited = (color: PixelColor) => {
    const next = [...(colors || [])];
    next[selection?.index || 0] = color;
    setColors(next);
  };

  const onCancel = () => setState({ type: "palette" });

  if (colors === undefined) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      {type === "palette" && (
        <PaletteColors
          colors={colors}
          setColors={setColors}
          addColor={() => setState({ type: "selection" })}
          editColor={(index, color) =>
            setState({ type: "selection", selection: { index, color } })
          }
        />
      )}
      {type === "selection" && (
        <ColorSelection
          baseColor={selection?.color}
          onSelected={selection ? onColorEdited : onColorAdded}
          onCancel={onCancel}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

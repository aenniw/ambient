import { PixelColor } from "@common/ble-plx/types";
import { IconButton } from "@components/Button";
import { LandscapeView } from "@components/View";
import { useOrientation } from "@hooks/useOrientation";
import { useHeaderHeight } from "@react-navigation/elements";
import React from "react";
import { StyleSheet, useWindowDimensions } from "react-native";
import DraggableFlatList, {
  RenderItemParams,
  OpacityDecorator,
} from "react-native-draggable-flatlist";

import { PaletteColor } from "./PaletteColor";

type PaletteColorsProps = {
  colors: PixelColor[];
  setColors: (c: PixelColor[]) => void;
  addColor: () => void;
  editColor: (i: number, c: PixelColor) => void;
};
export function PaletteColors({
  colors,
  setColors,
  addColor,
  editColor,
}: PaletteColorsProps) {
  const { height } = useWindowDimensions();
  const { landscape } = useOrientation();
  const headerHeight = useHeaderHeight();

  const maxColors = (colors || []).length > 10;

  const removeColor = (i: number) => {
    const c = [...(colors || [])];
    c.splice(i, 1);
    setColors(c);
  };

  const renderItem = ({
    item,
    drag,
    getIndex,
  }: RenderItemParams<PixelColor>) => {
    const index = getIndex() || 0;
    return (
      <OpacityDecorator>
        <PaletteColor
          color={item}
          onPress={() => editColor(index, item)}
          onLongPress={drag}
          onClose={() => removeColor(index)}
        />
      </OpacityDecorator>
    );
  };

  return (
    <LandscapeView style={styles.container}>
      {landscape && (
        <IconButton
          style={styles.button}
          name="add"
          size={36}
          color="white"
          onPress={addColor}
          disabled={maxColors}
        />
      )}
      <DraggableFlatList
        style={{
          maxHeight: height - headerHeight - (landscape ? 4 : 60),
        }}
        containerStyle={styles.content}
        scrollEnabled
        data={colors}
        onDragEnd={({ data }) => setColors(data)}
        keyExtractor={(item, i) => `${i}-${item[2]}`}
        renderItem={renderItem}
        ListFooterComponent={
          !landscape ? (
            <IconButton
              style={styles.button}
              name="add"
              size={36}
              color="white"
              onPress={addColor}
              disabled={maxColors}
            />
          ) : undefined
        }
      />
    </LandscapeView>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
  },
  content: {
    flex: 1,
    width: "100%",
  },
  button: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 10,
    marginHorizontal: 60,
    marginTop: 20,
  },
});

import { PixelsMode, PixelsOptions } from "@common/ble-plx/types";
import { Shadow } from "@components/Shadow";
import { TextLabel } from "@components/Text";
import { PIXEL_MODES } from "@hooks/pixels/useMode";
import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import {
  StyleProp,
  StyleSheet,
  TouchableOpacity,
  View,
  ViewStyle,
} from "react-native";

type ModeButtonProps = {
  mode: PixelsMode;
  selected: boolean;
  onPress: () => void;
};
function ModeButton({ mode, selected, onPress }: ModeButtonProps) {
  const primaryColor = useThemeColor("primary");
  const backgroundColor = useThemeColor("background");
  const textColor = useThemeColor("text");

  return (
    <Shadow
      style={{
        backgroundColor: selected ? primaryColor : backgroundColor,
        maxWidth: 180,
        height: 37,
        borderRadius: 15,
        paddingVertical: 5,
        paddingHorizontal: 10,
      }}
      containerStyle={styles.modeContainer}
    >
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
        testID={`button-mode-${mode}`}
      >
        <TextLabel
          label={`pixel.mode.${mode}`}
          style={{
            color: selected ? "white" : textColor,
          }}
        />
      </TouchableOpacity>
    </Shadow>
  );
}

type DeviceModesProps = {
  mode?: [PixelsMode, PixelsOptions];
  setMode: (m: PixelsMode) => void;
  style?: StyleProp<ViewStyle>;
};
export function DeviteModes({ style, mode, setMode }: DeviceModesProps) {
  return (
    <View style={[styles.container, style]}>
      {mode &&
        PIXEL_MODES.map((m) => (
          <ModeButton
            key={m}
            mode={m}
            selected={m === mode[0]}
            onPress={() => setMode(m)}
          />
        ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "center",
    flexWrap: "wrap",
  },
  modeContainer: {
    marginHorizontal: 8,
    marginVertical: 8,
  },
  button: {
    flex: 1,
    justifyContent: "center",
  },
});

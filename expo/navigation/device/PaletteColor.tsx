import { PixelColor } from "@common/ble-plx/types";
import { IconButton } from "@components/Button";
import { Shadow } from "@components/Shadow";
import { MaterialIcons } from "@expo/vector-icons";
import { useContrastColor } from "@hooks/useTheme";
import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

type PaletteColorProps = {
  color: PixelColor;
  onPress: () => void;
  onLongPress: () => void;
  onClose: () => void;
  disabled?: boolean;
};

export function PaletteColor({
  color,
  onPress,
  onLongPress,
  onClose,
  disabled,
}: PaletteColorProps) {
  const textColor = useContrastColor("text", color[2]);
  return (
    <Shadow style={styles.content} containerStyle={styles.container}>
      <LinearGradient
        colors={["#0000008C", "transparent"]}
        start={{ x: 0.18, y: 0.5 }}
        end={{ x: 1, y: 0.5 }}
        style={[
          {
            flex: 1,
            backgroundColor: color[2],
            flexDirection: "row",
            alignItems: "center",
          },
        ]}
      >
        <TouchableOpacity
          onPress={onPress}
          onLongPress={onLongPress}
          delayLongPress={100}
          disabled={disabled}
          testID={`button-color-${color}`}
          style={{
            flex: 1,
            paddingVertical: 10,
            paddingLeft: 20,
          }}
        >
          <MaterialIcons name="invert-colors" size={40} color={textColor} />
        </TouchableOpacity>
        <IconButton
          style={styles.button}
          name="close"
          size={40}
          color={textColor}
          onPress={onClose}
          testID={`button-color-${color}-close`}
        />
      </LinearGradient>
    </Shadow>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
  },
  content: {
    flex: 1,
    width: "100%",
    borderRadius: 10,
    overflow: "hidden",
  },
  button: {
    backgroundColor: "transparent",
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
});

import { PixelColor, PixelsMode, PixelsOptions } from "@common/ble-plx/types";
import { Switch } from "@components/Button";
import { ColorPicker } from "@components/ColorPicker";
import { Loading } from "@components/Loading";
import { CircularSlider, IconSlider } from "@components/Slider";
import { TextLabel } from "@components/Text";
import { LandscapeView, View } from "@components/View";
import { useOrientation } from "@hooks/useOrientation";
import { useHeaderHeight } from "@react-navigation/elements";
import React, { useEffect, useState } from "react";
import { StyleSheet, useWindowDimensions } from "react-native";
import tinycolor from "tinycolor2";

import { DeviteModes } from "./Modes";
import { Partitions } from "./Partitions";

type DeviceControlProps = {
  color?: PixelColor;
  setColor: (c: [number, number]) => void;
  brightness?: number;
  setBrightness: (b: number) => void;
  mode?: [PixelsMode, PixelsOptions];
  setMode: (m: PixelsMode) => void;
  setOptions: (o: PixelsOptions) => void;
  version?: string;
};
export function DeviceControl({
  color,
  brightness,
  mode: pixelMode,
  version,
  setMode,
  setColor,
  setBrightness,
  setOptions,
}: DeviceControlProps) {
  const [initialColor, setInitialColor] = useState<string>();
  const { height, width } = useWindowDimensions();
  const { landscape } = useOrientation();
  const headerHeight = useHeaderHeight();

  useEffect(() => {
    if (color === undefined) {
      setInitialColor(undefined);
    } else if (!initialColor) {
      setInitialColor(
        tinycolor({
          h: color[0],
          s: color[1],
          v: 1,
        }).toHexString(),
      );
    }
  }, [color]);

  if (!initialColor || pixelMode === undefined || version === undefined) {
    return <Loading />;
  }

  const [mode, options] = pixelMode;
  const speed = 1800 - options.duration;
  const setSpeed = (s: number) =>
    setOptions({ ...options, duration: 1800 - s });

  const dimmesions = {
    height: landscape ? height - headerHeight - 4 : width,
    width: landscape ? height - headerHeight - 4 : width,
  };

  return (
    <LandscapeView>
      {mode === PixelsMode.RAINBOW || mode === PixelsMode.TRANSITION ? (
        <CircularSlider
          name="speed"
          size={60}
          radius={dimmesions.height * 0.4}
          minimumValue={0}
          maximumValue={1800}
          value={speed}
          onSlidingComplete={setSpeed}
          style={dimmesions}
        />
      ) : (
        <ColorPicker
          color={initialColor}
          onColorChangeComplete={({ h, s }) => setColor([h, s])}
          style={dimmesions}
        />
      )}

      <View
        style={{
          minWidth: landscape ? undefined : "85%",
          paddingRight: landscape ? 40 : 0,
          paddingBottom: 15,
        }}
      >
        {landscape && <DeviteModes mode={pixelMode} setMode={setMode} />}
        <IconSlider
          name="brightness-medium"
          minimumValue={0}
          maximumValue={100}
          disabled={brightness === undefined}
          value={brightness}
          onSlidingComplete={setBrightness}
        />
        {mode === PixelsMode.FADE && (
          <IconSlider
            name="speed"
            minimumValue={0}
            maximumValue={1800}
            disabled={speed === undefined}
            value={speed}
            onSlidingComplete={setSpeed}
          />
        )}
        <Partitions
          version={version}
          mode={pixelMode}
          setOptions={setOptions}
        />
        {mode !== PixelsMode.STATIC && (
          <View style={styles.optionsContainer}>
            <View style={styles.labelSwitch}>
              <TextLabel label="pixel.modifiers.chain" style={styles.text} />
              <Switch
                onValueChange={() =>
                  setOptions({ ...options, chained: !options.chained })
                }
                value={options.chained}
                testID="chain"
              />
            </View>
            <View style={styles.labelSwitch}>
              <TextLabel label="pixel.modifiers.random" style={styles.text} />
              <Switch
                onValueChange={() =>
                  setOptions({ ...options, randomized: !options.randomized })
                }
                value={options.randomized}
                testID="random"
              />
            </View>
          </View>
        )}
      </View>
    </LandscapeView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    justifyContent: "center",
  },
  labelSwitch: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    flexGrow: 1,
  },
  optionsContainer: {
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
    justifyContent: "center",
    marginTop: 15,
  },
  text: {
    marginRight: 15,
  },
});

import { PixelsMode, PixelsOptions } from "@common/ble-plx/types";
import { Text } from "@components/Text";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useThemeColor } from "@hooks/useTheme";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import semver from "semver";

type PartitionProps = {
  partition: number;
  options: PixelsOptions;
  onPress: (partition: number) => void;
};
function Partition({ partition, options, onPress }: PartitionProps) {
  const primaryColor = useThemeColor("primary");

  return (
    <TouchableOpacity
      style={{
        flex: 1,
        height: 36,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor:
          partition - 1 === options.partition ? primaryColor : "gray",
      }}
      onPress={() => onPress(partition - 1)}
      testID={`button-partition-${partition}`}
    >
      <Text style={styles.text}>{partition}</Text>
    </TouchableOpacity>
  );
}

type PartitionsProps = {
  version: string;
  mode: [PixelsMode, PixelsOptions];
  setOptions: (o: PixelsOptions) => void;
};
export function Partitions({
  version,
  mode: pixelMode,
  setOptions,
}: PartitionsProps) {
  const textColor = useThemeColor("text");
  const [mode, options] = pixelMode;

  const setPartition = (partition: number) =>
    setOptions({ ...options, partition });

  if (
    semver.lte(version, "0.1.0") ||
    mode === PixelsMode.STATIC ||
    (!options.chained && !options.randomized)
  ) {
    return null;
  }

  return (
    <View style={styles.container}>
      <MaterialCommunityIcons
        name="lightbulb-group-outline"
        testID="icon-partition"
        size={42}
        color={textColor}
        style={styles.icon}
      />

      <View style={styles.content}>
        <Partition partition={1} options={options} onPress={setPartition} />
        <Partition partition={2} options={options} onPress={setPartition} />
        <Partition partition={3} options={options} onPress={setPartition} />
        <Partition partition={4} options={options} onPress={setPartition} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 15,
    paddingRight: 10,
  },
  content: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    borderRadius: 12,
    overflow: "hidden",
  },
  icon: {
    marginLeft: 7,
    marginRight: 16,
  },
  text: {
    marginTop: 2,
    fontSize: 22,
    color: "white",
    fontWeight: "600",
  },
});

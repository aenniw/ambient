import BleDevice from "@common/ble-plx/BleDevice";
import { PixelsMode, PixelsState } from "@common/ble-plx/types";
import { Slider } from "@components/Slider";
import { TextLabel } from "@components/Text";
import { MaterialIcons, MaterialCommunityIcons } from "@expo/vector-icons";
import { usePixel } from "@hooks/pixels/usePixel";
import { useContrastColor, useThemeColor } from "@hooks/useTheme";
import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, Switch, TouchableOpacity, View } from "react-native";

type StrandControllProps = {
  strand: number;
  device: BleDevice;
  onPress: (d: BleDevice, s: number) => void;
};
export function StrandControll({
  strand,
  device,
  onPress,
}: StrandControllProps) {
  const { ready, mode, state, color, brightness, setBrightness, toggle } =
    usePixel(device, strand);

  const disabled = !ready;
  const colorHex = color ? color[2] : "gray";
  const textColor = useContrastColor("text", disabled ? "black" : colorHex);
  const primaryColor = useThemeColor("primary");

  let gradient: [string, string, ...string[]] = ["#cdcdcd8C", colorHex];
  if (mode) {
    if (mode[0] === PixelsMode.FADE) {
      gradient = ["#cdcdcd8C", colorHex, "#cdcdcd8C"];
    } else if (mode[0] !== PixelsMode.STATIC) {
      gradient = ["#cc99c9", "#9ec1cf", "#9ee09e", "#fdfd97"];
    }
  }

  return (
    <LinearGradient
      colors={gradient}
      start={{ x: 0.18, y: 0.5 }}
      end={{ x: 1, y: 0.5 }}
      style={{
        paddingVertical: 8,
        paddingHorizontal: 20,
        backgroundColor: disabled ? "gray" : colorHex,
        opacity: disabled ? 0.75 : undefined,
        marginHorizontal: 10,
        marginBottom: 10,
        borderRadius: 10,
      }}
      testID={device.id}
    >
      <View style={styles.content}>
        <TouchableOpacity
          style={styles.strandButton}
          onPress={() => onPress(device, strand)}
          disabled={disabled}
          testID={`button-strand-${strand}`}
        >
          <MaterialCommunityIcons
            name="string-lights"
            size={32}
            color={textColor}
          />
          <TextLabel
            style={{
              color: textColor,
              marginTop: 2,
              fontSize: 18,
              fontWeight: "bold",
              marginHorizontal: 15,
            }}
            label="label.strand"
            options={{ strand: strand + 1 }}
          />
        </TouchableOpacity>

        <Switch
          trackColor={{ false: "#fff", true: "#fff" }}
          thumbColor={disabled ? "darkgray" : primaryColor}
          onValueChange={toggle}
          value={state === PixelsState.ON}
          disabled={disabled}
          testID={`switch-strand-${strand}`}
        />
      </View>

      <View style={styles.content}>
        <MaterialIcons
          style={styles.icon}
          name="brightness-medium"
          size={20}
          color={disabled || !state ? "darkgray" : textColor}
        />
        <Slider
          style={styles.slider}
          value={brightness}
          onSlidingComplete={setBrightness}
          minimumValue={0}
          maximumValue={100}
          disabled={disabled || !state}
        />
      </View>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  content: {
    marginVertical: 2,
    flexDirection: "row",
    alignItems: "center",
  },
  button: {
    justifyContent: "center",
  },
  strandButton: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
  },
  icon: {
    marginLeft: 5,
  },
  slider: {
    flex: 1,
    marginLeft: 10,
  },
});

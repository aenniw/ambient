import BleDevice from "@common/ble-plx/BleDevice";
import { Shadow } from "@components/Shadow";
import { useEnabled } from "@hooks/pixels/useEnabled";
import { useOrientation } from "@hooks/useOrientation";
import { useThemeColor } from "@hooks/useTheme";
import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet } from "react-native";

import { PixelsStatus } from "./PixelsStatus";
import { StrandControll } from "./StrandControll";

type PixelsControllProps = {
  device: BleDevice;
  onPress: (d: BleDevice, s: number) => void;
};
export function PixelsControll({ device, onPress }: PixelsControllProps) {
  const { landscape } = useOrientation();
  const enabled = useEnabled(device);
  const color = useThemeColor("text");
  const bacground = useThemeColor("background");

  return (
    <Shadow
      style={styles.content}
      containerStyle={{
        flex: 1,
        minWidth: landscape ? "40%" : "90%",
        marginTop: 20,
        borderRadius: 20,
        marginHorizontal: 15,
      }}
    >
      <LinearGradient
        colors={[bacground, `#cdcdcd8C`]}
        start={{ x: 0.18, y: 0.5 }}
        end={{ x: 1, y: 0.5 }}
      >
        <PixelsStatus
          device={device}
          disabled={!enabled || enabled.every((e) => !e)}
          color={color}
        />
        {enabled &&
          enabled.map((e, s) =>
            e ? (
              <StrandControll
                key={s}
                strand={s}
                device={device}
                onPress={onPress}
              />
            ) : null,
          )}
      </LinearGradient>
    </Shadow>
  );
}

const styles = StyleSheet.create({
  content: {
    width: "100%",
    borderRadius: 20,
    overflow: "hidden",
  },
});

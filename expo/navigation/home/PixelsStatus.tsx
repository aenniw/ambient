import BleDevice from "@common/ble-plx/BleDevice";
import { Text } from "@components/Text";
import { MaterialIcons, MaterialCommunityIcons } from "@expo/vector-icons";
import { router } from "expo-router";
import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";

type PixelsStatusProps = {
  device: BleDevice;
  disabled: boolean;
  color: string;
};
export function PixelsStatus({ device, disabled, color }: PixelsStatusProps) {
  const iconName = !disabled ? "bluetooth" : "bluetooth-disabled";

  return (
    <View style={styles.container} testID={device.id}>
      <MaterialIcons
        size={42}
        color={color}
        name={iconName}
        testID={`icon-${iconName}`}
      />
      <Text
        style={{
          color,
          fontSize: 20,
          fontWeight: "bold",
          flex: 1,
          marginLeft: 20,
        }}
      >
        {device.getName()}
      </Text>

      <TouchableOpacity
        style={styles.config}
        disabled={disabled}
        onPress={() => router.navigate(`/settings/${device.id}`)}
        testID="button-settings"
      >
        <MaterialCommunityIcons
          name={disabled ? "cog-off" : "cog"}
          size={32}
          color={color}
          testID={`icon-${disabled ? "cog-off" : "cog"}`}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  config: {
    marginHorizontal: 6,
  },
});

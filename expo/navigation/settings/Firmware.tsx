import BleDevice from "@common/ble-plx/BleDevice";
import { Firmware } from "@common/ble-plx/types";
import { toHHMMSS, wait } from "@common/common";
import { getFirmware } from "@common/firmware";
import { TextButton } from "@components/Button";
import { TextLabel } from "@components/Text";
import { View } from "@components/View";
import { useThemeColor } from "@hooks/useTheme";
import { activateKeepAwakeAsync, deactivateKeepAwake } from "expo-keep-awake";
import * as Network from "expo-network";
import React, { useEffect, useState } from "react";
import { Platform, StyleSheet } from "react-native";
import * as Progress from "react-native-progress";

type ChipFirmwareProps = {
  device: BleDevice;
  version: string;
  hardware: string;
  onUpgrade: (v: boolean) => void;
};
export function ChipFirmware({
  device,
  hardware,
  version,
  onUpgrade,
}: ChipFirmwareProps) {
  const [editable, setEditable] = useState<boolean>(false);
  const [showProgress, setShowProgress] = useState<boolean>(false);
  const [firmware, setFirmware] = useState<Firmware>();
  const [flashProgress, setFlashProgress] = useState<number>();
  const [uploadEtc, setUploadEtc] = useState<string>();

  const primaryColor = useThemeColor("primary");

  useEffect(() => {
    setFlashProgress(undefined);
    setFirmware(undefined);
    setUploadEtc(undefined);
    setShowProgress(false);
    setEditable(false);

    Network.getNetworkStateAsync().then(({ isInternetReachable = false }) =>
      setEditable(isInternetReachable),
    );

    return () => device.cancelUpgrade();
  }, [device]);

  const checkFirmware = async () => {
    setShowProgress(true);
    const [firmware] = await Promise.all([
      getFirmware(hardware, version),
      wait(1000),
    ]);
    setFirmware(firmware);
    setEditable(firmware !== undefined);
    setShowProgress(false);
  };

  const setUpgrade = (v: boolean) => {
    onUpgrade(v);
    setShowProgress(v);
  };

  const upgradeFirmware = async () => {
    if (!firmware) {
      return setUpgrade(false);
    }

    try {
      setUpgrade(true);
      await activateKeepAwakeAsync();
      const uploadStart = Date.now();
      await device.upgrade(firmware, (p) => {
        setUploadEtc(
          toHHMMSS((((Date.now() - uploadStart) / p) * (1 - p)) / 1000),
        );
        setFlashProgress(p);
      });
    } finally {
      setUpgrade(false);
      setEditable(false);
      await deactivateKeepAwake();
    }
  };

  if (showProgress) {
    return (
      <View style={styles.progress}>
        <Progress.Bar
          indeterminate={!flashProgress}
          progress={flashProgress}
          color={primaryColor}
          height={20}
          width={180}
        />
        {uploadEtc && (
          <TextLabel label="description.etc" options={{ time: uploadEtc }} />
        )}
      </View>
    );
  }

  return (
    <>
      {!firmware ? (
        <TextButton
          onPress={checkFirmware}
          disabled={!editable || (Platform.OS === "web" && !__DEV__)}
          label="button.check"
        />
      ) : (
        <TextButton
          onPress={upgradeFirmware}
          label="button.upgrade"
          options={firmware}
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  progress: {
    justifyContent: "center",
    alignItems: "center",
  },
});

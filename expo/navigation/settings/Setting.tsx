import { TextLabel } from "@components/Text";
import { View } from "@components/View";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useOrientation } from "@hooks/useOrientation";
import { useThemeColor } from "@hooks/useTheme";
import { TranslateOptions } from "i18n-js/typings/typing";
import React from "react";
import { StyleSheet } from "react-native";

type SettingProps = {
  icon: React.ComponentProps<typeof MaterialCommunityIcons>["name"];
  label: string;
  labelOptions?: TranslateOptions;
  children: React.ReactNode | React.ReactNode[];
  description?: string | React.ReactNode | React.ReactNode[];
  descriptionOptions?: TranslateOptions;
};
export function Setting({
  children,
  label,
  labelOptions,
  description,
  descriptionOptions,
  icon,
}: SettingProps) {
  const { landscape } = useOrientation();
  const textColor = useThemeColor("text");

  return (
    <View
      style={[
        styles.container,
        {
          maxWidth: landscape ? "50%" : "100%",
          justifyContent: "space-around",
        },
      ]}
    >
      <View style={styles.descriptionContainer}>
        <MaterialCommunityIcons name={icon} size={36} color={textColor} />
        <View style={styles.description}>
          <TextLabel
            label={label}
            options={labelOptions}
            style={{ textDecorationLine: description ? "underline" : "none" }}
          />
          {description &&
            (typeof description === "string" ? (
              <TextLabel label={description} options={descriptionOptions} />
            ) : (
              description
            ))}
        </View>
      </View>
      <View style={styles.content}>{children}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    marginVertical: 15,
    alignItems: "center",
    minHeight: 50,
    flexWrap: "wrap",
    paddingHorizontal: 10,
  },
  content: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 4,
  },
  description: {
    marginHorizontal: 20,
    flexDirection: "column",
  },
  descriptionContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 10,
  },
});
